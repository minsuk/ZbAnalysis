#!/usr/bin/env python
#
# Read NanoAOD skim and make histograms
# 12.10.2018/S.Lehti
#

import os
import sys
import re
import subprocess
import datetime
import json

import ROOT
import multiprocessing

MAX_WORKERS = max(multiprocessing.cpu_count()-1,1)

MULTIPROCESSING = True

#LEPTONFLAVOR = 11
LEPTONFLAVOR = 13

maxEvents = -1
#maxEvents = 10000

pileupfile = {}
pileupfile["2016"] = "pileup_2016.txt"
pileupfile["2017"] = "pileup_2017.txt"
pileupfile["2018"] = "pileup_2018.txt"

def usage():
    print
    print "### Usage:  ",os.path.basename(sys.argv[0]),"<multicrab skim>"
    print

root_re = re.compile("(?P<rootfile>([^/]*events_\d+\.root))")
"""
xsecs = {}
xsecs["TTJets"] = 6.639e+02 # https://twiki.cern.ch/twiki/bin/view/CMS/HowToGenXSecAnalyzer#Running_the_GenXSecAnalyzer_on_a
xsecs["ST_tW_top_5f_inclusiveDecays"]            = 35.85 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_tW_antitop_5f_inclusiveDecays"]        = 35.85 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_t_channel_top_4f_inclusiveDecays"]     = 136.02# https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_t_channel_antitop_4f_inclusiveDecays"] = 80.95 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["ST_s_channel_4f_InclusiveDecays"]         = 11.36 # https://twiki.cern.ch/twiki/bin/viewauth/CMS/SingleTopSigma
xsecs["DYJetsToLL_M_50_HT_70to100"]    = 209.592
xsecs["DYJetsToLL_M_50_HT_100to200"]   = 181.302 # McM times NNLO/LO ratio of inclusive sample
xsecs["DYJetsToLL_M_50_HT_200to400"]   = 50.4177 # McM times NNLO/LO ratio of inclusive sample
xsecs["DYJetsToLL_M_50_HT_400to600"]   = 6.98314
xsecs["DYJetsToLL_M_50_HT_600to800"]   = 1.6841 
xsecs["DYJetsToLL_M_50_HT_800to1200"]  = 0.775392
xsecs["DYJetsToLL_M_50_HT_1200to2500"] = 0.18622
xsecs["DYJetsToLL_M_50_HT_2500toInf"]  = 0.004384
xsecs["WJetsToLNu_HT_100To200"]   = 1.293e+03*1.2138
xsecs["WJetsToLNu_HT_200To400"]   = 3.86e+02*1.2138
xsecs["WJetsToLNu_HT_400To600"]   = 47.9*1.2138
xsecs["WJetsToLNu_HT_600To800"]   = 12.8*1.2138
xsecs["WJetsToLNu_HT_800To1200"]  = 5.26*1.2138
xsecs["WJetsToLNu_HT_1200To2500"] = 1.33*1.2138
xsecs["WJetsToLNu_HT_2500ToInf"]  = 3.089e-02*1.2138

label = {}
label["R_pT"] = "p_{T} balance"
label["R_MPF"] = "MPF"

def getCrossSection(datasetName):
    if "_ext" in datasetName:
        datasetName = datasetName[:datasetName.find("_ext")]
    return xsecs[datasetName]
"""
class Dataset:
    def __init__(self,path,run):
        self.name = os.path.basename(path)
        self.run = run
        self.isData = False
        if "Run201" in self.name:
            self.isData = True
        self.lumi = 0
        self.files = []
        cands = execute("ls %s"%os.path.join(path,"results"))
        for c in cands:
            match = root_re.search(c)
            if match:
                self.files.append(os.path.join(path,"results",match.group("rootfile")))
        #print self.name,len(self.files)

        if len(self.files) == 0:
            print "Dataset contains no root files"
            return
            
        if self.isData:
            self.fPU = ROOT.TFile.Open(os.path.join(path,"results","PileUp.root"))
            self.pileup = self.fPU.Get("pileup").Clone("pileup")
            self.pileup.SetDirectory(0)
            #print "check pu data",self.pileup.GetEntries()
            #for i in range(1,self.pileup.GetNbinsX()):
            #    print "check pu bin ",i,self.pileup.GetBinContent(i)
            self.fPU.Close()
        else:
            self.fPU = ROOT.TFile.Open(os.path.join(path,"results",self.files[0]))
            self.pileup = self.fPU.Get("configInfo/pileup").Clone("pileup_mc")
            self.pileup.SetDirectory(0)
            self.pileup.Reset()
            self.fPU.Close()
            for fname in self.files:
                f = ROOT.TFile.Open(os.path.join(path,"results",fname))
                h = f.Get("configInfo/pileup").Clone("pileup")
                self.pileup.Add(h)
                f.Close()


        self.fRF = ROOT.TFile.Open(os.path.join(path,"results",self.files[0]))
        self.skimCounter = self.fRF.Get("configInfo/skimCounter").Clone("skimCounter")
        self.skimCounter.Reset()
        for fname in self.files:
            rf = ROOT.TFile.Open(os.path.join(path,"results",fname))
            s = rf.Get("configInfo/skimCounter").Clone("skimCounter")
            self.skimCounter.Add(s)
            rf.Close()

        #self.fRF.cd("histograms")
        gDir = ROOT.gFile.CurrentDirectory()
        keys = gDir.GetListOfKeys()
        nkeys = gDir.GetNkeys()
        self.fRF.cd()
        self.histograms = {}
        for i in range(nkeys):
            keyname = keys.At(i).GetName()
            #print "check key",keyname
            obj = gDir.Get(keyname).Clone(keyname)
            obj.Reset()
            obj.SetTitle("skim")
            self.histograms[keyname] = obj
        for fname in self.files:
            #print "check file",fname
            rf = ROOT.TFile.Open(os.path.join(path,"results",fname))
            rf.cd()
            for i in range(nkeys):
                keyname = keys.At(i).GetName()
                #s = gDir.Get(os.path.join("histograms",keyname))
                #s = gDir.Get(keyname).Clone(keyname)
                s = rf.Get(os.path.join("histograms",keyname)).Clone(keyname)
                #print "obj",keyname,s.GetEntries(),s.GetMean()
                self.histograms[keyname].Add(s)
            rf.Close()

#        print "check histo",self.histograms["Z pt"].GetEntries(),self.histograms["Z pt"].GetMean()
#        sys.exit()
#            if isinstance(obj, ROOT.TH1F):
#                lsHisto(obj)
#        print "check pu",self.pileup.GetEntries()
#        print "check skimcounter",self.skimCounter.GetBinContent(1),self.skimCounter.GetBinContent(2)
#        self.fRF.Close()
#        if not self.isData:
#            self.xsec = getCrossSection(self.name)

    def getFileNames(self):
        return self.files

    def getSkimCounter(self):
        return self.skimCounter

    def Print(self):
        print self.name
        print "    is data",self.isData
        print "    number of files",len(self.files)

def execute(cmd):
    p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE, stderr=subprocess.STDOUT, close_fds=True)
    (s_in, s_out) = (p.stdin, p.stdout)

    f = s_out
    ret=[]
    for line in f:
        ret.append(line.replace("\n", ""))
    f.close()
    return ret

def getRun(datasetnames):
    run = ""
    run_re = re.compile("(?P<run>Run201\d)")
    for n in datasetnames:
        match = run_re.search(n)
        if match:
            run = match.group("run")
    return run

def getDatasets(multicrabdir,whitelist=[],blacklist=[]):
    datasets = []
    cands = execute("ls %s"%multicrabdir)
#    for c in cands:
#        resultdir = os.path.join(multicrabdir,c,"results")
#        if os.path.exists(resultdir):
#            datasets.append(Dataset(os.path.join(multicrabdir,c)))

    if len(whitelist) > 0:
        #print "check whitelist 1 ",whitelist,blacklist
        datasets_whitelist = []
        for d in cands:
            for wl in whitelist:
                wl_re = re.compile(wl)
                match = wl_re.search(d)
                if match:
                    datasets_whitelist.append(d)
                    break
        #print "check whitelist",datasets_whitelist
        cands = datasets_whitelist

    if len(blacklist) > 0:
        #print "check blacklist 1 ",whitelist,blacklist
        datasets_blacklist = []
        for d in cands:
            found = False
            for bl in blacklist:
                bl_re = re.compile(bl)
                match = bl_re.search(d)
                if match:
                    found = True
                    break
            if not found:
                datasets_blacklist.append(d)
        cands = datasets_blacklist

    run = getRun(cands)

    for c in cands:
        resultdir = os.path.join(multicrabdir,c,"results")
        if os.path.exists(resultdir):
            datasets.append(Dataset(os.path.join(multicrabdir,c),run))

    return datasets

def getPileup(datasets):
    pileup_data = ROOT.TH1F("pileup_data","",100,0,100)
    for d in datasets:
        if d.isData:
            #print d.name
            #for i in range(1,d.pileup.GetNbinsX()):
            #    print "check pu bin ",i,d.pileup.GetBinContent(i)
            if hasattr(d, 'pileup'):
                pileup_data.Add(d.pileup)
    #print "check pileup_data",pileup_data.GetEntries()
    #sys.exit()
    return pileup_data

def loadLuminosity(multicrabdir,datasets):
    lumisum = 0
    lumijson = open(os.path.join(multicrabdir,"lumi.json"),'r')
    data = json.load(lumijson)
    for d in datasets:
        if d.name in data.keys():
            d.lumi = data[d.name]
            lumisum += d.lumi
    lumijson.close()
    return lumisum

def eventloop(year,outputdir,dataset,pileup_data,lumi,txt,lock=None):

    #print "check eventloop input",year,outputdir,dataset.name,lumi
    subdir = os.path.join(outputdir,dataset.name)
    if not dataset.isData:
        subdir+="_"+dataset.run
    if not os.path.exists(subdir):
        os.mkdir(subdir)
        os.mkdir(os.path.join(subdir,"results"))

    ROOT.gSystem.Load("lib/libAnalysis.so")
    
    tchain = ROOT.TChain("Events")
    tchain.SetCacheSize(10000000) # Set cache size to 10 MB (somehow it is not automatically set contrary to ROOT docs)
    for f in dataset.getFileNames():
        tchain.Add(f)

    if tchain.GetEntries() == 0:
        return

    if not dataset.isData:
        pileup_mc = dataset.pileup
    
    inputList = ROOT.TList()
    inputList.Add(ROOT.TNamed("outputdir",os.path.join(subdir,"results")))
    inputList.Add(ROOT.TNamed("name",dataset.name))
    inputList.Add(ROOT.TNamed("year","%s"%year))
    inputList.Add(ROOT.TNamed("lumi","%s"%dataset.lumi))
    inputList.Add(ROOT.TNamed("leptonflavor","%s"%LEPTONFLAVOR))

    if(dataset.isData):
        inputList.Add(ROOT.TNamed("isData","1"))
        inputList.Add(ROOT.TNamed("pileupfile",pileupfile[year]))
    else:
        inputList.Add(ROOT.TNamed("isData","0"))
        inputList.Add(pileup_data)
        inputList.Add(pileup_mc)
    inputList.Add(dataset.getSkimCounter())
    #if "DoubleMuon" in dataset.name or not dataset.isData:
    if LEPTONFLAVOR == 13:
        inputList.Add(ROOT.TNamed("trigger","HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL"))

        inputList.Add(ROOT.TNamed("trigger","HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Mu19"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Mu17"))
        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu24_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_DoubleIsoMu17_eta2p1_noDzCut"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_DoubleIsoMu17_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu17_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu22_eta2p1"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_DoubleMu18NoFiltersNoVtx_v5"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_IsoMu22_v5"))
#        inputList.Add(ROOT.TNamed("trigger","HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8"))
    #if "DoubleEG" in dataset.name or not dataset.isData:
    if LEPTONFLAVOR == 11:
        inputList.Add(ROOT.TNamed("trigger","HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Ele17_Ele12_CaloIdL_TrackIdL_IsoVL"))
        inputList.Add(ROOT.TNamed("trigger","HLT_Ele17_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"))

    inputList.Add(ROOT.TNamed("Filters","Flag_HBHENoiseFilter"))
    inputList.Add(ROOT.TNamed("Filters","Flag_HBHENoiseIsoFilter"))
#    inputList.Add(ROOT.TNamed("Filters","Flag_CSCTightHaloFilter"))
    inputList.Add(ROOT.TNamed("Filters","Flag_EcalDeadCellTriggerPrimitiveFilter"))
    inputList.Add(ROOT.TNamed("Filters","Flag_goodVertices"))
    inputList.Add(ROOT.TNamed("Filters","Flag_eeBadScFilter"))
    inputList.Add(ROOT.TNamed("Filters","Flag_globalTightHalo2016Filter"))
    inputList.Add(ROOT.TNamed("Filters","Flag_BadPFMuonFilter"))
    inputList.Add(ROOT.TNamed("Filters","Flag_BadChargedCandidateFilter"))

    for key in dataset.histograms.keys():
        inputList.Add(dataset.histograms[key])
                      
    tselector = ROOT.Analysis()#("HLT_IsoMu27||HLT_Mu45_eta2p1") #ROOT.TSelector()
    tselector.SetInputList(inputList)

    if lock != None:
        lock.acquire()
        print
        print txt
        print dataset.name
        print "  Process ID",os.getpid()
        print "  Year",year
        print "  Lepton",LEPTONFLAVOR
        print "  Trigger OR:"
        for i in range(len(inputList)):
            if inputList.At(i).GetName() == "trigger":
                print "    %s"%inputList.At(i).GetTitle()
        lock.release()
    
    if maxEvents > 0:
        tchain.Process(tselector,"",maxEvents)
    else:
        tchain.Process(tselector)
    
    outputList = tselector.GetOutputList()

    if tselector.GetOutputList().FindObject("unweighted counters"):
        counter_unweighted = tselector.GetOutputList().FindObject("unweighted counters")
        counter_weighted   = tselector.GetOutputList().FindObject("weighted counters")

        if lock != None:
            lock.acquire()
        print
        print txt
        print dataset.name
        print "  Process ID",os.getpid()
        printBothCounters(counter_unweighted,counter_weighted)
        if tselector.GetOutputList().FindObject("jetCounter_all"):
            counter_jets_all       = tselector.GetOutputList().FindObject("jetCounter_all")
            counter_jets_btag  = tselector.GetOutputList().FindObject("jetCounter_btag")
            counter_jets_ctag  = tselector.GetOutputList().FindObject("jetCounter_ctag")
            counter_jets_quarktag  = tselector.GetOutputList().FindObject("jetCounter_quarktag")
            counter_jets_gluontag  = tselector.GetOutputList().FindObject("jetCounter_gluontag")
            counter_jets_qgtLT0tag = tselector.GetOutputList().FindObject("jetCounter_qgtLT0tag")
            printCounter(counter_jets_all)
            printCounter(counter_jets_btag)
            printCounter(counter_jets_ctag)
            printCounter(counter_jets_quarktag)
            printCounter(counter_jets_gluontag)
            printCounter(counter_jets_qgtLT0tag)

        if lock != None:
            lock.release()
    del outputList
    del tselector

def printCounter(counter):
    print counter.GetName()
    for i in range(1,counter.GetNbinsX()+1):
        line = "    ";
        clabel = "{:25.24}".format(counter.GetXaxis().GetBinLabel(i))
        if len(counter.GetXaxis().GetBinLabel(i)) == 0:
            print
            continue
        cvalue = str("{:12.0f}".format(counter.GetBinContent(i)))
        line += "%s %s"%(clabel,cvalue)
        print line
    print

def printBothCounters(counter1,counter2):
    for i in range(1,counter1.GetNbinsX()+1):
        line = "    ";
        clabel = "{:25.24}".format(counter1.GetXaxis().GetBinLabel(i))
        if len(counter1.GetXaxis().GetBinLabel(i)) == 0:
            print
            continue
        cvalue1 = str("{:12.0f}".format(counter1.GetBinContent(i)))
        cvalue2 = str("{:12.1f}".format(counter2.GetBinContent(i)))
        line += "%s %s %s"%(clabel,cvalue1,cvalue2)
        print line
    print

def getYear(multicrabdir):
    year_re = re.compile("Run(?P<year>201\d)\S+_")
    match = year_re.search(multicrabdir)
    if match:
        return match.group("year")
    return "-1"

def main():

    if len(sys.argv) == 1:
        usage()
        sys.exit()
        
    multicrabdir = sys.argv[1]
    if not os.path.exists(multicrabdir) or not os.path.isdir(multicrabdir):
        usage()
        sys.exit()
    year = getYear(multicrabdir)
    #print "check year",year

    time = datetime.datetime.now().strftime("%Y%m%dT%H%M")
    lepton = "_"
    if LEPTONFLAVOR == 11:
        lepton+="Electron"
    if LEPTONFLAVOR == 13:
        lepton+="Muon"
    outputmulticrab = os.path.basename(os.path.abspath(multicrabdir))+"_processed"+time+lepton
    if not os.path.exists(outputmulticrab):
        os.mkdir(outputmulticrab)

    blacklist = []
    whitelist = []
#    whitelist = ["DY1JetsToLL_M_50_ext1","DoubleMuon_Run2017D"] #"SingleMuon_Run2017B_31Mar2018_v1_297050_299329","TTJets"]
#    whitelist = ["DoubleMuon_Run2016C","DY1JetsToLL_M_10"] #"SingleMuon_Run2017B_31Mar2018_v1_297050_299329","TTJets"]
#    whitelist = ["DoubleMuon_Run2017B"]
    if LEPTONFLAVOR == 13:
        whitelist = ["DoubleMuon","DY1","DY2","DY3","DY4","TTJet"]
    if LEPTONFLAVOR == 11:
        whitelist = ["DoubleEG","EGamma","DY1","DY2","DY3","DY4","TTJet"]
#        whitelist = ["DoubleEG_Run2016C","DY1JetsToLL_M_50"]
#    whitelist = ["DoubleMuon_Run2016G","DoubleMuon_Run2016H","DY1","DY2","DY3","DY4"]
#    whitelist = ["DoubleMuon_Run2016G","DoubleMuon_Run2016H","JetsToLL_M_50"]
#    whitelist = ["DY1JetsToLL_M_50","DoubleMuon_Run201\dC"]
#    whitelist = ["DY1JetsToLL_M_50","DoubleEG_Run201\dC"]
#    whitelist = ["DY1JetsToLL_M_50","EGamma_Run201\dC"]
#    whitelist = ["DY3JetsToLL_M_50","DoubleMuon_Run201\dC"]
    datasets = getDatasets(multicrabdir,whitelist=whitelist,blacklist=blacklist)


    pileup_data = getPileup(datasets)

    lumi = loadLuminosity(multicrabdir,datasets)

    import time
    t0 = time.time()


    pool = multiprocessing.Pool(MAX_WORKERS)
    pids = []
    results = []
    lock = multiprocessing.Lock()
    for i,d in enumerate(datasets):
        txt = "Dataset %s/%s"%(i+1,len(datasets))
        if MULTIPROCESSING:
            p = multiprocessing.Process(target=eventloop, args=[year,outputmulticrab,d,pileup_data,lumi,txt,lock])
            p.start()
            pids.append(p.pid)

#            results.append(pool.apply_async(func=eventloop, args=[outputmulticrab,d,pileup_data,lumi,txt]))
        else:
            eventloop(outputmulticrab,d,pileup_data,lumi,txt)

#    if MULTIPROCESSING:
#        pool.close()
#        del results
#        pool.join()
#        print "check pool.join"
#    output = [p.get() for p in results]

    if MULTIPROCESSING:
        import psutil
        nalive = len(pids)
        print "\rProcesses running",nalive,
        i = 0

        os.system('setterm -cursor off')
        while nalive > 0:
            al = 0
            for pid in pids:
                if psutil.Process(pid).status() != psutil.STATUS_ZOMBIE:
                    al = al+1
            nalive = al
            dt = time.time()-t0
            lock.acquire()
            sys.stdout.write("\rProcesses running %s/%s, time used %s min %s s             "%(nalive,len(pids),int(dt/60),int(dt%60)))
            sys.stdout.flush()
            lock.release()
    else:
        dt = time.time()-t0
        sys.stdout.write("\rFinished. Runtime used %s min %s s             "%(int(dt/60),int(dt%60)))
        sys.stdout.flush()
    print
        
    print "Output written in",outputmulticrab
    os.system('setterm -cursor on')

if __name__ == "__main__":
#    import cProfile
#    cProfile.run('main()', 'analyse.prof')

    main()
