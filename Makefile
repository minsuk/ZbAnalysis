
BASEDIR := $(shell pwd)
SRCDIR := $(BASEDIR)/src
OBJDIR := $(BASEDIR)/obj
LIBDIR := $(BASEDIR)/lib


ROOTINC := $(shell root-config --cflags)
ROOTLIBS := $(shell root-config --glibs)

INC := -I$(SRCDIR) $(ROOTINC)

CXXFLAGS := -O2 -Wall -Wno-unused-local-typedefs -fPIC -D_REENTRANT $(ROOTINC) $(INC)

LDFLAGS := $(ROOTLIBS) -lProof -lGenVector -lTreePlayer -Llib -lTMVA


FILES_SRC := $(wildcard $(SRCDIR)/*.cc $(SRCDIR)/*.C)
FILES_OBJ := $(patsubst $(SRCDIR)%,$(OBJDIR)%,$(FILES_SRC:.cc=.o))

LIBNAME := libAnalysis.so

LIBPATH := $(LIBDIR)/$(LIBNAME)


CMSMODULES = CondFormats/JetMETObjects
CMSLIBS := $(addprefix $(LIBDIR)/lib, $(addsuffix .so, $(subst /,,$(CMSMODULES))))


CMSSRCDIR := $(BASEDIR)/src/*/*/src
CMSOBJDIR := $(BASEDIR)/obj/*/*/src
CMSSRC := $(wildcard $(CMSSRCDIR)/*.cc)
CMSOBJ := $(patsubst $(CMSSRCDIR)%,$(CMSOBJDIR)%,$(CMSSRC:.cc=.o))
CMSINC := -I$(BASEDIR)/src/*/*/interface

#CMSLIBS := $(LIBDIR)/libCondFormatsJetMETObjects.so
#STEP1 = 

FILES_OBJ += $(CMSOBJ)
#INC += $(CMSINC)

all:
#	${MAKE} CMSLIBS
	$(MAKE) $(SRCDIR)/analysisdict.cc
#	$(MAKE) COMPILE
	$(MAKE) $(LIBPATH)

#CMSLIBS: ${CMSOBJ}
#	mkdir -p ${CMSOBJDIR}

$(CMSLIBS): $(LIBDIR)
	@echo $(CXX) $(CMSOBJ) -shared -o $@

COMPILE: $(FILES_OBJ)

$(OBJDIR)/%.o: $(SRCDIR)/%.cc $(OBJDIR)
	$(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

$(LIBPATH): $(FILES_OBJ) $(LIBDIR)
	$(CXX) $(FILES_OBJ) $(LDFLAGS) -shared -o $@

$(LIBDIR):
	mkdir $@
$(OBJDIR):
	mkdir $@

$(SRCDIR)/analysisdict.cc: src/LinkDef.h
#	rootcint -f $@ -c $(INC) Pileup.h LinkDef.h
	rootcint -f $@ -c $(INC) Analysis.h LinkDef.h
	cp $(SRCDIR)/analysisdict_rdict.pcm $(LIBDIR)

clean:
	rm -f $(FILES_OBJ) $(SRCDIR)/analysisdict.cc

test:
#	make $(SRCDIR)/analysisdict.cc
#	echo $(FILES_OBJ)
	make ${CMSLIBS}
#	ls ${CMSOBJ}
