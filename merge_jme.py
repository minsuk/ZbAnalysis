#!/usr/bin/env python

import sys
import os
import re

def sort(filenames):
    subgroups = {}
    fn_re = re.compile("jet_response\S+(?P<alpha>alpha\d+)_(?P<eta>eta_\d+_\d+)\S*\.root") #jet_response_gluontag_alpha25_eta_13_19_PSWeight2.root
    for f in filenames:
        match = fn_re.search(f)
        if match:
            alpha = match.group("alpha")
            if alpha not in subgroups.keys():
                subgroups[alpha] = []
            subgroups[alpha].append(f)
    return subgroups

def main():

    filenames = sys.argv[1:]
    print len(filenames)

    fngroups = sort(filenames)
    for k in fngroups.keys():
        cmd = "hadd tmp_%s.root"%k
        for f in fngroups[k]:
            cmd += " "+f
        print cmd
        os.system(cmd)

    final_cmd = "hadd jme_bplusZ_merged_vX.root"
    for k in fngroups.keys():
        final_cmd += " tmp_%s.root"%k
    os.system(final_cmd)

    print "Merged jme_bplusZ_merged_vX.root"
    
if __name__=="__main__":
    main()
