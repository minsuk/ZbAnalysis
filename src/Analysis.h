#ifndef Analysis_h
#define Analysis_h

#include <iostream>

#include "TDirectory.h"
#include "TLorentzVector.h"
#include "TVector2.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"

#include "BaseSelector.h"
#include "Counters.h"
#include "PileupWeight.h"

#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"

#include <vector>

class Btag{
 public:
  Btag(){};
  ~Btag(){};

  void set(std::string year,double l, double m, double t){
    ll[year] = l;
    mm[year] = m;
    tt[year] = t;
  }
  bool  exists(std::string year){
    if(ll.find(year) != ll.end()) return true;
    return false;
  }
  double loose(std::string year){
    if(ll.find(year) != ll.end()) return ll[year];
    else printWarning(year);
    return -1;
  }
  double medium(std::string year){
    if(mm.find(year) != mm.end()) return mm[year];
    else printWarning(year);
    return -1;
  }
  double tight(std::string year){
    if(tt.find(year) != tt.end()) return tt[year];
    else printWarning(year);
    return -1;
  }

 private:
  void printWarning(std::string year){
    std::cout << "No btagging WP found for " << year << std::endl;
    exit(1);
  }
  std::map<std::string,double> ll;
  std::map<std::string,double> mm;
  std::map<std::string,double> tt;
};


class Analysis: public BaseSelector {
public:
  explicit Analysis();
  virtual ~Analysis();

  virtual void Begin(TTree*) override;
  virtual void SlaveBegin(TTree*) override;
  //  virtual void setupBranches(BranchManager& branchManager);
  virtual bool Process(Long64_t entry) override;
  virtual void Terminate() override;
  
  virtual void Init(TTree*) override;
  
private:

  Counters* cMain;
  Counters* cJetLabel;
  Counters* cJetLabel_btagDeepBtight;
  Counters* cJetLabel_btagDeepCtight;
  Counters* cJetLabel_gluontag;
  Counters* cJetLabel_quarktag;
  Counters* cJetLabel_qgtLT0tag;

  float eventWeight;
  PileupWeight* pileupWeight;
  //  std::vector<float> psWeights;
  size_t npsWeights;
  float* psWeights;

  template <class T>
    T GetParameter (const char* parameterName);
  std::vector<std::string> GetParameters (const char*);
    
  void setupTriggerBranches(TTree*);
  void setupMETCleaningBranches(TTree*);
  void setupGenLevelBranches(TTree*);
  void setupBranches(TTree*);

  bool getTriggerDecision();
  bool triggeredBy(std::string);
  bool METCleaning();

  std::vector<std::string> cuts;

  std::vector<TLorentzVector> getMuons(int charge);
  std::vector<TLorentzVector> getElectrons(int charge);
  std::vector<TLorentzVector> getGenParticles(int pid);

  bool passElectronID(int iElectron);
  //std::vector<TLorentzVector> getJets();
  std::vector<int> getJetIndexList();
  TLorentzVector getJet(int);
  TLorentzVector getGenJet(int);
  std::vector<int> drClean(std::vector<int> jetIndices,std::vector<TLorentzVector> compare, double cone=0.4);
  std::vector<int> jetId(std::vector<int> jetIndices);
  int getLeadingJetIndex(std::vector<int> jets, size_t nth=0);
  //  std::vector<TLorentzVector> drClean(std::vector<TLorentzVector> orig, std::vector<TLorentzVector> compare, double cone=0.4);
  TLorentzVector getZboson(std::vector<TLorentzVector> leptonsPlus,std::vector<TLorentzVector> leptonsMinus);
  //  TLorentzVector selectLeadingJet(std::vector<TLorentzVector> jets, size_t nth=0);
  double alphaCut(double zpt);
  TVector3 recalculateMET(std::vector<int> jets, float rho);
  bool passing(double, std::string);
  bool exists(const std::string&);
  int getJetFlavour(int jetIndex);
  //size_t getEtaBin(double);

  const char* datasetName;
  const char* year;

  bool isData;

  double lumi;

  int leptonflavor;

  std::vector<std::string> triggerNames;
  Bool_t* triggerBit;

  std::vector<std::string> filterFlags;
  Bool_t* filter;


  std::vector<TLorentzVector> leptonsPlus;
  std::vector<TLorentzVector> leptonsMinus;

  JetCorrectorParameters *l1rc;
  JetCorrectorParameters *l1;
  JetCorrectorParameters *l2;
  JetCorrectorParameters *l3;
  JetCorrectorParameters *l2l3;
  FactorizedJetCorrector* jec_l1rc;
  FactorizedJetCorrector* jec_l1;
  FactorizedJetCorrector* jec_noL2L3;
  FactorizedJetCorrector* jec_withL2L3;

  
  TFile* fOUT;

  TH1F* h_njets;
  TH1F* h_njets_sele;

  TH1F* h_Mu;
  TProfile* h_RhoVsMu;
  TProfile* h_NpvVsMu;
  TH1F* h_Mu0;
  TProfile* h_RhoVsMu0;
  TProfile* h_NpvVsMu0;

  TH1F* h_Jet_jetId;
  TH1F* h_Jet_electronIdx1;
  TH1F* h_Jet_electronIdx2;
  TH1F* h_Jet_muonIdx1;
  TH1F* h_Jet_muonIdx2;
  TH1F* h_Jet_nElectrons;
  TH1F* h_Jet_nMuons;
  TH1F* h_Jet_puId;

  TH1F* h_jet1pt;
  TH1F* h_jet1eta;
  TH1F* h_jet2pt;
  TH1F* h_jet2eta;

  TH1F* h_phibb;

  TProfile* h_JetPt_chHEF;
  TProfile* h_JetPt_neHEF;
  TProfile* h_JetPt_neEmEF;
  TProfile* h_JetPt_chEmEF;
  TProfile* h_JetPt_muEF;

  TH1F* h_jet1cpt;
  TH1F* h_jet1ceta;
  TH1F* h_jet2cpt;
  TH1F* h_jet2ceta;

  TH2F* h_jet2etapt;

  TH1F* h_Zpt;
  TH1F* h_Zpt_a10;
  TH1F* h_Zpt_a15;
  TH1F* h_Zpt_a20;
  TH1F* h_Zpt_a30;
  TH1F* h_Zpt_a40;
  TH1F* h_Zpt_a50;
  TH1F* h_Zpt_a60;
  TH1F* h_Zpt_a80;
  TH1F* h_Zpt_a100;

  TH1F* h_alpha;
  TH1F** h_alphaZpt;

  TH1F* h_RpT;
  TH1F* h_RMPF;

  TH2F* h_pTGenJet_JESRpTGen;
  TH2F* h_pTGenJet_JESRpTGen_genb;
  TH2F* h_pTGenJet_JESRpTGen_genc;
  TH2F* h_pTGenJet_JESRpTGen_geng;
  TH2F* h_pTGenJet_JESRpTGen_genuds;

  TH2F* h_pTGenJet_JESRMPFGen;
  TH2F* h_pTGenJet_JESRMPFGen_genb;
  TH2F* h_pTGenJet_JESRMPFGen_genc;
  TH2F* h_pTGenJet_JESRMPFGen_geng;
  TH2F* h_pTGenJet_JESRMPFGen_genuds;

  TProfile* hprof_pTGenJet_JESRpTGen;
  TProfile* hprof_pTGenJet_JESRpTGen_genb;
  TProfile* hprof_pTGenJet_JESRpTGen_genc;
  TProfile* hprof_pTGenJet_JESRpTGen_geng;
  TProfile* hprof_pTGenJet_JESRpTGen_genuds;

  TProfile* hprof_pTGenJet_JESRMPFGen;
  TProfile* hprof_pTGenJet_JESRMPFGen_genb;
  TProfile* hprof_pTGenJet_JESRMPFGen_genc;
  TProfile* hprof_pTGenJet_JESRMPFGen_geng;
  TProfile* hprof_pTGenJet_JESRMPFGen_genuds;

  TH2F* h_pTGenZ_JESRpTGen;
  TH2F* h_pTGenZ_JESRpTGen_genb;
  TH2F* h_pTGenZ_JESRpTGen_genc;
  TH2F* h_pTGenZ_JESRpTGen_geng;
  TH2F* h_pTGenZ_JESRpTGen_genuds;

  TH2F* h_pTGenZ_JESRMPFGen;
  TH2F* h_pTGenZ_JESRMPFGen_genb;
  TH2F* h_pTGenZ_JESRMPFGen_genc;
  TH2F* h_pTGenZ_JESRMPFGen_geng;
  TH2F* h_pTGenZ_JESRMPFGen_genuds;

  TH2F* h_alpha_RpT;
  TH2F* h_alpha_RMPF;
  TH2F* h_alpha_RpT_genb;
  TH2F* h_alpha_RMPF_genb;
  
  TH2F* h_alpha_RpT_btagCSVV2loose;
  TH2F* h_alpha_RMPF_btagCSVV2loose;
  TH2F* h_alpha_RpT_btagCSVV2medium;
  TH2F* h_alpha_RMPF_btagCSVV2medium;
  TH2F* h_alpha_RpT_btagCSVV2tight;
  TH2F* h_alpha_RMPF_btagCSVV2tight;
  TH2F* h_alpha_RpT_btagCSVV2loose_genb;
  TH2F* h_alpha_RMPF_btagCSVV2loose_genb;
  TH2F* h_alpha_RpT_btagCSVV2medium_genb;
  TH2F* h_alpha_RMPF_btagCSVV2medium_genb;
  TH2F* h_alpha_RpT_btagCSVV2tight_genb;
  TH2F* h_alpha_RMPF_btagCSVV2tight_genb;
  
  TH2F* h_alpha_RpT_btagCMVAloose;
  TH2F* h_alpha_RMPF_btagCMVAloose;
  TH2F* h_alpha_RpT_btagCMVAmedium;
  TH2F* h_alpha_RMPF_btagCMVAmedium;
  TH2F* h_alpha_RpT_btagCMVAtight;
  TH2F* h_alpha_RMPF_btagCMVAtight;
  TH2F* h_alpha_RpT_btagCMVAloose_genb;
  TH2F* h_alpha_RMPF_btagCMVAloose_genb;
  TH2F* h_alpha_RpT_btagCMVAmedium_genb;
  TH2F* h_alpha_RMPF_btagCMVAmedium_genb;
  TH2F* h_alpha_RpT_btagCMVAtight_genb;
  TH2F* h_alpha_RMPF_btagCMVAtight_genb;

  TH2F* h_alpha_RpT_btagDeepBloose;
  TH2F* h_alpha_RMPF_btagDeepBloose;
  TH2F* h_alpha_RpT_btagDeepBmedium;
  TH2F* h_alpha_RMPF_btagDeepBmedium;
  TH2F* h_alpha_RpT_btagDeepBtight;
  TH2F* h_alpha_RMPF_btagDeepBtight;
  TH2F* h_alpha_RpT_btagDeepBloose_genb;
  TH2F* h_alpha_RMPF_btagDeepBloose_genb;
  TH2F* h_alpha_RpT_btagDeepBmedium_genb;
  TH2F* h_alpha_RMPF_btagDeepBmedium_genb;
  TH2F* h_alpha_RpT_btagDeepBtight_genb;
  TH2F* h_alpha_RMPF_btagDeepBtight_genb;

  TH2F* h_alpha_RpT_btagDeepFlavBloose;
  TH2F* h_alpha_RMPF_btagDeepFlavBloose;
  TH2F* h_alpha_RpT_btagDeepFlavBmedium;
  TH2F* h_alpha_RMPF_btagDeepFlavBmedium;
  TH2F* h_alpha_RpT_btagDeepFlavBtight;
  TH2F* h_alpha_RMPF_btagDeepFlavBtight;
  TH2F* h_alpha_RpT_btagDeepFlavBloose_genb;
  TH2F* h_alpha_RMPF_btagDeepFlavBloose_genb;
  TH2F* h_alpha_RpT_btagDeepFlavBmedium_genb;
  TH2F* h_alpha_RMPF_btagDeepFlavBmedium_genb;
  TH2F* h_alpha_RpT_btagDeepFlavBtight_genb;
  TH2F* h_alpha_RMPF_btagDeepFlavBtight_genb;


  TH2F** h_alpha_RpT_Zpt;
  TH2F** h_alpha_RMPF_Zpt;
  TH2F** h_alpha_RpT_Zpt_genb;
  TH2F** h_alpha_RMPF_Zpt_genb;

  TH2F** h_alpha_RpT_btagCSVV2loose_Zpt;
  TH2F** h_alpha_RMPF_btagCSVV2loose_Zpt;
  TH2F** h_alpha_RpT_btagCSVV2medium_Zpt;
  TH2F** h_alpha_RMPF_btagCSVV2medium_Zpt;
  TH2F** h_alpha_RpT_btagCSVV2tight_Zpt;
  TH2F** h_alpha_RMPF_btagCSVV2tight_Zpt;
  TH2F** h_alpha_RpT_btagCSVV2loose_Zpt_genb;
  TH2F** h_alpha_RMPF_btagCSVV2loose_Zpt_genb;
  TH2F** h_alpha_RpT_btagCSVV2medium_Zpt_genb;
  TH2F** h_alpha_RMPF_btagCSVV2medium_Zpt_genb;
  TH2F** h_alpha_RpT_btagCSVV2tight_Zpt_genb;
  TH2F** h_alpha_RMPF_btagCSVV2tight_Zpt_genb;

  TH2F** h_alpha_RpT_btagDeepBloose_Zpt;
  TH2F** h_alpha_RMPF_btagDeepBloose_Zpt;
  TH2F** h_alpha_RpT_btagDeepBmedium_Zpt;
  TH2F** h_alpha_RMPF_btagDeepBmedium_Zpt;
  TH2F** h_alpha_RpT_btagDeepBtight_Zpt;
  TH2F** h_alpha_RMPF_btagDeepBtight_Zpt;
  TH2F** h_alpha_RpT_btagDeepBloose_Zpt_genb;
  TH2F** h_alpha_RMPF_btagDeepBloose_Zpt_genb;
  TH2F** h_alpha_RpT_btagDeepBmedium_Zpt_genb;
  TH2F** h_alpha_RMPF_btagDeepBmedium_Zpt_genb;
  TH2F** h_alpha_RpT_btagDeepBtight_Zpt_genb;
  TH2F** h_alpha_RMPF_btagDeepBtight_Zpt_genb;

  TH2F** h_alpha_RpT_btagDeepFlavBloose_Zpt;
  TH2F** h_alpha_RMPF_btagDeepFlavBloose_Zpt;
  TH2F** h_alpha_RpT_btagDeepFlavBmedium_Zpt;
  TH2F** h_alpha_RMPF_btagDeepFlavBmedium_Zpt;
  TH2F** h_alpha_RpT_btagDeepFlavBtight_Zpt;
  TH2F** h_alpha_RMPF_btagDeepFlavBtight_Zpt;
  TH2F** h_alpha_RpT_btagDeepFlavBloose_Zpt_genb;
  TH2F** h_alpha_RMPF_btagDeepFlavBloose_Zpt_genb;
  TH2F** h_alpha_RpT_btagDeepFlavBmedium_Zpt_genb;
  TH2F** h_alpha_RMPF_btagDeepFlavBmedium_Zpt_genb;
  TH2F** h_alpha_RpT_btagDeepFlavBtight_Zpt_genb;
  TH2F** h_alpha_RMPF_btagDeepFlavBtight_Zpt_genb;


  TH2F*  h_Zpt_R_template;
  TH2F*  h_Zpt_mZ_template;

  TH2F** h_Zpt_mZ;
  TH2F** h_Zpt_mZgen;

  TH2F** h_Zpt_RpT;
  TH2F** h_Zpt_RMPF;
  TH2F** h_Zpt_RMPFjet1;
  TH2F** h_Zpt_RMPFjetn;
  TH2F** h_Zpt_RMPFuncl;
  TH2F** h_Zpt_RpT_btagCSVV2loose;
  TH2F** h_Zpt_RMPF_btagCSVV2loose;
  TH2F** h_Zpt_RpT_btagCSVV2medium;
  TH2F** h_Zpt_RMPF_btagCSVV2medium;
  TH2F** h_Zpt_RpT_btagCSVV2tight;
  TH2F** h_Zpt_RMPF_btagCSVV2tight;

  TH2F** h_Zpt_RpT_genb;
  TH2F** h_Zpt_RMPF_genb;
  TH2F** h_Zpt_RMPFjet1_genb;
  TH2F** h_Zpt_RMPFjetn_genb;
  TH2F** h_Zpt_RMPFuncl_genb;
  TH2F** h_Zpt_RpT_btagCSVV2loose_genb;
  TH2F** h_Zpt_RMPF_btagCSVV2loose_genb;
  TH2F** h_Zpt_RpT_btagCSVV2medium_genb;
  TH2F** h_Zpt_RMPF_btagCSVV2medium_genb;
  TH2F** h_Zpt_RpT_btagCSVV2tight_genb;
  TH2F** h_Zpt_RMPF_btagCSVV2tight_genb;

  TH2F** h_Zpt_RpT_genc;
  TH2F** h_Zpt_RMPF_genc;
  TH2F** h_Zpt_RMPFjet1_genc;
  TH2F** h_Zpt_RMPFjetn_genc;
  TH2F** h_Zpt_RMPFuncl_genc;
  TH2F** h_Zpt_RpT_btagCSVV2loose_genc;
  TH2F** h_Zpt_RMPF_btagCSVV2loose_genc;
  TH2F** h_Zpt_RpT_btagCSVV2medium_genc;
  TH2F** h_Zpt_RMPF_btagCSVV2medium_genc;
  TH2F** h_Zpt_RpT_btagCSVV2tight_genc;
  TH2F** h_Zpt_RMPF_btagCSVV2tight_genc;

  TH2F** h_Zpt_RpT_genuds;
  TH2F** h_Zpt_RMPF_genuds;
  TH2F** h_Zpt_RMPFjet1_genuds;
  TH2F** h_Zpt_RMPFjetn_genuds;
  TH2F** h_Zpt_RMPFuncl_genuds;
  TH2F** h_Zpt_RpT_btagCSVV2loose_genuds;
  TH2F** h_Zpt_RMPF_btagCSVV2loose_genuds;
  TH2F** h_Zpt_RpT_btagCSVV2medium_genuds;
  TH2F** h_Zpt_RMPF_btagCSVV2medium_genuds;
  TH2F** h_Zpt_RpT_btagCSVV2tight_genuds;
  TH2F** h_Zpt_RMPF_btagCSVV2tight_genuds;

  TH2F** h_Zpt_RpT_geng;
  TH2F** h_Zpt_RMPF_geng;
  TH2F** h_Zpt_RMPFjet1_geng;
  TH2F** h_Zpt_RMPFjetn_geng;
  TH2F** h_Zpt_RMPFuncl_geng;
  TH2F** h_Zpt_RpT_btagCSVV2loose_geng;
  TH2F** h_Zpt_RMPF_btagCSVV2loose_geng;
  TH2F** h_Zpt_RpT_btagCSVV2medium_geng;
  TH2F** h_Zpt_RMPF_btagCSVV2medium_geng;
  TH2F** h_Zpt_RpT_btagCSVV2tight_geng;
  TH2F** h_Zpt_RMPF_btagCSVV2tight_geng;


  TH2F** h_Zpt_RpT_btagDeepBloose;
  TH2F** h_Zpt_RMPF_btagDeepBloose;
  TH2F** h_Zpt_RpT_btagDeepBmedium;
  TH2F** h_Zpt_RMPF_btagDeepBmedium;
  TH2F** h_Zpt_RpT_btagDeepBtight;
  TH2F** h_Zpt_RMPF_btagDeepBtight;
  TH2F** h_Zpt_RMPFjet1_btagDeepBtight;
  TH2F** h_Zpt_RMPFjetn_btagDeepBtight;
  TH2F** h_Zpt_RMPFuncl_btagDeepBtight;

  TH2F** h_Zpt_RpT_btagDeepBloose_genb;
  TH2F** h_Zpt_RMPF_btagDeepBloose_genb;
  TH2F** h_Zpt_RpT_btagDeepBmedium_genb;
  TH2F** h_Zpt_RMPF_btagDeepBmedium_genb;
  TH2F** h_Zpt_RpT_btagDeepBtight_genb;
  TH2F** h_Zpt_RMPF_btagDeepBtight_genb;

  TH2F** h_Zpt_RpT_btagDeepBloose_genc;
  TH2F** h_Zpt_RMPF_btagDeepBloose_genc;
  TH2F** h_Zpt_RpT_btagDeepBmedium_genc;
  TH2F** h_Zpt_RMPF_btagDeepBmedium_genc;
  TH2F** h_Zpt_RpT_btagDeepBtight_genc;
  TH2F** h_Zpt_RMPF_btagDeepBtight_genc;

  TH2F** h_Zpt_RpT_btagDeepBloose_genuds;
  TH2F** h_Zpt_RMPF_btagDeepBloose_genuds;
  TH2F** h_Zpt_RpT_btagDeepBmedium_genuds;
  TH2F** h_Zpt_RMPF_btagDeepBmedium_genuds;
  TH2F** h_Zpt_RpT_btagDeepBtight_genuds;
  TH2F** h_Zpt_RMPF_btagDeepBtight_genuds;

  TH2F** h_Zpt_RpT_btagDeepBloose_geng;
  TH2F** h_Zpt_RMPF_btagDeepBloose_geng;
  TH2F** h_Zpt_RpT_btagDeepBmedium_geng;
  TH2F** h_Zpt_RMPF_btagDeepBmedium_geng;
  TH2F** h_Zpt_RpT_btagDeepBtight_geng;
  TH2F** h_Zpt_RMPF_btagDeepBtight_geng;


  TH2F** h_Zpt_RpT_btagDeepCloose;
  TH2F** h_Zpt_RMPF_btagDeepCloose;
  TH2F** h_Zpt_RpT_btagDeepCmedium;
  TH2F** h_Zpt_RMPF_btagDeepCmedium;
  TH2F** h_Zpt_RpT_btagDeepCtight;
  TH2F** h_Zpt_RMPF_btagDeepCtight;
  TH2F** h_Zpt_RMPFjet1_btagDeepCtight;
  TH2F** h_Zpt_RMPFjetn_btagDeepCtight;
  TH2F** h_Zpt_RMPFuncl_btagDeepCtight;

  TH2F** h_Zpt_RpT_btagDeepCloose_genb;
  TH2F** h_Zpt_RMPF_btagDeepCloose_genb;
  TH2F** h_Zpt_RpT_btagDeepCmedium_genb;
  TH2F** h_Zpt_RMPF_btagDeepCmedium_genb;
  TH2F** h_Zpt_RpT_btagDeepCtight_genb;
  TH2F** h_Zpt_RMPF_btagDeepCtight_genb;

  TH2F** h_Zpt_RpT_btagDeepCloose_genc;
  TH2F** h_Zpt_RMPF_btagDeepCloose_genc;
  TH2F** h_Zpt_RpT_btagDeepCmedium_genc;
  TH2F** h_Zpt_RMPF_btagDeepCmedium_genc;
  TH2F** h_Zpt_RpT_btagDeepCtight_genc;
  TH2F** h_Zpt_RMPF_btagDeepCtight_genc;

  TH2F** h_Zpt_RpT_btagDeepCloose_genuds;
  TH2F** h_Zpt_RMPF_btagDeepCloose_genuds;
  TH2F** h_Zpt_RpT_btagDeepCmedium_genuds;
  TH2F** h_Zpt_RMPF_btagDeepCmedium_genuds;
  TH2F** h_Zpt_RpT_btagDeepCtight_genuds;
  TH2F** h_Zpt_RMPF_btagDeepCtight_genuds;

  TH2F** h_Zpt_RpT_btagDeepCloose_geng;
  TH2F** h_Zpt_RMPF_btagDeepCloose_geng;
  TH2F** h_Zpt_RpT_btagDeepCmedium_geng;
  TH2F** h_Zpt_RMPF_btagDeepCmedium_geng;
  TH2F** h_Zpt_RpT_btagDeepCtight_geng;
  TH2F** h_Zpt_RMPF_btagDeepCtight_geng;

  
  TH2F** h_Zpt_RpT_btagDeepFlavBloose;
  TH2F** h_Zpt_RMPF_btagDeepFlavBloose;
  TH2F** h_Zpt_RpT_btagDeepFlavBmedium;
  TH2F** h_Zpt_RMPF_btagDeepFlavBmedium;
  TH2F** h_Zpt_RpT_btagDeepFlavBtight;
  TH2F** h_Zpt_RMPF_btagDeepFlavBtight;

  TH2F** h_Zpt_RpT_btagDeepFlavBloose_genb;
  TH2F** h_Zpt_RMPF_btagDeepFlavBloose_genb;
  TH2F** h_Zpt_RpT_btagDeepFlavBmedium_genb;
  TH2F** h_Zpt_RMPF_btagDeepFlavBmedium_genb;
  TH2F** h_Zpt_RpT_btagDeepFlavBtight_genb;
  TH2F** h_Zpt_RMPF_btagDeepFlavBtight_genb;

  TH2F** h_Zpt_RpT_btagDeepFlavBloose_genc;
  TH2F** h_Zpt_RMPF_btagDeepFlavBloose_genc;
  TH2F** h_Zpt_RpT_btagDeepFlavBmedium_genc;
  TH2F** h_Zpt_RMPF_btagDeepFlavBmedium_genc;
  TH2F** h_Zpt_RpT_btagDeepFlavBtight_genc;
  TH2F** h_Zpt_RMPF_btagDeepFlavBtight_genc;

  TH2F** h_Zpt_RpT_btagDeepFlavBloose_genuds;
  TH2F** h_Zpt_RMPF_btagDeepFlavBloose_genuds;
  TH2F** h_Zpt_RpT_btagDeepFlavBmedium_genuds;
  TH2F** h_Zpt_RMPF_btagDeepFlavBmedium_genuds;
  TH2F** h_Zpt_RpT_btagDeepFlavBtight_genuds;
  TH2F** h_Zpt_RMPF_btagDeepFlavBtight_genuds;

  TH2F** h_Zpt_RpT_btagDeepFlavBloose_geng;
  TH2F** h_Zpt_RMPF_btagDeepFlavBloose_geng;
  TH2F** h_Zpt_RpT_btagDeepFlavBmedium_geng;
  TH2F** h_Zpt_RMPF_btagDeepFlavBmedium_geng;
  TH2F** h_Zpt_RpT_btagDeepFlavBtight_geng;
  TH2F** h_Zpt_RMPF_btagDeepFlavBtight_geng;

  TH2F** h_Zpt_RpT_gluontag;
  TH2F** h_Zpt_RMPF_gluontag;
  TH2F** h_Zpt_RMPFjet1_gluontag;
  TH2F** h_Zpt_RMPFjetn_gluontag;   
  TH2F** h_Zpt_RMPFuncl_gluontag;   
  TH2F** h_Zpt_RpT_gluontag_genb;
  TH2F** h_Zpt_RMPF_gluontag_genb;
  TH2F** h_Zpt_RpT_gluontag_genc;
  TH2F** h_Zpt_RMPF_gluontag_genc;
  TH2F** h_Zpt_RpT_gluontag_genuds;
  TH2F** h_Zpt_RMPF_gluontag_genuds;
  TH2F** h_Zpt_RpT_gluontag_geng;
  TH2F** h_Zpt_RMPF_gluontag_geng;

  TH2F** h_Zpt_RpT_quarktag;
  TH2F** h_Zpt_RMPF_quarktag;
  TH2F** h_Zpt_RMPFjet1_quarktag;
  TH2F** h_Zpt_RMPFjetn_quarktag;
  TH2F** h_Zpt_RMPFuncl_quarktag;

  TH2F** h_Zpt_RpT_quarktag_genb;
  TH2F** h_Zpt_RMPF_quarktag_genb;
  TH2F** h_Zpt_RpT_quarktag_genc;
  TH2F** h_Zpt_RMPF_quarktag_genc;
  TH2F** h_Zpt_RpT_quarktag_genuds;
  TH2F** h_Zpt_RMPF_quarktag_genuds;
  TH2F** h_Zpt_RpT_quarktag_geng;
  TH2F** h_Zpt_RMPF_quarktag_geng;

  TH2F** h_Zpt_RpT_quarktag_gens;
  TH2F** h_Zpt_RMPF_quarktag_gens;
  TH2F** h_Zpt_RpT_quarktag_genud;
  TH2F** h_Zpt_RMPF_quarktag_genud;

  Float_t* bins_alpha;
  size_t nbins_alpha;

  std::vector<std::string> bins_eta;
};

#endif
