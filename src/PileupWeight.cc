#include "PileupWeight.h"
#include <iostream>

PileupWeight::PileupWeight(TH1F* hData, TH1F* hMC){
  calculateWeights(hData,hMC);
}

PileupWeight::~PileupWeight() {}

double PileupWeight::getWeight(int pileup){
  if(h_weight == nullptr)
    std::cout << "PileupWeight enabled, but no PileupWeights in multicrab!" << std::endl;

  int bin = h_weight->GetXaxis()->FindBin( pileup );
  return h_weight->GetBinContent( bin );
}

void PileupWeight::calculateWeights(TH1F* hData, TH1F* hMC){
  h_data = hData;
  h_mc   = hMC;
  //std::cout << "check PileupWeight::calculateWeights " << hData << " " << hMC << std::endl;
  noData = false;
  if(h_data == nullptr)
    std::cout << "Did not find data pileup distributions" << std::endl;

  if(h_mc == nullptr)
    std::cout << "Did not find mc pileup distributions" << std::endl;

  h_data->Scale(1.0/h_data->Integral());
  h_mc->Scale(1.0/h_mc->Integral());

  if(h_data->Integral() < 0.99 || h_data->Integral() > 1.01) {
    std::cout << "Problem with data pileup " << h_data->Integral() << std::endl;
    exit(1);
  }
  if(h_mc->Integral() < 0.99 || h_mc->Integral() > 1.01) {
    std::cout << "Problem with mc pileup " << h_mc->Integral() << std::endl;
    exit(1);
  }
  //std::cout << "check data mc integral " << h_data->Integral() << ", " << h_mc->Integral() << std::endl;

  h_weight = (TH1F*)h_data->Clone("lumiWeights");
  h_weight->Divide(h_mc);
  //for (int i = 1; i < h_weight->GetNbinsX()+1; ++i)
  //  std::cout << i << ":" << h_data->GetBinContent(i) << " " << h_mc->GetBinContent(i) << " " << h_weight->GetBinContent(i) << std::endl;
}
