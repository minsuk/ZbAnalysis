#include "Analysis.h"
#include <iostream>
#include <regex>
#include <iomanip>
#include <iterator>

#include "Math/VectorUtil.h"
#include "TMath.h"
#include "TVector3.h"

#include "parsePileUpJSON2.h"

Analysis::Analysis()
  : BaseSelector()
{
  //std::cout << "check Analysis::Analysis " << dummy << std::endl;
  cMain = new Counters("main counter");
  cJetLabel = new Counters("jet label");
  cJetLabel_btagDeepBtight = new Counters("jet label btagDeepBtight");
  cJetLabel_btagDeepCtight = new Counters("jet label btagDeepCtight");
  cJetLabel_gluontag = new Counters("jet label gluontag");
  cJetLabel_quarktag = new Counters("jet label quarktag");
  cJetLabel_qgtLT0tag = new Counters("jet label q/gtag failed (-1)");

  //  TList* inputList = this->GetInputList();
  //  std::cout << "check name " << inputList->GetSize() << std::endl;
  //  for(const auto&& obj: *GetListOfPrimitives())
  //    obj->Write();
  /*
  TNamed* name = (TNamed*)inputList->FindObject("name");
  const char* datasetName = name->GetTitle();
  std::cout << "check name " << datasetName << std::endl;
  */
  //  HLT_test = {fReader, "HLT_IsoMu27"};
  //fOUT = TFile::Open("histograms.root","RECREATE");
}

Analysis::~Analysis() {
  delete l1rc;
  delete l1;
  delete l2;
  delete l3;
  delete l2l3;
  delete jec_noL2L3;
  delete jec_withL2L3;
  /*
  cMain->print();
  
  fOUT->cd();
  fOUT->mkdir("analysis");
  fOUT->cd("analysis");

  h_RpT->Write();
  h_RMPF->Write();
  fOUT->Write();
  fOUT->ls();
  fOUT->Close();
  */
}

void Analysis::Begin(TTree* tree) {
  /*
  TList* inputList = this->GetInputList();
  std::cout << "check B size " << inputList->GetSize() << std::endl;
  TNamed* name = (TNamed*)inputList->FindObject("name");
  const char* datasetName = name->GetTitle();
  std::cout << "check name " << datasetName << std::endl;
  */

  //  HLT_test = {fReader, "HLT_IsoMu27"};
  //h_ZbosonPt   = new TH1F('Z pt','',20,0,200);
  //h_ZbosonPt_b = new TH1F('Z pt (b jet)','',20,0,200);
  //h_alpha      = new TH1F('alpha','',100,0,1);
  //h_bdiscr     = new TH1F('b discr','',100,0,1);

  datasetName = this->GetParameter<TNamed*>("name")->GetTitle();
  year = this->GetParameter<TNamed*>("year")->GetTitle();
  //std::cout << "check dset,year " << datasetName << " " << year << std::endl;  
  std::string resultDir = std::string(this->GetParameter<TNamed*>("outputdir")->GetTitle());
  isData = (std::string(this->GetParameter<TNamed*>("isData")->GetTitle()) == '1');
  TH1F* pu_data = (TH1F*)this->GetParameter<TH1F*>("pileup_data");
  TH1F* pu_mc   = (TH1F*)this->GetParameter<TH1F*>("pileup_mc");
  if(!isData) pileupWeight = new PileupWeight(pu_data,pu_mc);

  TH1F* skimCounter = (TH1F*)this->GetParameter<TH1F*>("skimCounter");
  for(int i = 1; i < skimCounter->GetNbinsX()+1; ++i){
    cMain->book(skimCounter->GetXaxis()->GetBinLabel(i),skimCounter->GetBinContent(i));
    //std::cout << "  " << skimCounter->GetXaxis()->GetBinLabel(i) << " " << skimCounter->GetBinContent(i) << std::endl;
  }

  triggerNames = this->GetParameters("trigger");
  //std::cout << "  Trigger OR:" << std::endl;
  //for(size_t i = 0; i < triggerNames.size(); ++i){
  //  std::cout << "    " << triggerNames[i] << std::endl;
  //}

  eventWeight = 1;
  lumi = strtof(this->GetParameter<TNamed*>("lumi")->GetTitle(),0);
  //std::cout << "check lumi " << lumi << std::endl;
  //if(!isData) {
      //float lumi = strtof(this->GetParameter<TNamed*>("lumi")->GetTitle(),0);
      //float xsec = strtof(this->GetParameter<TNamed*>("xsec")->GetTitle(),0);
      //    std::string s_xsec = strtof(this->GetParameter<TNamed*>("xsec")->GetTitle(),0);
      //int nevents = skimCounter->GetBinContent(1);
      //eventWeight = lumi*xsec/nevents;
      //std::cout << "check lumi,xsec,nevents " << lumi << " " << xsec << " " << nevents << std::endl;
      //std::cout << "check eventWeight " << eventWeight << std::endl;
  //}
  if(isData){
    const char* pileupfile = this->GetParameter<TNamed*>("pileupfile")->GetTitle();
    parsePileUpJSON2(std::string(pileupfile));
  }

  leptonflavor = strtof(this->GetParameter<TNamed*>("leptonflavor")->GetTitle(),0);

  npsWeights = 1;
  if(!isData) npsWeights = 5;
  psWeights = new float[npsWeights];

  fOUT = TFile::Open((resultDir+"/histograms.root").c_str(),"RECREATE");
  fOUT->mkdir("analysis");
  fOUT->cd("analysis");

  h_Mu               = new TH1F("Mu","",100,0,100);
  h_RhoVsMu          = new TProfile("RhoVsMu","",100,0,100);
  h_NpvVsMu          = new TProfile("NpvVsMu","",100,0,100);
  h_Mu0              = new TH1F("Mu0","",100,0,100);
  h_RhoVsMu0         = new TProfile("RhoVsMu0","",100,0,100);
  h_NpvVsMu0         = new TProfile("NpvVsMu0","",100,0,100);

  h_njets            = new TH1F("njets","",20,0,20);
  h_njets_sele       = new TH1F("njets_selected","",20,0,20);

  h_Jet_jetId        = new TH1F("Jet_jetId","",10,-1,9);
  h_Jet_electronIdx1 = new TH1F("Jet_electronIdx1","",10,-1,9);
  h_Jet_electronIdx2 = new TH1F("Jet_electronIdx2","",10,-1,9);
  h_Jet_muonIdx1     = new TH1F("Jet_muonIdx1","",10,-1,9);
  h_Jet_muonIdx2     = new TH1F("Jet_muonIdx2","",10,-1,9);
  h_Jet_nElectrons   = new TH1F("Jet_nElectrons","",10,-1,9);
  h_Jet_nMuons       = new TH1F("Jet_nMuons","",10,-1,9);
  h_Jet_puId         = new TH1F("Jet_puId","",10,-1,9);

  //Float_t zptbins[] = {0,50,100,150,200,250,300,350,400,450,500};
  Float_t zptbins[] = {30, 40, 50, 60, 70, 85, 105, 130, 175, 230, 300, 400, 500, 700, 1000, 1500};
  Int_t  nzptbins = sizeof(zptbins)/sizeof(Float_t) - 1;
  
  //  h_jet1pt   = new TH1F("jet1pt","",nzptbins,zptbins);
  h_jet1pt   = new TH1F("jet1pt","",250,0,250);
  h_jet1eta  = new TH1F("jet1eta","",50,-2.5,2.5);
  h_jet2pt   = new TH1F("jet2pt","",250,0,250);
  h_jet2eta  = new TH1F("jet2eta","",50,-2.5,2.5);

  h_phibb    = new TH1F("phibb","",128,0,6.4);

  Float_t ptbins[] = {1, 15, 21, 28, 37, 49, 64, 84, 114, 153, 196, 272, 330, 395, 468, 548, 686, 846, 1032, 1248, 1588, 2000, 2500, 3103, 3832, 4713, 5777, 7000};


  Int_t  nptbins = sizeof(ptbins)/sizeof(Float_t) - 1;

  h_JetPt_chHEF  = new TProfile("JetPt_chHEF","",nptbins,ptbins);
  h_JetPt_neHEF  = new TProfile("JetPt_neHEF","",nptbins,ptbins);
  h_JetPt_neEmEF = new TProfile("JetPt_neEmE","",nptbins,ptbins);
  h_JetPt_chEmEF = new TProfile("JetPt_chEmEF","",nptbins,ptbins);
  h_JetPt_muEF   = new TProfile("JetPt_muEF","",nptbins,ptbins);

  h_jet1cpt  = new TH1F("jet1cpt","",250,0,250);
  h_jet1ceta = new TH1F("jet1ceta","",50,-2.5,2.5);
  h_jet2cpt  = new TH1F("jet2cpt","",250,0,250);
  h_jet2ceta = new TH1F("jet2ceta","",50,-2.5,2.5);

  h_jet2etapt = new TH2F("jet2etapt","",600,-3.0,3.0,100,0,100);

  h_Zpt      = new TH1F("Zpt","",nzptbins,zptbins);
  h_Zpt_a10  = new TH1F("Zpt_a10","alpha<0.10",nzptbins,zptbins);
  h_Zpt_a15  = new TH1F("Zpt_a15","alpha<0.15",nzptbins,zptbins);
  h_Zpt_a20  = new TH1F("Zpt_a20","alpha<0.20",nzptbins,zptbins);
  h_Zpt_a30  = new TH1F("Zpt_a30","alpha<0.30",nzptbins,zptbins);
  h_Zpt_a40  = new TH1F("Zpt_a40","alpha<0.40",nzptbins,zptbins);
  h_Zpt_a50  = new TH1F("Zpt_a50","alpha<0.50",nzptbins,zptbins);
  h_Zpt_a60  = new TH1F("Zpt_a60","alpha<0.60",nzptbins,zptbins);
  h_Zpt_a80  = new TH1F("Zpt_a80","alpha<0.80",nzptbins,zptbins);
  h_Zpt_a100 = new TH1F("Zpt_a100","alpha<1.0",nzptbins,zptbins);

  //Float_t bins_alpha_[] = {0.0,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.8,1.0,1.2};
  Float_t bins_alpha_[] = {0.0,0.001,0.05,0.1,0.15,0.2,0.25,0.3,0.4,0.5,0.6,0.8,1.0,1.2};
  nbins_alpha = sizeof(bins_alpha_)/sizeof(Float_t) - 1;

  bins_alpha = new Float_t[nbins_alpha];
  for(size_t i = 0; i < nbins_alpha+1; i++) bins_alpha[i] = bins_alpha_[i];

  //  bins_alpha = bins_alpha_;

  //Float_t binsy[] = {-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0,2.1,2.2,2.3,2.4,2.5};
  //Int_t  nbinsy = sizeof(binsy)/sizeof(Float_t) - 1;

//  Float_t binsy[] = {-0.5,-0.45,-0.4,-0.35,-0.3,-0.25,-0.2,-0.15,-0.1,-0.05,0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95,1,1.05,1.1,1.15,1.2,1.25,1.3,1.35,1.4,1.45,1.5,1.55,1.6,1.65,1.7,1.75,1.8,1.85,1.9,1.95,2,2.05,2.1,2.15,2.2,2.25,2.3,2.35,2.4,2.45,2.5};
  Float_t binsy[] = {-1.51,-1.49,-1.47,-1.45,-1.43,-1.41,-1.39,-1.37,-1.35,-1.33,-1.31,-1.29,-1.27,-1.25,-1.23,-1.21,-1.19,-1.17,-1.15,-1.13,-1.11,-1.09,-1.07,-1.05,-1.03,-1.01,-0.99,-0.97,-0.95,-0.93,-0.91,-0.89,-0.87,-0.85,-0.83,-0.81,-0.79,-0.77,-0.75,-0.73,-0.71,-0.69,-0.67,-0.65,-0.63,-0.61,-0.59,-0.57,-0.55,-0.53,-0.51,-0.49,-0.47,-0.45,-0.43,-0.41,-0.39,-0.37,-0.35,-0.33,-0.31,-0.29,-0.27,-0.25,-0.23,-0.21,-0.19,-0.17,-0.15,-0.13,-0.11,-0.09,-0.07,-0.05,-0.03,-0.01,0.01,0.03,0.05,0.07,0.09,0.11,0.13,0.15,0.17,0.19,0.21,0.23,0.25,0.27,0.29,0.31,0.33,0.35,0.37,0.39,0.41,0.43,0.45,0.47,0.49,0.51,0.53,0.55,0.57,0.59,0.61,0.63,0.65,0.67,0.69,0.71,0.73,0.75,0.77,0.79,0.81,0.83,0.85,0.87,0.89,0.91,0.93,0.95,0.97,0.99,1.01,1.03,1.05,1.07,1.09,1.11,1.13,1.15,1.17,1.19,1.21,1.23,1.25,1.27,1.29,1.31,1.33,1.35,1.37,1.39,1.41,1.43,1.45,1.47,1.49,1.51,1.53,1.55,1.57,1.59,1.61,1.63,1.65,1.67,1.69,1.71,1.73,1.75,1.77,1.79,1.81,1.83,1.85,1.87,1.89,1.91,1.93,1.95,1.97,1.99,2.01,2.03,2.05,2.07,2.09,2.11,2.13,2.15,2.17,2.19,2.21,2.23,2.25,2.27,2.29,2.31,2.33,2.35,2.37,2.39,2.41,2.43,2.45,2.47,2.49,2.51,2.53,2.55,2.57,2.59,2.61,2.63,2.65,2.67,2.69,2.71,2.73,2.75,2.77,2.79,2.81,2.83,2.85,2.87,2.89,2.91,2.93,2.95,2.97,2.99,3.01,3.03,3.05,3.07,3.09,3.11,3.13,3.15,3.17,3.19,3.21,3.23,3.25,3.27,3.29,3.31,3.33,3.35,3.37,3.39,3.41,3.43,3.45,3.47,3.49,3.51};
  UInt_t  nbinsy = sizeof(binsy)/sizeof(Float_t) - 1;
  /*
  Float_t bins_eta_[] = {0.0,1.3,1.9,2.5};
  nbins_eta = sizeof(bins_eta_)/sizeof(Float_t) - 1;
  bins_eta = new Float_t[nbins_eta];
  for(size_t i = 0; i < nbins_eta+1; i++) bins_eta[i] = bins_eta_[i];
  */
  
  /*
  Int_t  nbinsy = 201;
  Float_t vmin = -1.51;
  Float_t step = 0.02;
  Float_t binsy[nbinsy];
  for(Int_t i = 0; i < nbinsy+1; ++i){
    binsy[i] = vmin + step*i;
    std::cout << binsy[i] << ",";
  }
  std::cout << std::endl;
< */
  bins_eta = {"0.0_1.3","1.3_1.9","1.9_2.5","0.0_2.5"};
  cuts = {"0_30","30_50","50_70","70_100","100_140","140_200","200_Inf"};

  h_alpha      = new TH1F("alpha","",nbinsy,binsy);

  h_alphaZpt = new TH1F*[cuts.size()];
  for(size_t i = 0; i < cuts.size(); ++i){
    h_alphaZpt[i] = (TH1F*)h_alpha->Clone(("alpha_Zpt"+cuts[i]).c_str());
  }

  h_RpT        = new TH1F("R_pT","",nbins_alpha,bins_alpha);
  h_RMPF       = new TH1F("R_MPF","",nbins_alpha,bins_alpha);

  h_alpha_RpT  = new TH2F("h_alpha_RpT","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF = new TH2F("h_alpha_RMPF","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_genb  = new TH2F("h_alpha_RpT_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_genb = new TH2F("h_alpha_RMPF_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagCSVV2loose = new TH2F("h_alpha_RpT_btagCSVV2loose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2loose = new TH2F("h_alpha_RMPF_btagCSVV2loose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2medium = new TH2F("h_alpha_RpT_btagCSVV2medium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2medium = new TH2F("h_alpha_RMPF_btagCSVV2medium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2tight = new TH2F("h_alpha_RpT_btagCSVV2tight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2tight = new TH2F("h_alpha_RMPF_btagCSVV2tight","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagCSVV2loose_genb = new TH2F("h_alpha_RpT_btagCSVV2loose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2loose_genb = new TH2F("h_alpha_RMPF_btagCSVV2loose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2medium_genb = new TH2F("h_alpha_RpT_btagCSVV2medium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2medium_genb = new TH2F("h_alpha_RMPF_btagCSVV2medium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCSVV2tight_genb = new TH2F("h_alpha_RpT_btagCSVV2tight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCSVV2tight_genb = new TH2F("h_alpha_RMPF_btagCSVV2tight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagCMVAloose = new TH2F("h_alpha_RpT_btagCMVAloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCMVAloose = new TH2F("h_alpha_RMPF_btagCMVAloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCMVAmedium = new TH2F("h_alpha_RpT_btagCMVAmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCMVAmedium = new TH2F("h_alpha_RMPF_btagCMVAmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagCMVAtight = new TH2F("h_alpha_RpT_btagCMVAtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagCMVAtight = new TH2F("h_alpha_RMPF_btagCMVAtight","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagDeepBloose = new TH2F("h_alpha_RpT_btagDeepBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBloose = new TH2F("h_alpha_RMPF_btagDeepBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepBmedium = new TH2F("h_alpha_RpT_btagDeepBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBmedium = new TH2F("h_alpha_RMPF_DeepBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepBtight = new TH2F("h_alpha_RpT_btagDeepBtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBtight = new TH2F("h_alpha_RMPF_btagDeepBtight","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagDeepBloose_genb = new TH2F("h_alpha_RpT_btagDeepBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBloose_genb = new TH2F("h_alpha_RMPF_btagDeepBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepBmedium_genb = new TH2F("h_alpha_RpT_btagDeepBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBmedium_genb = new TH2F("h_alpha_RMPF_DeepBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepBtight_genb = new TH2F("h_alpha_RpT_btagDeepBtight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepBtight_genb = new TH2F("h_alpha_RMPF_btagDeepBtight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);

  h_alpha_RpT_btagDeepFlavBloose = new TH2F("h_alpha_RpT_btagDeepFlavBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBloose = new TH2F("h_alpha_RMPF_btagDeepFlavBloose","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBmedium = new TH2F("h_alpha_RpT_btagDeepFlavBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBmedium = new TH2F("h_alpha_RMPF_DeepFlavBmedium","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBtight = new TH2F("h_alpha_RpT_btagDeepFlavBtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBtight = new TH2F("h_alpha_RMPF_btagDeepFlavBtight","",nbins_alpha,bins_alpha,nbinsy,binsy);
  
  h_alpha_RpT_btagDeepFlavBloose_genb = new TH2F("h_alpha_RpT_btagDeepFlavBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBloose_genb = new TH2F("h_alpha_RMPF_btagDeepFlavBloose_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBmedium_genb = new TH2F("h_alpha_RpT_btagDeepFlavBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBmedium_genb = new TH2F("h_alpha_RMPF_DeepFlavBmedium_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RpT_btagDeepFlavBtight_genb = new TH2F("h_alpha_RpT_btagDeepFlavBtight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);
  h_alpha_RMPF_btagDeepFlavBtight_genb = new TH2F("h_alpha_RMPF_btagDeepFlavBtight_genb","",nbins_alpha,bins_alpha,nbinsy,binsy);


  h_alpha_RpT_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_Zpt_genb = new TH2F*[cuts.size()];

  h_alpha_RpT_btagCSVV2loose_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagCSVV2loose_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagCSVV2medium_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagCSVV2medium_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagCSVV2tight_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagCSVV2tight_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagCSVV2loose_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagCSVV2loose_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RpT_btagCSVV2medium_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagCSVV2medium_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RpT_btagCSVV2tight_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagCSVV2tight_Zpt_genb = new TH2F*[cuts.size()];

  h_alpha_RpT_btagDeepBloose_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepBloose_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepBmedium_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepBmedium_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepBtight_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepBtight_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepBloose_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepBloose_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepBmedium_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepBmedium_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepBtight_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepBtight_Zpt_genb = new TH2F*[cuts.size()];

  h_alpha_RpT_btagDeepFlavBloose_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBloose_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBmedium_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBmedium_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBtight_Zpt = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBtight_Zpt = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBloose_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBloose_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBmedium_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBmedium_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RpT_btagDeepFlavBtight_Zpt_genb = new TH2F*[cuts.size()];
  h_alpha_RMPF_btagDeepFlavBtight_Zpt_genb = new TH2F*[cuts.size()];

  for(size_t i = 0; i < cuts.size(); ++i){
    h_alpha_RpT_Zpt[i] = (TH2F*)h_alpha_RpT->Clone(("h_alpha_RpT_Zpt"+cuts[i]).c_str());
    h_alpha_RMPF_Zpt[i] = (TH2F*)h_alpha_RMPF->Clone(("h_alpha_RMPF_Zpt"+cuts[i]).c_str());
    h_alpha_RpT_Zpt_genb[i] = (TH2F*)h_alpha_RpT->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_genb").c_str());
    h_alpha_RMPF_Zpt_genb[i] = (TH2F*)h_alpha_RMPF->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_genb").c_str());

    h_alpha_RpT_btagCSVV2loose_Zpt[i] = (TH2F*)h_alpha_RpT_btagCSVV2loose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2loose").c_str());
    h_alpha_RMPF_btagCSVV2loose_Zpt[i] = (TH2F*)h_alpha_RMPF_btagCSVV2loose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2loose").c_str());
    h_alpha_RpT_btagCSVV2medium_Zpt[i] = (TH2F*)h_alpha_RpT_btagCSVV2medium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2medium").c_str());
    h_alpha_RMPF_btagCSVV2medium_Zpt[i] = (TH2F*)h_alpha_RMPF_btagCSVV2medium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2medium").c_str());
    h_alpha_RpT_btagCSVV2tight_Zpt[i] = (TH2F*)h_alpha_RpT_btagCSVV2tight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2tight").c_str());
    h_alpha_RMPF_btagCSVV2tight_Zpt[i] = (TH2F*)h_alpha_RMPF_btagCSVV2tight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2tight").c_str());
    h_alpha_RpT_btagCSVV2loose_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagCSVV2loose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2loose"+"_genb").c_str());
    h_alpha_RMPF_btagCSVV2loose_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagCSVV2loose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2loose"+"_genb").c_str());
    h_alpha_RpT_btagCSVV2medium_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagCSVV2medium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2medium"+"_genb").c_str());
    h_alpha_RMPF_btagCSVV2medium_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagCSVV2medium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2medium"+"_genb").c_str());
    h_alpha_RpT_btagCSVV2tight_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagCSVV2tight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagCSVV2tight"+"_genb").c_str());
    h_alpha_RMPF_btagCSVV2tight_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagCSVV2tight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagCSVV2tight"+"_genb").c_str());

    h_alpha_RpT_btagDeepBloose_Zpt[i] = (TH2F*)h_alpha_RpT_btagDeepBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeeploose").c_str());
    h_alpha_RMPF_btagDeepBloose_Zpt[i] = (TH2F*)h_alpha_RMPF_btagDeepBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBloose").c_str());
    h_alpha_RpT_btagDeepBmedium_Zpt[i] = (TH2F*)h_alpha_RpT_btagDeepBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBmedium").c_str());
    h_alpha_RMPF_btagDeepBmedium_Zpt[i] = (TH2F*)h_alpha_RMPF_btagDeepBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBmedium").c_str());
    h_alpha_RpT_btagDeepBtight_Zpt[i] = (TH2F*)h_alpha_RpT_btagDeepBtight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBtight").c_str());
    h_alpha_RMPF_btagDeepBtight_Zpt[i] = (TH2F*)h_alpha_RMPF_btagDeepBtight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBtight").c_str());
    h_alpha_RpT_btagDeepBloose_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagDeepBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBloose"+"_genb").c_str());
    h_alpha_RMPF_btagDeepBloose_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagDeepBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBloose"+"_genb").c_str());
    h_alpha_RpT_btagDeepBmedium_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagDeepBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBmedium"+"_genb").c_str());
    h_alpha_RMPF_btagDeepBmedium_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagDeepBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBmedium"+"_genb").c_str());
    h_alpha_RpT_btagDeepBtight_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagDeepBtight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepBtight"+"_genb").c_str());
    h_alpha_RMPF_btagDeepBtight_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagDeepBtight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepBtight"+"_genb").c_str());

    h_alpha_RpT_btagDeepFlavBloose_Zpt[i] = (TH2F*)h_alpha_RpT_btagDeepFlavBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBloose").c_str());
    h_alpha_RMPF_btagDeepFlavBloose_Zpt[i] = (TH2F*)h_alpha_RMPF_btagDeepFlavBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBloose").c_str());
    h_alpha_RpT_btagDeepFlavBmedium_Zpt[i] = (TH2F*)h_alpha_RpT_btagDeepFlavBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBmedium").c_str());
    h_alpha_RMPF_btagDeepFlavBmedium_Zpt[i] = (TH2F*)h_alpha_RMPF_btagDeepFlavBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBmedium").c_str());
    h_alpha_RpT_btagDeepFlavBtight_Zpt[i] = (TH2F*)h_alpha_RpT_btagDeepFlavBtight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBtight").c_str());
    h_alpha_RMPF_btagDeepFlavBtight_Zpt[i] = (TH2F*)h_alpha_RMPF_btagDeepFlavBtight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBtight").c_str());
    h_alpha_RpT_btagDeepFlavBloose_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagDeepFlavBloose->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBloose"+"_genb").c_str());
    h_alpha_RMPF_btagDeepFlavBloose_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagDeepFlavBloose->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBloose"+"_genb").c_str());
    h_alpha_RpT_btagDeepFlavBmedium_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagDeepFlavBmedium->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBmedium"+"_genb").c_str());
    h_alpha_RMPF_btagDeepFlavBmedium_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagDeepFlavBmedium->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBmedium"+"_genb").c_str());
    h_alpha_RpT_btagDeepFlavBtight_Zpt_genb[i] = (TH2F*)h_alpha_RpT_btagDeepFlavBtight->Clone(("h_alpha_RpT_Zpt"+cuts[i]+"_btagDeepFlavBtight"+"_genb").c_str());
    h_alpha_RMPF_btagDeepFlavBtight_Zpt_genb[i] = (TH2F*)h_alpha_RMPF_btagDeepFlavBtight->Clone(("h_alpha_RMPF_Zpt"+cuts[i]+"_btagDeepFlavBtight"+"_genb").c_str());
  }
  
  //Float_t massbins[] = {50,60,70,80,90,100,110,120,130,140,150};
  Float_t massbins[] = {70.0,70.5,71.0,71.5,72.0,72.5,73.0,73.5,74.0,74.5,75.0,75.5,76.0,76.5,77.0,77.5,78.0,78.5,79.0,79.5,80.0,80.5,81.0,81.5,82.0,82.5,83.0,83.5,84.0,84.5,85.0,85.5,86.0,86.5,87.0,87.5,88.0,88.5,89.0,89.5,90.0,90.5,91.0,91.5,92.0,92.5,93.0,93.5,94.0,94.5,95.0,95.5,96.0,96.5,97.0,97.5,98.0,98.5,99.0,99.5,100.0,100.5,101.0,101.5,102.0,102.5,103.0,103.5,104.0,104.5,105.0,105.5,106.0,106.5,107.0,107.5,108.0,108.5,109.0,109.5,110.0,110.5,111.0,111.5,112.0,112.5,113.0,113.5,114.0,114.5,115.0,115.5,116.0,116.5,117.0,117.5,118.0,118.5,119.0,119.5,120.0};
  UInt_t  nmassbins = sizeof(massbins)/sizeof(Float_t) - 1;
  h_Zpt_mZ_template = new TH2F("h_Zpt_mZ_temp","",nzptbins,zptbins,nmassbins,massbins);

  h_pTGenJet_JESRpTGen = new TH2F("h_pTGenJet_JESRpTGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRpTGen_genb = new TH2F("h_pTGenJet_JESRpTGen_genb","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_genc = new TH2F("h_pTGenJet_JESRpTGen_genc","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_geng = new TH2F("h_pTGenJet_JESRpTGen_geng","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRpTGen_genuds = new TH2F("h_pTGenJet_JESRpTGen_genuds","",nzptbins,zptbins,nbinsy,binsy); 

  h_pTGenJet_JESRMPFGen = new TH2F("h_pTGenJet_JESRMPFGen","",nzptbins,zptbins,nbinsy,binsy); 
  h_pTGenJet_JESRMPFGen_genb = new TH2F("h_pTGenJet_JESRMPFGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genc = new TH2F("h_pTGenJet_JESRMPFGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_geng = new TH2F("h_pTGenJet_JESRMPFGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenJet_JESRMPFGen_genuds = new TH2F("h_pTGenJet_JESRMPFGen_genuds","",nzptbins,zptbins,nbinsy,binsy);

  hprof_pTGenJet_JESRpTGen = new TProfile("hprof_pTGenJet_JESRpTGen","",nzptbins,zptbins);
  hprof_pTGenJet_JESRpTGen_genb = new TProfile("hprof_pTGenJet_JESRpTGen_genb","",nzptbins,zptbins);
  hprof_pTGenJet_JESRpTGen_genc = new TProfile("hprof_pTGenJet_JESRpTGen_genc","",nzptbins,zptbins);
  hprof_pTGenJet_JESRpTGen_geng = new TProfile("hprof_pTGenJet_JESRpTGen_geng","",nzptbins,zptbins);
  hprof_pTGenJet_JESRpTGen_genuds = new TProfile("hprof_pTGenJet_JESRpTGen_genuds","",nzptbins,zptbins);

  hprof_pTGenJet_JESRMPFGen = new TProfile("hprof_pTGenJet_JESRMPFGen","",nzptbins,zptbins);
  hprof_pTGenJet_JESRMPFGen_genb = new TProfile("hprof_pTGenJet_JESRMPFGen_genb","",nzptbins,zptbins);
  hprof_pTGenJet_JESRMPFGen_genc = new TProfile("hprof_pTGenJet_JESRMPFGen_genc","",nzptbins,zptbins);
  hprof_pTGenJet_JESRMPFGen_geng = new TProfile("hprof_pTGenJet_JESRMPFGen_geng","",nzptbins,zptbins);
  hprof_pTGenJet_JESRMPFGen_genuds = new TProfile("hprof_pTGenJet_JESRMPFGen_genuds","",nzptbins,zptbins);

  Float_t ymin = -1.5, ymax = 3.5;
  hprof_pTGenJet_JESRpTGen->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRpTGen_genb->BuildOptions(ymin,ymax,"i"); 
  hprof_pTGenJet_JESRpTGen_genc->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRpTGen_geng->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRpTGen_genuds->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRMPFGen->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRMPFGen_genb->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRMPFGen_genc->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRMPFGen_geng->BuildOptions(ymin,ymax,"i");
  hprof_pTGenJet_JESRMPFGen_genuds->BuildOptions(ymin,ymax,"i");

  h_pTGenZ_JESRpTGen        = new TH2F("h_pTGenZ_JESRpTGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_genb   = new TH2F("h_pTGenZ_JESRpTGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_genc   = new TH2F("h_pTGenZ_JESRpTGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_geng   = new TH2F("h_pTGenZ_JESRpTGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRpTGen_genuds = new TH2F("h_pTGenZ_JESRpTGen_genuds","",nzptbins,zptbins,nbinsy,binsy);

  h_pTGenZ_JESRMPFGen        = new TH2F("h_pTGenZ_JESRMPFGen","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_genb   = new TH2F("h_pTGenZ_JESRMPFGen_genb","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_genc   = new TH2F("h_pTGenZ_JESRMPFGen_genc","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_geng   = new TH2F("h_pTGenZ_JESRMPFGen_geng","",nzptbins,zptbins,nbinsy,binsy);
  h_pTGenZ_JESRMPFGen_genuds = new TH2F("h_pTGenZ_JESRMPFGen_genuds","",nzptbins,zptbins,nbinsy,binsy);

  h_Zpt_R_template = new TH2F("h_Zpt_R_temp","",nzptbins,zptbins,nbinsy,binsy);
  size_t nbins = nbins_alpha*bins_eta.size()*npsWeights;

  h_Zpt_mZ = new TH2F*[nbins];
  h_Zpt_mZgen = new TH2F*[nbins];
  
  h_Zpt_RpT = new TH2F*[nbins];
  h_Zpt_RMPF  = new TH2F*[nbins];
  h_Zpt_RMPFjet1  = new TH2F*[nbins];
  h_Zpt_RMPFjetn  = new TH2F*[nbins];
  h_Zpt_RMPFuncl  = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2loose = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2loose = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2medium = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2medium = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2tight = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2tight = new TH2F*[nbins];

  h_Zpt_RpT_genb = new TH2F*[nbins];
  h_Zpt_RMPF_genb = new TH2F*[nbins];
  h_Zpt_RMPFjet1_genb  = new TH2F*[nbins];
  h_Zpt_RMPFjetn_genb  = new TH2F*[nbins];
  h_Zpt_RMPFuncl_genb  = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2loose_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2medium_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2tight_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_genb = new TH2F*[nbins];

  h_Zpt_RpT_genc = new TH2F*[nbins];
  h_Zpt_RMPF_genc = new TH2F*[nbins];
  h_Zpt_RMPFjet1_genc  = new TH2F*[nbins];
  h_Zpt_RMPFjetn_genc  = new TH2F*[nbins];
  h_Zpt_RMPFuncl_genc  = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2loose_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2medium_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2tight_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_genc = new TH2F*[nbins];

  h_Zpt_RpT_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_genuds = new TH2F*[nbins];
  h_Zpt_RMPFjet1_genuds  = new TH2F*[nbins];
  h_Zpt_RMPFjetn_genuds  = new TH2F*[nbins];
  h_Zpt_RMPFuncl_genuds  = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2loose_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2medium_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2tight_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_genuds = new TH2F*[nbins];

  h_Zpt_RpT_geng = new TH2F*[nbins];
  h_Zpt_RMPF_geng = new TH2F*[nbins];
  h_Zpt_RMPFjet1_geng  = new TH2F*[nbins];
  h_Zpt_RMPFjetn_geng  = new TH2F*[nbins];
  h_Zpt_RMPFuncl_geng  = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2loose_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2loose_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2medium_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2medium_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagCSVV2tight_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagCSVV2tight_geng = new TH2F*[nbins];


  h_Zpt_RpT_btagDeepBloose = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBloose = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBmedium = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBmedium = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBtight = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBtight = new TH2F*[nbins];
  h_Zpt_RMPFjet1_btagDeepBtight = new TH2F*[nbins];
  h_Zpt_RMPFjetn_btagDeepBtight = new TH2F*[nbins];
  h_Zpt_RMPFuncl_btagDeepBtight = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepBloose_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBloose_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBmedium_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBmedium_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBtight_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBtight_genb = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepBloose_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBloose_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBmedium_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBmedium_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBtight_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBtight_genc = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepBloose_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBloose_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBmedium_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBmedium_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBtight_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBtight_genuds = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepBloose_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBloose_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBmedium_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBmedium_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepBtight_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepBtight_geng = new TH2F*[nbins];


  h_Zpt_RpT_btagDeepCloose = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCloose = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCmedium = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCmedium = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCtight = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCtight = new TH2F*[nbins];
  h_Zpt_RMPFjet1_btagDeepCtight = new TH2F*[nbins];
  h_Zpt_RMPFjetn_btagDeepCtight = new TH2F*[nbins];
  h_Zpt_RMPFuncl_btagDeepCtight = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepCloose_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCloose_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCmedium_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCmedium_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCtight_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCtight_genb = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepCloose_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCloose_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCmedium_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCmedium_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCtight_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCtight_genc = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepCloose_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCloose_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCmedium_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCmedium_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCtight_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCtight_genuds = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepCloose_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCloose_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCmedium_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCmedium_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepCtight_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepCtight_geng = new TH2F*[nbins];
  

  h_Zpt_RpT_btagDeepFlavBloose = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBloose = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBmedium = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBmedium = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepFlavBloose_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBloose_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBmedium_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBmedium_genb = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_genb = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_genb = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepFlavBloose_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBloose_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBmedium_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBmedium_genc = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_genc = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_genc = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepFlavBloose_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBloose_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBmedium_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBmedium_genuds = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_genuds = new TH2F*[nbins];

  h_Zpt_RpT_btagDeepFlavBloose_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBloose_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBmedium_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBmedium_geng = new TH2F*[nbins];
  h_Zpt_RpT_btagDeepFlavBtight_geng = new TH2F*[nbins];
  h_Zpt_RMPF_btagDeepFlavBtight_geng = new TH2F*[nbins];

  h_Zpt_RpT_gluontag = new TH2F*[nbins];
  h_Zpt_RMPF_gluontag = new TH2F*[nbins];
  h_Zpt_RMPFjet1_gluontag = new TH2F*[nbins];
  h_Zpt_RMPFjetn_gluontag = new TH2F*[nbins];
  h_Zpt_RMPFuncl_gluontag = new TH2F*[nbins];
  h_Zpt_RpT_gluontag_genb = new TH2F*[nbins];
  h_Zpt_RMPF_gluontag_genb = new TH2F*[nbins];
  h_Zpt_RpT_gluontag_genc = new TH2F*[nbins];
  h_Zpt_RMPF_gluontag_genc = new TH2F*[nbins];
  h_Zpt_RpT_gluontag_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_gluontag_genuds = new TH2F*[nbins];
  h_Zpt_RpT_gluontag_geng = new TH2F*[nbins];
  h_Zpt_RMPF_gluontag_geng = new TH2F*[nbins];

  h_Zpt_RpT_quarktag = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag = new TH2F*[nbins];
  h_Zpt_RMPFjet1_quarktag = new TH2F*[nbins];
  h_Zpt_RMPFjetn_quarktag = new TH2F*[nbins];
  h_Zpt_RMPFuncl_quarktag = new TH2F*[nbins];
  h_Zpt_RpT_quarktag_genb = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag_genb = new TH2F*[nbins];
  h_Zpt_RpT_quarktag_genc = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag_genc = new TH2F*[nbins];
  h_Zpt_RpT_quarktag_genuds = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag_genuds = new TH2F*[nbins];
  h_Zpt_RpT_quarktag_geng = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag_geng = new TH2F*[nbins];
  h_Zpt_RpT_quarktag_gens = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag_gens = new TH2F*[nbins];
  h_Zpt_RpT_quarktag_genud = new TH2F*[nbins];
  h_Zpt_RMPF_quarktag_genud = new TH2F*[nbins];
  
  for(size_t i = 0; i < nbins; ++i){
    //nbins = nbins_alpha*bins_eta.size()*npsWeights;
    size_t ialpha = i%(nbins_alpha*bins_eta.size())%nbins_alpha;
    size_t ieta = i%(nbins_alpha*bins_eta.size())/nbins_alpha;
    size_t ipsw = i/(nbins_alpha*bins_eta.size());
    
    std::string s_alpha = std::string("_alpha")+std::to_string(int(100*bins_alpha[ialpha+1]));
    std::string s_eta = std::string("_eta_")+std::regex_replace(bins_eta[ieta], std::regex("\\."), "");
    std::string s_psw = "";
    if(ipsw > 0) s_psw = "/PSWeight"+std::to_string(ipsw-1)+"/";

    std::string s = s_alpha+s_eta;
    //std::cout << "check s " << i << " " << s << std::endl;
    h_Zpt_mZ[i] = (TH2F*)h_Zpt_mZ_template->Clone(("h_Zpt_mZ"+s).c_str());
    if(!isData) h_Zpt_mZgen[i] = (TH2F*)h_Zpt_mZ_template->Clone(("h_Zpt_mZgen"+s).c_str());
    
    h_Zpt_RpT[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s).c_str());
    h_Zpt_RMPF[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s).c_str());
    h_Zpt_RMPFjet1[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s).c_str());
    h_Zpt_RMPFjetn[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s).c_str());
    h_Zpt_RMPFuncl[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s).c_str());
    h_Zpt_RpT_btagCSVV2loose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s).c_str());
    h_Zpt_RMPF_btagCSVV2loose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s).c_str());
    h_Zpt_RpT_btagCSVV2medium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s).c_str());
    h_Zpt_RMPF_btagCSVV2medium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s).c_str());
    h_Zpt_RpT_btagCSVV2tight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s).c_str());
    h_Zpt_RMPF_btagCSVV2tight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s).c_str());

    h_Zpt_RpT_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genb").c_str());
    h_Zpt_RMPF_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genb").c_str());
    h_Zpt_RMPFjet1_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genb").c_str());
    h_Zpt_RMPFjetn_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genb").c_str());
    h_Zpt_RMPFuncl_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genb").c_str());
    h_Zpt_RpT_btagCSVV2loose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_genb").c_str());
    h_Zpt_RMPF_btagCSVV2loose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_genb").c_str());
    h_Zpt_RpT_btagCSVV2medium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_genb").c_str());
    h_Zpt_RMPF_btagCSVV2medium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_genb").c_str());
    h_Zpt_RpT_btagCSVV2tight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_genb").c_str());
    h_Zpt_RMPF_btagCSVV2tight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_genb").c_str());

    h_Zpt_RpT_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genc").c_str());
    h_Zpt_RMPF_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genc").c_str());
    h_Zpt_RMPFjet1_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genc").c_str());
    h_Zpt_RMPFjetn_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genc").c_str());
    h_Zpt_RMPFuncl_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genc").c_str());
    h_Zpt_RpT_btagCSVV2loose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_genc").c_str());
    h_Zpt_RMPF_btagCSVV2loose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_genc").c_str());
    h_Zpt_RpT_btagCSVV2medium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_genc").c_str());
    h_Zpt_RMPF_btagCSVV2medium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_genc").c_str());
    h_Zpt_RpT_btagCSVV2tight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_genc").c_str());
    h_Zpt_RMPF_btagCSVV2tight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_genc").c_str());

    h_Zpt_RpT_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_genuds").c_str());
    h_Zpt_RMPF_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_genuds").c_str());
    h_Zpt_RMPFjet1_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_genuds").c_str());
    h_Zpt_RMPFjetn_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_genuds").c_str());
    h_Zpt_RMPFuncl_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_genuds").c_str());
    h_Zpt_RpT_btagCSVV2loose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagCSVV2loose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_genuds").c_str());
    h_Zpt_RpT_btagCSVV2medium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagCSVV2medium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_genuds").c_str());
    h_Zpt_RpT_btagCSVV2tight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagCSVV2tight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_genuds").c_str());

    h_Zpt_RpT_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT"+s+"_geng").c_str());
    h_Zpt_RMPF_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF"+s+"_geng").c_str());
    h_Zpt_RMPFjet1_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1"+s+"_geng").c_str());
    h_Zpt_RMPFjetn_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn"+s+"_geng").c_str());
    h_Zpt_RMPFuncl_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl"+s+"_geng").c_str());
    h_Zpt_RpT_btagCSVV2loose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2loose"+s+"_geng").c_str());
    h_Zpt_RMPF_btagCSVV2loose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2loose"+s+"_geng").c_str());
    h_Zpt_RpT_btagCSVV2medium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2medium"+s+"_geng").c_str());
    h_Zpt_RMPF_btagCSVV2medium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2medium"+s+"_geng").c_str());
    h_Zpt_RpT_btagCSVV2tight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagCSVV2tight"+s+"_geng").c_str());
    h_Zpt_RMPF_btagCSVV2tight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagCSVV2tight"+s+"_geng").c_str());

 
    h_Zpt_RpT_btagDeepBloose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s).c_str());
    h_Zpt_RMPF_btagDeepBloose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s).c_str());
    h_Zpt_RpT_btagDeepBmedium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s).c_str());
    h_Zpt_RMPF_btagDeepBmedium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s).c_str());
    h_Zpt_RpT_btagDeepBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBtight"+s).c_str());
    h_Zpt_RMPF_btagDeepBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBtight"+s).c_str());
    h_Zpt_RMPFjet1_btagDeepBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btagDeepBtight"+s).c_str());
    h_Zpt_RMPFjetn_btagDeepBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btagDeepBtight"+s).c_str());
    h_Zpt_RMPFuncl_btagDeepBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btagDeepBtight"+s).c_str());

    h_Zpt_RpT_btagDeepBloose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepBloose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepBmedium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepBmedium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepBtight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBtight"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepBtight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBtight"+s+"_genb").c_str());

    h_Zpt_RpT_btagDeepBloose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepBloose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepBmedium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepBmedium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepBtight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBtight"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepBtight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBtight"+s+"_genc").c_str());

    h_Zpt_RpT_btagDeepBloose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepBloose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepBmedium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepBmedium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepBtight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBtight"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepBtight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBtight"+s+"_genuds").c_str());

    h_Zpt_RpT_btagDeepBloose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBloose"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepBloose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBloose"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepBmedium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBmedium"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepBmedium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBmedium"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepBtight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepBtight"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepBtight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepBtight"+s+"_geng").c_str());


    h_Zpt_RpT_btagDeepCloose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s).c_str());
    h_Zpt_RMPF_btagDeepCloose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s).c_str());
    h_Zpt_RpT_btagDeepCmedium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s).c_str());
    h_Zpt_RMPF_btagDeepCmedium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s).c_str());
    h_Zpt_RpT_btagDeepCtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCtight"+s).c_str());
    h_Zpt_RMPF_btagDeepCtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCtight"+s).c_str());
    h_Zpt_RMPFjet1_btagDeepCtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_btagDeepCtight"+s).c_str());
    h_Zpt_RMPFjetn_btagDeepCtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_btagDeepCtight"+s).c_str());
    h_Zpt_RMPFuncl_btagDeepCtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_btagDeepCtight"+s).c_str());

    h_Zpt_RpT_btagDeepCloose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepCloose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepCmedium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepCmedium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepCtight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCtight"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepCtight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCtight"+s+"_genb").c_str());

    h_Zpt_RpT_btagDeepCloose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepCloose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepCmedium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepCmedium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepCtight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCtight"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepCtight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCtight"+s+"_genc").c_str());

    h_Zpt_RpT_btagDeepCloose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepCloose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepCmedium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepCmedium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepCtight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCtight"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepCtight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCtight"+s+"_genuds").c_str());

    h_Zpt_RpT_btagDeepCloose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCloose"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepCloose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCloose"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepCmedium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCmedium"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepCmedium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCmedium"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepCtight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepCtight"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepCtight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepCtight"+s+"_geng").c_str());


    h_Zpt_RpT_btagDeepFlavBloose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s).c_str());
    h_Zpt_RMPF_btagDeepFlavBloose[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s).c_str());
    h_Zpt_RpT_btagDeepFlavBmedium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s).c_str());
    h_Zpt_RMPF_btagDeepFlavBmedium[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s).c_str());
    h_Zpt_RpT_btagDeepFlavBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s).c_str());
    h_Zpt_RMPF_btagDeepFlavBtight[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s).c_str());

    h_Zpt_RpT_btagDeepFlavBloose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepFlavBloose_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepFlavBmedium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepFlavBmedium_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_genb").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_genb").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_genb").c_str());

    h_Zpt_RpT_btagDeepFlavBloose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepFlavBloose_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepFlavBmedium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepFlavBmedium_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_genc").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_genc").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_genc").c_str());

    h_Zpt_RpT_btagDeepFlavBloose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepFlavBloose_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepFlavBmedium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepFlavBmedium_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_genuds").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_genuds").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_genuds").c_str());

    h_Zpt_RpT_btagDeepFlavBloose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBloose"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepFlavBloose_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBloose"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepFlavBmedium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBmedium"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepFlavBmedium_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBmedium"+s+"_geng").c_str());
    h_Zpt_RpT_btagDeepFlavBtight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_btagDeepFlavBtight"+s+"_geng").c_str());
    h_Zpt_RMPF_btagDeepFlavBtight_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_btagDeepFlavBtight"+s+"_geng").c_str());


    h_Zpt_RpT_gluontag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s).c_str());
    h_Zpt_RMPF_gluontag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s).c_str());
    h_Zpt_RMPFjet1_gluontag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_gluontag"+s).c_str()); 
    h_Zpt_RMPFjetn_gluontag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_gluontag"+s).c_str()); 
    h_Zpt_RMPFuncl_gluontag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_gluontag"+s).c_str()); 
    h_Zpt_RpT_gluontag_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genb").c_str());
    h_Zpt_RMPF_gluontag_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genb").c_str());
    h_Zpt_RpT_gluontag_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genc").c_str());
    h_Zpt_RMPF_gluontag_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genc").c_str());
    h_Zpt_RpT_gluontag_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_genuds").c_str());
    h_Zpt_RMPF_gluontag_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_genuds").c_str());
    h_Zpt_RpT_gluontag_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_gluontag"+s+"_geng").c_str());
    h_Zpt_RMPF_gluontag_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_gluontag"+s+"_geng").c_str());

    h_Zpt_RpT_quarktag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s).c_str());
    h_Zpt_RMPF_quarktag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s).c_str());
    h_Zpt_RMPFjet1_quarktag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjet1_quarktag"+s).c_str());
    h_Zpt_RMPFjetn_quarktag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFjetn_quarktag"+s).c_str());
    h_Zpt_RMPFuncl_quarktag[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPFuncl_quarktag"+s).c_str());
    h_Zpt_RpT_quarktag_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genb").c_str());
    h_Zpt_RMPF_quarktag_genb[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genb").c_str());
    h_Zpt_RpT_quarktag_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genc").c_str());
    h_Zpt_RMPF_quarktag_genc[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genc").c_str());
    h_Zpt_RpT_quarktag_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genuds").c_str());
    h_Zpt_RMPF_quarktag_genuds[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genuds").c_str());
    h_Zpt_RpT_quarktag_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_geng").c_str());
    h_Zpt_RMPF_quarktag_geng[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_geng").c_str());
    
    h_Zpt_RpT_quarktag_gens[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_gens").c_str());
    h_Zpt_RMPF_quarktag_gens[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_gens").c_str());
    h_Zpt_RpT_quarktag_genud[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RpT_quarktag"+s+"_genud").c_str());
    h_Zpt_RMPF_quarktag_genud[i] = (TH2F*)h_Zpt_R_template->Clone(("h_Zpt_RMPF_quarktag"+s+"_genud").c_str());
  }

  //std::cout << "check1" << std::endl;  
  cMain->book("all events");
  cMain->book("trigger");
  cMain->book("MET cleaning");
  cMain->book("2-3 leptons");
  cMain->book("2 leptons");
  cMain->book("Z boson");
  cMain->book("jets");
  cMain->book("leading jet");
  cMain->book("phiBB");
  cMain->book("  0-jet == muon");
  cMain->book("  leading jet == 0-jet");
  //  cMain->book("subleading jet");
  cMain->book("  alpha0.3");
  cMain->book("  alpha1.0");
  cMain->book("alpha");
  cMain->book("RpT+MPF cuts");
  cMain->book("btagCSVV2loose");
  cMain->book("btagCSVV2medium");
  cMain->book("btagCSVV2tight");
  cMain->book("btagDeepBloose");
  cMain->book("btagDeepBmedium");
  cMain->book("btagDeepBtight");
  cMain->book("btagDeepFlavBloose");
  cMain->book("btagDeepFlavBmedium");
  cMain->book("btagDeepFlavBtight");

  cJetLabel->book("all jets");
  cJetLabel->book("b jets");
  cJetLabel->book("c jets");
  cJetLabel->book("uds jets");
  cJetLabel->book("gluon jets");
  cJetLabel->book("unclassified jets");

  cJetLabel_btagDeepBtight->book("all jets");
  cJetLabel_btagDeepBtight->book("b jets");
  cJetLabel_btagDeepBtight->book("c jets");
  cJetLabel_btagDeepBtight->book("uds jets");
  cJetLabel_btagDeepBtight->book("gluon jets");
  cJetLabel_btagDeepBtight->book("unclassified jets");

  cJetLabel_btagDeepCtight->book("all jets");
  cJetLabel_btagDeepCtight->book("b jets");
  cJetLabel_btagDeepCtight->book("c jets");
  cJetLabel_btagDeepCtight->book("uds jets");
  cJetLabel_btagDeepCtight->book("gluon jets");
  cJetLabel_btagDeepCtight->book("unclassified jets");

  cJetLabel_gluontag->book("all jets");
  cJetLabel_gluontag->book("b jets");
  cJetLabel_gluontag->book("c jets");
  cJetLabel_gluontag->book("uds jets");
  cJetLabel_gluontag->book("gluon jets");
  cJetLabel_gluontag->book("unclassified jets");

  cJetLabel_quarktag->book("all jets");
  cJetLabel_quarktag->book("b jets");
  cJetLabel_quarktag->book("c jets");
  cJetLabel_quarktag->book("uds jets");
  cJetLabel_quarktag->book("gluon jets");
  cJetLabel_quarktag->book("unclassified jets");

  cJetLabel_qgtLT0tag->book("all jets");
  cJetLabel_qgtLT0tag->book("b jets");
  cJetLabel_qgtLT0tag->book("c jets");
  cJetLabel_qgtLT0tag->book("uds jets");
  cJetLabel_qgtLT0tag->book("gluon jets");
  cJetLabel_qgtLT0tag->book("unclassified jets");

  TList* inputList = this->GetInputList();
  for(int i = 0; i < inputList->GetEntries(); ++i){
    //std::cout << "check inputlist " << inputList->At(i)->GetTitle() << " " << std::string(inputList->At(i)->GetTitle()).find("skim") << std::endl;
    if(std::string(inputList->At(i)->GetTitle()).find("skim") == 0){
      inputList->At(i)->Write();
      //std::cout << "check skim" << std::endl;
    }
  }
  //  exit(0);
  //cMain->print();  
  //Init(tree);
}

void Analysis::SlaveBegin(TTree* tree) {
  //Init(tree);
  /*
  //  tree->SetMakeClass(1); // Use the decomposed object mode.                                                                                 
  const char* trg = this->GetParameter<const char*>("trigger");

  std::vector<std::string> triggerNames;
  std::string s_trg = std::string(trg);
  size_t pos = s_trg.find("||");
  if (pos == s_trg.size()){
    triggerNames.push_back(s_trg);
  }else{
    while (pos < s_trg.size()){
      pos = s_trg.find("||");
      triggerNames.push_back(s_trg.substr(0,pos));
      s_trg = s_trg.substr(pos+2,s_trg.size());
    }
  }

  triggerBit = new bool[triggerNames.size()];
  
  bool trgDecision = false;
  std::map<std::string,TTreeReaderValue<Bool_t> > trgMap;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    std::cout << "check trgMap" << triggerNames[i].c_str() << std::endl;
    //    trgMap[triggerNames[i].c_str()] = {fReader, triggerNames[i].c_str()};
    //    TTreeReaderValue<Bool_t> trgBit = {fReader, triggerNames[i].c_str()};
    tree->SetBranchAddress(triggerNames[i].c_str(),&triggerBit[i]);
  }
  */
}

void Analysis::Terminate() {
  //const char* datasetName = this->GetParameter<TNamed*>("name")->GetTitle();
  //std::cout << "Dataset " << datasetName << std::endl;
  //cMain->printBothCounters();
  //std::cout << std::endl;
  //  TList* lOUT = new TList();
  //  lOUT->Add(cMain->getHisto());
  //  lOUT->Add(cMain->getWeightedHisto());
  fOutput->Add(cMain->getHisto());
  fOutput->Add(cMain->getWeightedHisto());
  if(!isData) {
    fOutput->Add(cJetLabel->getHisto("jetCounter_all"));
    fOutput->Add(cJetLabel_btagDeepBtight->getHisto("jetCounter_btag"));
    fOutput->Add(cJetLabel_btagDeepCtight->getHisto("jetCounter_ctag"));
    fOutput->Add(cJetLabel_quarktag->getHisto("jetCounter_quarktag"));
    fOutput->Add(cJetLabel_gluontag->getHisto("jetCounter_gluontag"));
    fOutput->Add(cJetLabel_qgtLT0tag->getHisto("jetCounter_qgtLT0tag"));
  }
  //  fOutput->Add(cJetLabel->getWeightedHisto());

  fOUT->cd();
  fOUT->mkdir("configInfo");
  fOUT->cd("configInfo");
  TH1F* h_counters = cMain->getHisto();
  h_counters->Write();
  TH1F* h_wcounters = cMain->getWeightedHisto();
  h_wcounters->Write();

  if(!isData){
    TH1F* h_jetlabel = cJetLabel->getHisto("jet labels, unweighted");
    h_jetlabel->Write();
    TH1F* h_wjetlabel = cJetLabel->getWeightedHisto("jet labels, weighted");
    h_wjetlabel->Write();

    TH1F* h_jetlabel_btagDeepBtight = cJetLabel_btagDeepBtight->getHisto("jet labels btagDeepBtight, unweighted");
    h_jetlabel_btagDeepBtight->Write();
    TH1F* h_wjetlabel_btagDeepBtight = cJetLabel_btagDeepBtight->getWeightedHisto("jet labels btagDeepBtight, weighted");
    h_wjetlabel_btagDeepBtight->Write();

    TH1F* h_jetlabel_btagDeepCtight = cJetLabel_btagDeepCtight->getHisto("jet labels btagDeepCtight, unweighted");
    h_jetlabel_btagDeepCtight->Write();
    TH1F* h_wjetlabel_btagDeepCtight = cJetLabel_btagDeepCtight->getWeightedHisto("jet labels btagDeepCtight, weighted");
    h_wjetlabel_btagDeepCtight->Write();

    TH1F* h_jetlabel_gluontag = cJetLabel_gluontag->getHisto("jet labels gluontag, unweighted");
    h_jetlabel_gluontag->Write();
    TH1F* h_wjetlabel_gluontag = cJetLabel_gluontag->getWeightedHisto("jet labels gluontag, weighted");
    h_wjetlabel_gluontag->Write();

    TH1F* h_jetlabel_quarktag = cJetLabel_quarktag->getHisto("jet labels quarktag, unweighted");
    h_jetlabel_quarktag->Write();
    TH1F* h_wjetlabel_quarktag = cJetLabel_quarktag->getWeightedHisto("jet labels quarktag, weighted");
    h_wjetlabel_quarktag->Write();

    TH1F* h_jetlabel_qgtLT0tag = cJetLabel_qgtLT0tag->getHisto("jet labels q/gtag failed, unweighted");
    h_jetlabel_qgtLT0tag->Write();
    TH1F* h_wjetlabel_qgtLT0tag = cJetLabel_qgtLT0tag->getWeightedHisto("jet labels q/gtag failed, weighted");
    h_wjetlabel_qgtLT0tag->Write();
  }

  TH1F* h_isdata = new TH1F("isdata","",1,0,1);
  h_isdata->SetBinContent(1,int(isData));
  h_isdata->Write();

  if(isData){
    TH1F* h_lumi = new TH1F("lumi","",1,0,1);
    h_lumi->SetBinContent(1,lumi);
    h_lumi->Write();
  }

  if(!isData){
    TH1F* pu_data = pileupWeight->getPUData();
    pu_data->Write();
    TH1F* pu_mc = pileupWeight->getPUMC();
    pu_mc->Write();
  }
  fOUT->cd("analysis");

  h_Mu->Write();
  h_RhoVsMu->Write();
  h_NpvVsMu->Write();
  h_Mu0->Write();
  h_RhoVsMu0->Write();
  h_NpvVsMu0->Write();

  h_JetPt_chHEF->Write();
  h_JetPt_neHEF->Write();
  h_JetPt_neEmEF->Write();
  h_JetPt_chEmEF->Write();
  h_JetPt_muEF->Write();

  h_njets->Write();
  h_njets_sele->Write();

  h_Jet_jetId->Write();
  h_Jet_electronIdx1->Write();
  h_Jet_electronIdx2->Write();
  h_Jet_muonIdx1->Write();
  h_Jet_muonIdx2->Write();
  h_Jet_nElectrons->Write();
  h_Jet_nMuons->Write();
  h_Jet_puId->Write();

  h_jet1pt->Write();
  h_jet1eta->Write();
  h_jet2pt->Write();
  h_jet2eta->Write();

  h_phibb->Write();

  h_jet1cpt->Write();
  h_jet1ceta->Write();
  h_jet2cpt->Write();
  h_jet2ceta->Write();

  h_jet2etapt->Write();
  
  h_Zpt->Write();
  h_Zpt_a10->Write();
  h_Zpt_a15->Write();
  h_Zpt_a20->Write();
  h_Zpt_a30->Write();
  h_Zpt_a40->Write();
  h_Zpt_a50->Write();
  h_Zpt_a60->Write();
  h_Zpt_a80->Write();
  h_Zpt_a100->Write();

  h_alpha->Write();
  for(size_t i = 0; i < cuts.size(); ++i){
    h_alphaZpt[i]->Write();
  }

  h_RpT->Write();
  h_RMPF->Write();
  /*
  h_alpha_RpT->Write();
  h_alpha_RMPF->Write();
  h_alpha_RpT_genb->Write();
  h_alpha_RMPF_genb->Write();

  // btagging
  // Jet_btagCMVA
  // Jet_btagCSVV2
  // Jet_btagDeepB
  // Jet_btagDeepC
  // Jet_btagDeepFlavB
  ////if(!Jet_btagCMVA) return;
  
  h_alpha_RpT_btagCSVV2loose->Write();
  h_alpha_RMPF_btagCSVV2loose->Write();
  h_alpha_RpT_btagCSVV2medium->Write();
  h_alpha_RMPF_btagCSVV2medium->Write();
  h_alpha_RpT_btagCSVV2tight->Write();
  h_alpha_RMPF_btagCSVV2tight->Write();

  h_alpha_RpT_btagCSVV2loose_genb->Write();
  h_alpha_RMPF_btagCSVV2loose_genb->Write();
  h_alpha_RpT_btagCSVV2medium_genb->Write();
  h_alpha_RMPF_btagCSVV2medium_genb->Write();
  h_alpha_RpT_btagCSVV2tight_genb->Write();
  h_alpha_RMPF_btagCSVV2tight_genb->Write();

  h_alpha_RpT_btagCMVAloose->Write();
  h_alpha_RMPF_btagCMVAloose->Write();
  h_alpha_RpT_btagCMVAmedium->Write();
  h_alpha_RMPF_btagCMVAmedium->Write();
  h_alpha_RpT_btagCMVAtight->Write();
  h_alpha_RMPF_btagCMVAtight->Write();

  h_alpha_RpT_btagDeepBloose->Write();
  h_alpha_RMPF_btagDeepBloose->Write();
  h_alpha_RpT_btagDeepBmedium->Write();
  h_alpha_RMPF_btagDeepBmedium->Write();
  h_alpha_RpT_btagDeepBtight->Write();
  h_alpha_RMPF_btagDeepBtight->Write();

  h_alpha_RpT_btagDeepFlavBloose->Write();
  h_alpha_RMPF_btagDeepFlavBloose->Write();
  h_alpha_RpT_btagDeepFlavBmedium->Write();
  h_alpha_RMPF_btagDeepFlavBmedium->Write();
  h_alpha_RpT_btagDeepFlavBtight->Write();
  h_alpha_RMPF_btagDeepFlavBtight->Write();

  for(size_t i = 0; i < cuts.size(); ++i){
    h_alpha_RpT_Zpt[i]->Write();
    h_alpha_RMPF_Zpt[i]->Write();
    h_alpha_RpT_Zpt_genb[i]->Write();
    h_alpha_RMPF_Zpt_genb[i]->Write();

    h_alpha_RpT_btagCSVV2loose_Zpt[i]->Write();
    h_alpha_RMPF_btagCSVV2loose_Zpt[i]->Write();
    h_alpha_RpT_btagCSVV2medium_Zpt[i]->Write();
    h_alpha_RMPF_btagCSVV2medium_Zpt[i]->Write();
    h_alpha_RpT_btagCSVV2tight_Zpt[i]->Write();
    h_alpha_RMPF_btagCSVV2tight_Zpt[i]->Write();
    h_alpha_RpT_btagCSVV2loose_Zpt_genb[i]->Write();
    h_alpha_RMPF_btagCSVV2loose_Zpt_genb[i]->Write();
    h_alpha_RpT_btagCSVV2medium_Zpt_genb[i]->Write();
    h_alpha_RMPF_btagCSVV2medium_Zpt_genb[i]->Write();
    h_alpha_RpT_btagCSVV2tight_Zpt_genb[i]->Write();
    h_alpha_RMPF_btagCSVV2tight_Zpt_genb[i]->Write();
    
    h_alpha_RpT_btagDeepBloose_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepBloose_Zpt[i]->Write();
    h_alpha_RpT_btagDeepBmedium_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepBmedium_Zpt[i]->Write();
    h_alpha_RpT_btagDeepBtight_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepBtight_Zpt[i]->Write();

    h_alpha_RpT_btagDeepFlavBloose_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepFlavBloose_Zpt[i]->Write();
    h_alpha_RpT_btagDeepFlavBmedium_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepFlavBmedium_Zpt[i]->Write();
    h_alpha_RpT_btagDeepFlavBtight_Zpt[i]->Write();
    h_alpha_RMPF_btagDeepFlavBtight_Zpt[i]->Write();

  }
  */

  h_pTGenJet_JESRpTGen->Write();
  h_pTGenJet_JESRpTGen_genb->Write();
  h_pTGenJet_JESRpTGen_genc->Write();
  h_pTGenJet_JESRpTGen_geng->Write();
  h_pTGenJet_JESRpTGen_genuds->Write();

  h_pTGenJet_JESRMPFGen->Write();
  h_pTGenJet_JESRMPFGen_genb->Write();
  h_pTGenJet_JESRMPFGen_genc->Write();
  h_pTGenJet_JESRMPFGen_geng->Write();
  h_pTGenJet_JESRMPFGen_genuds->Write();

  hprof_pTGenJet_JESRpTGen->Write();
  hprof_pTGenJet_JESRpTGen_genb->Write();
  hprof_pTGenJet_JESRpTGen_genc->Write();
  hprof_pTGenJet_JESRpTGen_geng->Write();
  hprof_pTGenJet_JESRpTGen_genuds->Write();

  hprof_pTGenJet_JESRMPFGen->Write();
  hprof_pTGenJet_JESRMPFGen_genb->Write();
  hprof_pTGenJet_JESRMPFGen_genc->Write();
  hprof_pTGenJet_JESRMPFGen_geng->Write();
  hprof_pTGenJet_JESRMPFGen_genuds->Write();

  h_pTGenZ_JESRpTGen->Write();
  h_pTGenZ_JESRpTGen_genb->Write();
  h_pTGenZ_JESRpTGen_genc->Write();
  h_pTGenZ_JESRpTGen_geng->Write();
  h_pTGenZ_JESRpTGen_genuds->Write();

  h_pTGenZ_JESRMPFGen->Write();
  h_pTGenZ_JESRMPFGen_genb->Write();
  h_pTGenZ_JESRMPFGen_genc->Write();
  h_pTGenZ_JESRMPFGen_geng->Write();
  h_pTGenZ_JESRMPFGen_genuds->Write();

  for(size_t i = 0; i < nbins_alpha*bins_eta.size()*npsWeights; ++i){
    size_t ipsw = i/(nbins_alpha*bins_eta.size());
    std::string s_psw = "analysis/PSWeight"+std::to_string(ipsw-1);

    if(ipsw > 0){
      //std::cout << "check PSWeight-dir 1" << std::endl;
      if(!fOUT->cd(s_psw.c_str())) fOUT->mkdir(s_psw.c_str());
      fOUT->cd(s_psw.c_str());
      //std::cout << "check PSWeight-dir 2" << std::endl;
    }
    h_Zpt_mZ[i]->Write();
    if(!isData) h_Zpt_mZgen[i]->Write();

    h_Zpt_RpT[i]->Write();
    h_Zpt_RMPF[i]->Write();
    h_Zpt_RMPFjet1[i]->Write();
    h_Zpt_RMPFjetn[i]->Write();
    h_Zpt_RMPFuncl[i]->Write();

    h_Zpt_RpT_btagCSVV2loose[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose[i]->Write();
    h_Zpt_RpT_btagCSVV2medium[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium[i]->Write();
    h_Zpt_RpT_btagCSVV2tight[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight[i]->Write();

    h_Zpt_RpT_genb[i]->Write();
    h_Zpt_RMPF_genb[i]->Write();
    h_Zpt_RMPFjet1_genb[i]->Write();
    h_Zpt_RMPFjetn_genb[i]->Write();
    h_Zpt_RMPFuncl_genb[i]->Write();

    h_Zpt_RpT_btagCSVV2loose_genb[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose_genb[i]->Write();
    h_Zpt_RpT_btagCSVV2medium_genb[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium_genb[i]->Write();
    h_Zpt_RpT_btagCSVV2tight_genb[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight_genb[i]->Write();

    h_Zpt_RpT_genc[i]->Write();
    h_Zpt_RMPF_genc[i]->Write();
    h_Zpt_RMPFjet1_genc[i]->Write();
    h_Zpt_RMPFjetn_genc[i]->Write();
    h_Zpt_RMPFuncl_genc[i]->Write();

    h_Zpt_RpT_btagCSVV2loose_genc[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose_genc[i]->Write();
    h_Zpt_RpT_btagCSVV2medium_genc[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium_genc[i]->Write();
    h_Zpt_RpT_btagCSVV2tight_genc[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight_genc[i]->Write();

    h_Zpt_RpT_genuds[i]->Write();
    h_Zpt_RMPF_genuds[i]->Write();
    h_Zpt_RMPFjet1_genuds[i]->Write();
    h_Zpt_RMPFjetn_genuds[i]->Write();
    h_Zpt_RMPFuncl_genuds[i]->Write();

    h_Zpt_RpT_btagCSVV2loose_genuds[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose_genuds[i]->Write();
    h_Zpt_RpT_btagCSVV2medium_genuds[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium_genuds[i]->Write();
    h_Zpt_RpT_btagCSVV2tight_genuds[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight_genuds[i]->Write();

    h_Zpt_RpT_geng[i]->Write();
    h_Zpt_RMPF_geng[i]->Write();
    h_Zpt_RMPFjet1_geng[i]->Write();
    h_Zpt_RMPFjetn_geng[i]->Write();
    h_Zpt_RMPFuncl_geng[i]->Write();

    h_Zpt_RpT_btagCSVV2loose_geng[i]->Write();
    h_Zpt_RMPF_btagCSVV2loose_geng[i]->Write();
    h_Zpt_RpT_btagCSVV2medium_geng[i]->Write();
    h_Zpt_RMPF_btagCSVV2medium_geng[i]->Write();
    h_Zpt_RpT_btagCSVV2tight_geng[i]->Write();
    h_Zpt_RMPF_btagCSVV2tight_geng[i]->Write();

    h_Zpt_RpT_btagDeepBloose[i]->Write();
    h_Zpt_RMPF_btagDeepBloose[i]->Write();
    h_Zpt_RpT_btagDeepBmedium[i]->Write();
    h_Zpt_RMPF_btagDeepBmedium[i]->Write();
    h_Zpt_RpT_btagDeepBtight[i]->Write();
    h_Zpt_RMPF_btagDeepBtight[i]->Write();
    h_Zpt_RMPFjet1_btagDeepBtight[i]->Write();
    h_Zpt_RMPFjetn_btagDeepBtight[i]->Write();
    h_Zpt_RMPFuncl_btagDeepBtight[i]->Write();

    h_Zpt_RpT_btagDeepBloose_genb[i]->Write();
    h_Zpt_RMPF_btagDeepBloose_genb[i]->Write();
    h_Zpt_RpT_btagDeepBmedium_genb[i]->Write();
    h_Zpt_RMPF_btagDeepBmedium_genb[i]->Write();
    h_Zpt_RpT_btagDeepBtight_genb[i]->Write();
    h_Zpt_RMPF_btagDeepBtight_genb[i]->Write();

    h_Zpt_RpT_btagDeepBloose_genc[i]->Write();
    h_Zpt_RMPF_btagDeepBloose_genc[i]->Write();
    h_Zpt_RpT_btagDeepBmedium_genc[i]->Write();
    h_Zpt_RMPF_btagDeepBmedium_genc[i]->Write();
    h_Zpt_RpT_btagDeepBtight_genc[i]->Write();
    h_Zpt_RMPF_btagDeepBtight_genc[i]->Write();

    h_Zpt_RpT_btagDeepBloose_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepBloose_genuds[i]->Write();
    h_Zpt_RpT_btagDeepBmedium_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepBmedium_genuds[i]->Write();
    h_Zpt_RpT_btagDeepBtight_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepBtight_genuds[i]->Write();

    h_Zpt_RpT_btagDeepBloose_geng[i]->Write();
    h_Zpt_RMPF_btagDeepBloose_geng[i]->Write();
    h_Zpt_RpT_btagDeepBmedium_geng[i]->Write();
    h_Zpt_RMPF_btagDeepBmedium_geng[i]->Write();
    h_Zpt_RpT_btagDeepBtight_geng[i]->Write();
    h_Zpt_RMPF_btagDeepBtight_geng[i]->Write();


    h_Zpt_RpT_btagDeepCloose[i]->Write();
    h_Zpt_RMPF_btagDeepCloose[i]->Write();
    h_Zpt_RpT_btagDeepCmedium[i]->Write();
    h_Zpt_RMPF_btagDeepCmedium[i]->Write();
    h_Zpt_RpT_btagDeepCtight[i]->Write();
    h_Zpt_RMPF_btagDeepCtight[i]->Write();
    h_Zpt_RMPFjet1_btagDeepCtight[i]->Write();
    h_Zpt_RMPFjetn_btagDeepCtight[i]->Write();
    h_Zpt_RMPFuncl_btagDeepCtight[i]->Write();

    h_Zpt_RpT_btagDeepCloose_genb[i]->Write();
    h_Zpt_RMPF_btagDeepCloose_genb[i]->Write();
    h_Zpt_RpT_btagDeepCmedium_genb[i]->Write();
    h_Zpt_RMPF_btagDeepCmedium_genb[i]->Write();
    h_Zpt_RpT_btagDeepCtight_genb[i]->Write();
    h_Zpt_RMPF_btagDeepCtight_genb[i]->Write();

    h_Zpt_RpT_btagDeepCloose_genc[i]->Write();
    h_Zpt_RMPF_btagDeepCloose_genc[i]->Write();
    h_Zpt_RpT_btagDeepCmedium_genc[i]->Write();
    h_Zpt_RMPF_btagDeepCmedium_genc[i]->Write();
    h_Zpt_RpT_btagDeepCtight_genc[i]->Write();
    h_Zpt_RMPF_btagDeepCtight_genc[i]->Write();

    h_Zpt_RpT_btagDeepCloose_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepCloose_genuds[i]->Write();
    h_Zpt_RpT_btagDeepCmedium_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepCmedium_genuds[i]->Write();
    h_Zpt_RpT_btagDeepCtight_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepCtight_genuds[i]->Write();

    h_Zpt_RpT_btagDeepCloose_geng[i]->Write();
    h_Zpt_RMPF_btagDeepCloose_geng[i]->Write();
    h_Zpt_RpT_btagDeepCmedium_geng[i]->Write();
    h_Zpt_RMPF_btagDeepCmedium_geng[i]->Write();
    h_Zpt_RpT_btagDeepCtight_geng[i]->Write();
    h_Zpt_RMPF_btagDeepCtight_geng[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_genb[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_genb[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_genb[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_genb[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_genb[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_genb[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_genc[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_genc[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_genc[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_genc[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_genc[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_genc[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_genuds[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_genuds[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_genuds[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_genuds[i]->Write();

    h_Zpt_RpT_btagDeepFlavBloose_geng[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBloose_geng[i]->Write();
    h_Zpt_RpT_btagDeepFlavBmedium_geng[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBmedium_geng[i]->Write();
    h_Zpt_RpT_btagDeepFlavBtight_geng[i]->Write();
    h_Zpt_RMPF_btagDeepFlavBtight_geng[i]->Write();

    h_Zpt_RpT_gluontag[i]->Write();
    h_Zpt_RMPF_gluontag[i]->Write();
    h_Zpt_RMPFjet1_gluontag[i]->Write();
    h_Zpt_RMPFjetn_gluontag[i]->Write();
    h_Zpt_RMPFuncl_gluontag[i]->Write();
    h_Zpt_RpT_gluontag_genb[i]->Write();
    h_Zpt_RMPF_gluontag_genb[i]->Write();
    h_Zpt_RpT_gluontag_genc[i]->Write();
    h_Zpt_RMPF_gluontag_genc[i]->Write();
    h_Zpt_RpT_gluontag_genuds[i]->Write();
    h_Zpt_RMPF_gluontag_genuds[i]->Write();
    h_Zpt_RpT_gluontag_geng[i]->Write();
    h_Zpt_RMPF_gluontag_geng[i]->Write();

    h_Zpt_RpT_quarktag[i]->Write();
    h_Zpt_RMPF_quarktag[i]->Write();
    h_Zpt_RMPFjet1_quarktag[i]->Write();
    h_Zpt_RMPFjetn_quarktag[i]->Write();
    h_Zpt_RMPFuncl_quarktag[i]->Write();

    h_Zpt_RpT_quarktag_genb[i]->Write();
    h_Zpt_RMPF_quarktag_genb[i]->Write();
    h_Zpt_RpT_quarktag_genc[i]->Write();
    h_Zpt_RMPF_quarktag_genc[i]->Write();
    h_Zpt_RpT_quarktag_genuds[i]->Write();
    h_Zpt_RMPF_quarktag_genuds[i]->Write();
    h_Zpt_RpT_quarktag_geng[i]->Write();
    h_Zpt_RMPF_quarktag_geng[i]->Write();

    h_Zpt_RpT_quarktag_gens[i]->Write();
    h_Zpt_RMPF_quarktag_gens[i]->Write();
    h_Zpt_RpT_quarktag_genud[i]->Write();
    h_Zpt_RMPF_quarktag_genud[i]->Write();

    /*
    std::cout << "Jet flavor fraction check incl. vs sum " << h_Zpt_RpT_btagDeepFlavBtight[i]->GetName() << " " << h_Zpt_RpT_btagDeepFlavBtight[i]->GetEntries() << " " 
              << h_Zpt_RpT_btagDeepFlavBtight_genb[i]->GetEntries()+
                 h_Zpt_RpT_btagDeepFlavBtight_genc[i]->GetEntries()+
                 h_Zpt_RpT_btagDeepFlavBtight_geng[i]->GetEntries()+
                 h_Zpt_RpT_btagDeepFlavBtight_genuds[i]->GetEntries() << std::endl;
    */
    fOUT->cd("analysis");

  }
  fOUT->Close();
}

void Analysis::Init(TTree *tree){
  if (!tree) std::cout << " no tree" << std::endl;
  if (!tree) return;
  fChain = tree;

  setupTriggerBranches(fChain);
  if(isData)
    setupMETCleaningBranches(fChain);
  else
    setupGenLevelBranches(fChain);

  setupBranches(fChain);
  /*
  std::cout << "check nJet " << nJet.IsValid() << " " << nJet.GetSetupStatus() << std::endl;
  std::cout << "check nGenJet " << nGenJet.IsValid() << " " << nGenJet.GetSetupStatus() << std::endl;
  //  if(!nGenJet.IsValid()) tree->DropBranchFromCache ("nGenJet");
  //fReader.SetTree(tree);
  //if(!nGenJet.IsValid()) tree->DropBranchFromCache ("nGenJet");
  std::cout << "check nJet2 " << nJet.IsValid() << " " << nJet.GetSetupStatus() << std::endl;
  std::cout << "check nGenJet2 " << nGenJet.IsValid() << " " << nGenJet.GetSetupStatus() << std::endl;
  */

  //    year = "2016";
  std::string runPeriod(year);
  std::regex run_re("_Run(\\d+\\S)_");
  std::regex year_re("_Run(\\d+)\\S_");
  std::smatch match;
  std::string dsetName = std::string(datasetName);
  if (std::regex_search(dsetName, match, run_re) && match.size() > 0) {
    runPeriod = match.str(1);
    //    if (std::regex_search(dsetName, match, year_re) && match.size() > 0) {
      //std::cout << "check match " << match.str(1) << std::endl;
      ////      year = match.str(1);
    //}
    //    std::cout << "check match " << match.str(1) << " " << match.str(2) << std::endl;
    //    year = match.str(1)
  }
  if (runPeriod == "2017D" || runPeriod == "2017E") runPeriod = "2017DE";

  std::string prefix = "Fall17_17Nov";
  std::string version = "V32";
  std::string data = "DATA";

  // Jet corr txt files at https://github.com/cms-jet/JECDatabase/tree/master/textFiles
  const char *sd = "src/CondFormats/JetMETObjects/data";
  //  Summer16_07Aug2017GH_V11_DATA_L1FastJet_AK4PFchs.txt
  if(std::string(year).compare("2016") == 0){
    prefix = "Summer16_07Aug";
    if (runPeriod == "2016B" || runPeriod == "2016C" || runPeriod == "2016D") runPeriod = "2017BCD";
    else if (runPeriod == "2016E" || runPeriod == "2016F") runPeriod = "2017EF";
    else if (runPeriod == "2016G" || runPeriod == "2016H") runPeriod = "2017GH";  
    else runPeriod = "2017";
    version= "V11";
  }
  if(std::string(year).compare("2018") == 0){
    prefix = "Autumn18_";
    runPeriod = runPeriod.replace(0,4,"Run");
    if(!isData) {
      prefix = "Autumn18";
      runPeriod = "";
    }
    version= "V19";
  }
  if(!isData){
    data = "MC";
    if(runPeriod.size() >= 4) runPeriod = runPeriod.replace(4,runPeriod.length(),"");
  }

  const char *st = Form("%s%s_%s_%s",prefix.c_str(),runPeriod.c_str(),version.c_str(),data.c_str());
  const char *s;

  s = Form("%s/%s_L1RC_AK4PFchs.txt",sd,st); std::cout << s << std::endl;
  l1rc = new JetCorrectorParameters(s);

  s = Form("%s/%s_L1FastJet_AK4PFchs.txt",sd,st); std::cout << s << std::endl;
  if(!exists(std::string(s))) {
    std::cout << "File " << s << " not found" << std::endl;
    exit(1);
  }

  l1 = new JetCorrectorParameters(s);
  s = Form("%s/%s_L2Relative_AK4PFchs.txt",sd,st); std::cout << s << std::endl;
  l2 = new JetCorrectorParameters(s);
  s = Form("%s/%s_L3Absolute_AK4PFchs.txt",sd,st); std::cout << s << std::endl;
  l3 = new JetCorrectorParameters(s);
  s = Form("%s/%s_L2L3Residual_AK4PFchs.txt",sd,st); std::cout << s << std::endl;
  l2l3 = new JetCorrectorParameters(s);

  std::vector<JetCorrectorParameters> v0;
  v0.push_back(*l1rc);
  jec_l1rc = new FactorizedJetCorrector(v0);

  std::vector<JetCorrectorParameters> v1;
  v1.push_back(*l1);
  jec_l1 = new FactorizedJetCorrector(v1);

  std::vector<JetCorrectorParameters> v2;
  v2.push_back(*l1);
  v2.push_back(*l2);
  v2.push_back(*l3);
  jec_noL2L3 = new FactorizedJetCorrector(v2);

  std::vector<JetCorrectorParameters> v3;
  v3.push_back(*l1);
  v3.push_back(*l2);
  v3.push_back(*l3);
  v3.push_back(*l2l3);
  jec_withL2L3 = new FactorizedJetCorrector(v3);
}


template <class T>
T Analysis::GetParameter (const char* parameterName) {
  TList* inputList = this->GetInputList();
  T obj = (T)inputList->FindObject(parameterName);
  return obj;
}

std::vector<std::string> Analysis::GetParameters (const char* regexp) {
  std::vector<std::string> parameters;

  std::regex r(regexp);
  TList* inputList = this->GetInputList();
  for(int i = 0; i < inputList->GetEntries(); ++i){
    TNamed* name = (TNamed*)inputList->At(i);
    //std::cout << "check parameters " << name->GetName() << " " << name->GetTitle() << std::endl;
    if(std::regex_match(name->GetName(),r)) parameters.push_back(name->GetTitle());
  }
  //std::cout << "check para len " << parameters.size() << std::endl;
  return parameters;
}

void Analysis::setupTriggerBranches(TTree *tree){
  //triggerNames = this->GetParameters("trigger");
  
  /*
  const char* trg = this->GetParameter<TNamed*>("trigger")->GetTitle();
  //std::cout << "check trg " << trg << std::endl;
  triggerNames.clear();
  std::string s_trg = std::string(trg);
  size_t pos = s_trg.find("||");
  //std::cout << "check pos " << pos << std::endl;
  
  if (pos >= s_trg.size()){
    triggerNames.push_back(s_trg);
  }else{
    while (pos < s_trg.size()){
      pos = s_trg.find("||");
      triggerNames.push_back(s_trg.substr(0,pos));
      s_trg = s_trg.substr(pos+2,s_trg.size());
    }
  }
  */
    
  triggerBit = new Bool_t[triggerNames.size()];
  for(size_t i = 0; i < triggerNames.size(); ++i){
    if(tree->FindBranch(triggerNames[i].c_str()) == 0){ 
      std::cout << "\033[1;31mWarning, trying to access trigger " << triggerNames[i] << ", which does not exist in " << datasetName << "\033[0m" << std::endl;
      //std::cout << "Exiting..\n" << std::endl;
      //exit(1);
    }else tree->SetBranchAddress(triggerNames[i].c_str(),&triggerBit[i]);
  }
}

void Analysis::setupMETCleaningBranches(TTree *tree){
  filterFlags = this->GetParameters("Filters");
  filter = new Bool_t[filterFlags.size()];
  for(size_t i = 0; i < filterFlags.size(); ++i){
    tree->SetBranchAddress(filterFlags[i].c_str(),&filter[i]);
    //std::cout << "check flags " << filterFlags[i] << std::endl;
  }
}

void Analysis::setupGenLevelBranches(TTree *tree){
  
}

void Analysis::setupBranches(TTree *tree){
  tree->SetBranchAddress("run",&run, &b_run);
  tree->SetBranchAddress("luminosityBlock", &luminosityBlock, &b_luminosityBlock);
  //  tree->SetBranchAddress("event", &event, b_event);

  if(!isData){
    tree->SetBranchAddress("Pileup_nTrueInt", &Pileup_nTrueInt, &b_Pileup_nTrueInt);
    tree->SetBranchAddress("nPSWeight", &nPSWeight, &b_nPSWeight);
    tree->SetBranchAddress("PSWeight", PSWeight, &b_PSWeight);
  }
  
  tree->SetBranchAddress("fixedGridRhoFastjetAll", &fixedGridRhoFastjetAll, &b_fixedGridRhoFastjetAll);
  tree->SetBranchAddress("PV_npvsGood", &PV_npvsGood, &b_PV_npvsGood);

  tree->SetBranchAddress("nMuon", &nMuon, &b_nMuon);
  tree->SetBranchAddress("Muon_pt", Muon_pt, &b_Muon_pt);
  tree->SetBranchAddress("Muon_eta", Muon_eta, &b_Muon_eta);
  tree->SetBranchAddress("Muon_phi", Muon_phi, &b_Muon_phi);
  tree->SetBranchAddress("Muon_mass", Muon_mass, &b_Muon_mass);
  tree->SetBranchAddress("Muon_charge", Muon_charge, &b_Muon_charge);
  tree->SetBranchAddress("Muon_softId", Muon_softId, &b_Muon_softId);
  tree->SetBranchAddress("Muon_mediumPromptId", Muon_mediumPromptId, &b_Muon_mediumPromptId);
  tree->SetBranchAddress("Muon_miniPFRelIso_all", Muon_miniPFRelIso_all, &b_Muon_miniPFRelIso_all);
  tree->SetBranchAddress("Muon_pfRelIso03_all",  Muon_pfRelIso03_all, &b_Muon_pfRelIso03_all);
  tree->SetBranchAddress("Muon_pfRelIso04_all",  Muon_pfRelIso04_all, &b_Muon_pfRelIso04_all);

  tree->SetBranchAddress("nElectron", &nElectron, &b_nElectron);
  tree->SetBranchAddress("Electron_pt", Electron_pt, &b_Electron_pt);
  tree->SetBranchAddress("Electron_eta", Electron_eta, &b_Electron_eta);
  tree->SetBranchAddress("Electron_phi", Electron_phi, &b_Electron_phi);
  tree->SetBranchAddress("Electron_mass", Electron_mass, &b_Electron_mass);
  tree->SetBranchAddress("Electron_charge", Electron_charge, &b_Electron_charge);
  tree->SetBranchAddress("Electron_cutBased", Electron_cutBased, &b_Electron_cutBased);
  //  tree->SetBranchAddress("Electron_mvaFall17Iso_WP90", Electron_mvaFall17Iso_WP90, &b_Electron_mvaFall17Iso_WP90);
  tree->SetBranchAddress("Electron_miniPFRelIso_all", Electron_miniPFRelIso_all, &b_Electron_miniPFRelIso_all);
  tree->SetBranchAddress("Electron_pfRelIso03_all", Electron_pfRelIso03_all, &b_Electron_pfRelIso03_all);
  tree->SetBranchAddress("Electron_dxy", Electron_dxy, &b_Electron_dxy);
  tree->SetBranchAddress("Electron_dz", Electron_dz, &b_Electron_dz);
  tree->SetBranchAddress("Electron_mvaFall17V2Iso_WP90", Electron_mvaFall17V2Iso_WP90, &b_Electron_mvaFall17V2Iso_WP90);
  tree->SetBranchAddress("Electron_cutBased_Fall17_V1", Electron_cutBased_Fall17_V1, &b_Electron_cutBased_Fall17_V1);
  
  tree->SetBranchAddress("nJet", &nJet, &b_nJet);
  tree->SetBranchAddress("Jet_area", Jet_area, &b_Jet_area);
  tree->SetBranchAddress("Jet_pt", Jet_pt, &b_Jet_pt);
  tree->SetBranchAddress("Jet_eta", Jet_eta, &b_Jet_eta);
  tree->SetBranchAddress("Jet_phi", Jet_phi, &b_Jet_phi);
  tree->SetBranchAddress("Jet_mass", Jet_mass, &b_Jet_mass);

  tree->SetBranchAddress("Jet_electronIdx1", Jet_electronIdx1, &b_Jet_electronIdx1);
  tree->SetBranchAddress("Jet_electronIdx2", Jet_electronIdx2, &b_Jet_electronIdx2);
  tree->SetBranchAddress("Jet_muonIdx1", Jet_muonIdx1, &b_Jet_muonIdx1);
  tree->SetBranchAddress("Jet_muonIdx2", Jet_muonIdx2, &b_Jet_muonIdx2);

  tree->SetBranchAddress("Jet_jetId", Jet_jetId, &b_Jet_jetId);
  tree->SetBranchAddress("Jet_nElectrons", Jet_nElectrons, &b_Jet_nElectrons);
  tree->SetBranchAddress("Jet_nMuons", Jet_nMuons, &b_Jet_nMuons);
  tree->SetBranchAddress("Jet_puId", Jet_puId, &b_Jet_puId);

  tree->SetBranchAddress("Jet_nConstituents", Jet_nConstituents, &b_Jet_nConstituents);
  tree->SetBranchAddress("Jet_neEmEF", Jet_neEmEF, &b_Jet_neEmEF);
  tree->SetBranchAddress("Jet_neHEF", Jet_neHEF, &b_Jet_neHEF);
  tree->SetBranchAddress("Jet_muEF", Jet_muEF, &b_Jet_muEF);
  tree->SetBranchAddress("Jet_chEmEF", Jet_chEmEF, &b_Jet_chEmEF);
  tree->SetBranchAddress("Jet_chHEF", Jet_chHEF, &b_Jet_chHEF);

  
  tree->SetBranchAddress("Jet_btagCMVA", Jet_btagCMVA, &b_Jet_btagCMVA);
  tree->SetBranchAddress("Jet_btagCSVV2", Jet_btagCSVV2, &b_Jet_btagCSVV2);
  tree->SetBranchAddress("Jet_btagDeepB", Jet_btagDeepB, &b_Jet_btagDeepB);
  tree->SetBranchAddress("Jet_btagDeepC", Jet_btagDeepC, &b_Jet_btagDeepC);
  tree->SetBranchAddress("Jet_btagDeepFlavB", Jet_btagDeepFlavB, &b_Jet_btagDeepFlavB);

  tree->SetBranchAddress("Jet_qgl", Jet_qgl, &b_Jet_qgl);

  if(!isData){
    tree->SetBranchAddress("Jet_hadronFlavour", Jet_hadronFlavour, &b_Jet_hadronFlavour);
    tree->SetBranchAddress("Jet_partonFlavour", Jet_partonFlavour, &b_Jet_partonFlavour);

    tree->SetBranchAddress("nGenJet", &nGenJet, &b_nGenJet);
    tree->SetBranchAddress("GenJet_pt", GenJet_pt, &b_GenJet_pt);
    tree->SetBranchAddress("GenJet_eta", GenJet_eta, &b_GenJet_eta);
    tree->SetBranchAddress("GenJet_phi", GenJet_phi, &b_GenJet_phi);
    tree->SetBranchAddress("GenJet_mass", GenJet_mass, &b_GenJet_mass);
  }  
  tree->SetBranchAddress("MET_pt", &MET_pt, &b_MET_pt);
  tree->SetBranchAddress("MET_phi", &MET_phi, &b_MET_phi);

  tree->SetBranchAddress("RawMET_pt", &RawMET_pt, &b_RawMET_pt);
  tree->SetBranchAddress("RawMET_phi", &RawMET_phi, &b_RawMET_phi);

  tree->SetBranchAddress("MET_MetUnclustEnUpDeltaX", &MET_MetUnclustEnUpDeltaX, &b_MET_MetUnclustEnUpDeltaX);
  tree->SetBranchAddress("MET_MetUnclustEnUpDeltaY", &MET_MetUnclustEnUpDeltaY, &b_MET_MetUnclustEnUpDeltaY);

  if(!isData){
    tree->SetBranchAddress("GenMET_pt", &GenMET_pt, &b_GenMET_pt);
    tree->SetBranchAddress("GenMET_phi", &GenMET_phi, &b_GenMET_phi);
  }

  if(!isData){
    tree->SetBranchAddress("nGenPart", &nGenPart, &b_nGenPart);
    tree->SetBranchAddress("GenPart_pt", &GenPart_pt, &b_GenPart_pt);
    tree->SetBranchAddress("GenPart_eta", &GenPart_eta, &b_GenPart_eta);
    tree->SetBranchAddress("GenPart_phi", &GenPart_phi, &b_GenPart_phi);
    tree->SetBranchAddress("GenPart_mass", &GenPart_mass, &b_GenPart_mass);
    tree->SetBranchAddress("GenPart_pdgId", &GenPart_pdgId, &b_GenPart_pdgId);
    tree->SetBranchAddress("GenPart_genPartIdxMother", &GenPart_genPartIdxMother, &b_GenPart_genPartIdxMother);
    tree->SetBranchAddress("GenPart_status", &GenPart_status, &b_GenPart_status);
  }
}

bool Analysis::getTriggerDecision(){
  //std::cout << "check Analysis::getTriggerDecision " << triggerNames.size() << "  " << triggerBit[0] << std::endl;
  bool decision = false;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    //    if(triggerBit[i]) triggerBit[i] = 0;
    //std::cout << triggerNames[i] << " " << triggerBit[i] << std::endl;
    decision = decision || triggerBit[i];
  }
  //std::cout << "trg decision " << decision << std::endl;
  return decision;
}

bool Analysis::triggeredBy(std::string regexp){
  std::regex word_regex(regexp);
  bool decision = false;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    //std::cout << "check triggeredBy " << triggerNames[i] << " " << regexp << " " << std::regex_search(triggerNames[i], word_regex) << std::endl;
    if (std::regex_search(triggerNames[i], word_regex) && triggerBit[i]) decision = true;
  }
  return decision;
}

bool Analysis::METCleaning(){
  if(!isData) return true;
  
  //std::cout << "check Analysis::getTriggerDecision " << triggerNames.size() << "  " << triggerBit[0] << std::endl;
  bool decision = false;
  for(size_t i = 0; i < filterFlags.size(); ++i){
    //    if(filter[i]) filter[i] = 0;
    //std::cout << filterFlags[i] << " " << filter[i] << std::endl;
    decision = decision || filter[i];
  }
  return decision;
}

//void Analysis::setupBranches(BranchManager& branchManager) {}

std::vector<TLorentzVector> Analysis::getMuons(int charge=0){
  std::vector<TLorentzVector> muons;
  
  double ptmin = 20;
  //double etamax = 2.3;
  double etamax = 2.4;
  double pfRelIsoMax = 0.15;
  
  int n = nMuon;
  //std::cout << "check nMuons " << n << std::endl;
  for(int i = 0; i < n; ++i){
    if(Muon_pt[i] > ptmin &&
       fabs(Muon_eta[i]) < etamax &&
       Muon_tightId[i] &&
       //Muon_pfRelIso03_all[i] < pfRelIsoMax){
       Muon_pfRelIso04_all[i] < pfRelIsoMax){
      double pt = Muon_pt[i];
      double eta = Muon_eta[i];
      double phi = Muon_phi[i];
      double m = Muon_mass[i];
      double q = Muon_charge[i];
      
      TLorentzVector mu;
      mu.SetPtEtaPhiM(pt,eta,phi,m);

      //std::cout << "  check muon pt " << pt << " " << Muon_charge[i] << " " << q << std::endl;
      if(charge == 0){
	muons.push_back(mu);
      }else{
	if(fabs(q-charge) < 0.001) muons.push_back(mu);
      }
    }
  }
  return muons;
}

bool Analysis::passElectronID(int iElectron){
  bool pass = true;

  //  if(Electron_mvaFall17V2Iso_WP80[iElectron] < 0.5) return false;
  if(!(Electron_cutBased_Fall17_V1[iElectron] == 4)) return false; //0:fail, 1:veto, 2:loose, 3:medium, 4:tight
  //std::cout << "check electron id " << nElectron << " " << iElectron << " " << Electron_cutBased_Fall17_V1[iElectron] << " " << Electron_cutBased_Fall17_V1[0] << std::endl;
  // electron IP
  if(fabs(Electron_eta[iElectron]) <= 1.479){   
    if(Electron_dxy[iElectron] > 0.05) return false;
    if(Electron_dz[iElectron] > 0.10) return false;
  }else{
    if(Electron_dxy[iElectron] > 0.10) return false;
    if(Electron_dz[iElectron] > 0.20) return false;
  }
  return pass;
}

std::vector<TLorentzVector> Analysis::getElectrons(int charge=0){
  std::vector<TLorentzVector> cands;
  
  double ptmin = 25;
  double etamax = 2.4;
  double pfRelIsoMax = 1.0;//0.4;//0.15;

  int n = nElectron;
  //std::cout << "check nElectrons " << n << std::endl;
  for(int i = 0; i < n; ++i){
    //    std::cout << "  check electron pt " << Electron_pt[i] << " " << Electron_eta[i] << " " << Electron_phi[i] << " " << Electron_mass[i] << std::endl;
    //std::cout << "  check electron iso " << Electron_cutBased[i] << " " <<  " " << Electron_miniPFRelIso_all[i] << std::endl;
    if(Electron_pt[i] > ptmin &&
       fabs(Electron_eta[i]) < etamax &&
       //       int(Electron_cutBased[i]) == 3
       //       Electron_mvaSpring16GP_WP90[i] &&
       Electron_miniPFRelIso_all[i] < pfRelIsoMax
       //Electron_pfRelIso03_all[i] < pfRelIsoMax
       ){

      if(!passElectronID(i)) continue;

      Float_t pt = Electron_pt[i];
      Float_t eta = Electron_eta[i];
      Float_t phi = Electron_phi[i];
      TVector3 tv3;
      tv3.SetPtEtaPhi(pt,eta,phi);
      //std::cout << "check tv3 " << tv3.Mag()  << std::endl;
      //Float_t m = 0.0; //Electron_mass[i];
      Int_t q = Electron_charge[i];
      //std::cout << "check m " << Electron_mass[i] << " " << m << " " << Double_t(m) << std::endl;
      //std::cout << "check q " << Electron_charge[i] << " " << charge << " " << std::endl;
      TLorentzVector cand;
      //      cand.SetPtEtaPhiM(pt,eta,phi,m);
      cand.SetPtEtaPhiE(pt,eta,phi,tv3.Mag());
      if(charge == 0){
	cands.push_back(cand);
      }else{
	//std::cout << "check q " << Electron_charge[i] << " " << charge << " " << q-charge << std::endl;
        if(fabs(q-charge) < 0.001) cands.push_back(cand);
	//std::cout << "check ele " << cand.Pt() << " " << cand.Eta() << " " << cand.M() << std::endl;
      }
    }
  }
  //std::cout << "check cands " << cands.size() << std::endl;
  return cands;
}

std::vector<TLorentzVector> Analysis::getGenParticles(int pid){
  //.,.
  std::vector<TLorentzVector> cands;
  for(size_t i = 0; i < nGenPart; ++i){
    if(GenPart_pdgId[i] != pid) continue;
    if(abs(GenPart_pdgId[i]) == GenPart_genPartIdxMother[i]) continue;
    //std::cout << "check mother " << GenPart_pdgId[i] << " " << GenPart_genPartIdxMother[i] << std::endl;
    //std::cout << "check genparticles " << i << " " << GenPart_status[i] << std::endl;
    double pt = GenPart_pt[i];
    double eta = GenPart_eta[i];
    double phi = GenPart_phi[i];
    double m = GenPart_mass[i];

    TLorentzVector cand;
    cand.SetPtEtaPhiM(pt,eta,phi,m);
    cands.push_back(cand);
  }
  return cands;
}

/*
std::vector<TLorentzVector> Analysis::getJets() {
  std::vector<TLorentzVector> cands;

  double ptmin = 15;
  double etamax = 2.5;

  int n = nJet;
  h_njets->Fill(nJet);
  //std::cout << "check nJet " << n << std::endl;
  TLorentzVector leadingJet(0,0,0,0);
  for(int i = 0; i < n; ++i){
    if(Jet_pt[i] > ptmin &&
       fabs(Jet_eta[i]) < etamax){
      double pt  = Jet_pt[i];
      double eta = Jet_eta[i];
      double phi = Jet_phi[i];
      double m   = Jet_mass[i];

      if(pt > leadingJet.Pt()) leadingJet.SetPtEtaPhiE(pt,eta,phi,m);
    }
  }
  
      
      
      h_Jet_jetId->Fill(Jet_jetId[i]);
      h_Jet_electronIdx1->Fill(Jet_electronIdx1[i]);
      h_Jet_electronIdx2->Fill(Jet_electronIdx2[i]);
      h_Jet_muonIdx1->Fill(Jet_muonIdx1[i]);
      h_Jet_muonIdx2->Fill(Jet_muonIdx2[i]);
      h_Jet_nElectrons->Fill(Jet_nElectrons[i]);
      h_Jet_nMuons->Fill(Jet_nMuons[i]);
      h_Jet_puId->Fill(Jet_puId[i]);
      //std::cout << "check jet" << pt << " " << eta << " " << phi << " " << Jet_jetId[i] << " " << Jet_electronIdx1[i] << " " << Jet_electronIdx2[i] << " " << Jet_muonIdx1[i] << " " << Jet_muonIdx2[i] << " " << Jet_nElectrons[i] << " " << Jet_nMuons[i] << " " << Jet_puId[i] << std::endl;

//      if(Jet_electronIdx1[i] >= 0) continue;
//      if(Jet_electronIdx2[i] >= 0) continue;
//      if(Jet_muonIdx1[i] >= 0) continue;
//      if(Jet_muonIdx2[i] >= 0) continue;
//      if(Jet_nElectrons[i] > 0) continue;
//      if(Jet_nMuons[i] > 0) continue;

      TLorentzVector cand;
      cand.SetPtEtaPhiE(pt,eta,phi,m);

      cands.push_back(cand);
    }
  }
  return cands;
}
*/

std::vector<int> Analysis::getJetIndexList(){
  std::vector<int> jetIndices;
  for(size_t i = 0; i < nJet; ++i){
    jetIndices.push_back(i);
  }
  return jetIndices;
}
TLorentzVector Analysis::getJet(int i){
  if(i < 0) return TLorentzVector(0,0,0,0);
  double pt  = Jet_pt[i];
  double eta = Jet_eta[i];
  double phi = Jet_phi[i];
  double m   = Jet_mass[i];
  TLorentzVector jet;
  jet.SetPtEtaPhiM(pt,eta,phi,m);
  return jet;
}
TLorentzVector Analysis::getGenJet(int i){
  if(i < 0) return TLorentzVector(0,0,0,0);
  double pt  = GenJet_pt[i];
  double eta = GenJet_eta[i];
  double phi = GenJet_phi[i];
  double m   = GenJet_mass[i];
  TLorentzVector jet;
  jet.SetPtEtaPhiM(pt,eta,phi,m);
  return jet;
}
std::vector<int> Analysis::drClean(std::vector<int> jetIndices,std::vector<TLorentzVector> compare, double cone){
  std::vector<int> cleaned;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    TLorentzVector jet = getJet(*i);
    bool passed = true;
    for(size_t j = 0; j < compare.size(); ++j){
      double DR = ROOT::Math::VectorUtil::DeltaR(jet,compare[j]);
      if(DR < cone) passed = false;
    }
    if(passed) cleaned.push_back(*i);
  }
  return cleaned;
}

std::vector<int> Analysis::jetId(std::vector<int> jetIndices){
  std::vector<int> cleaned;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    if(Jet_jetId[*i] > 1) cleaned.push_back(*i);
  }
  return cleaned;
}

int Analysis::getLeadingJetIndex(std::vector<int> jetIndices, size_t nth){
  if(jetIndices.size() < nth+1) return -1;
  double ptmin = 12;
  double etamax = 2.5;//1.3; divided in eta bins 0-1.3, 1.3-1.9, 1.9-2.5

  int leadingJetIndex = -1;
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    if(Jet_pt[*i] < ptmin) continue;
    if(fabs(Jet_eta[*i]) > etamax) continue;
    
    if(leadingJetIndex < 0) leadingJetIndex = *i;
    else{
      if(Jet_pt[*i] > Jet_pt[leadingJetIndex]) leadingJetIndex = *i;
    }
  }
  if(leadingJetIndex < 0) return -1;

  std::vector<int> reordered;
  reordered.push_back(leadingJetIndex);
  // assuming the jets are pt ordered
  for(std::vector<int>::const_iterator i = jetIndices.begin(); i != jetIndices.end(); ++i){
    if( *i != leadingJetIndex) reordered.push_back(*i);
  }
  //std::cout << "check Analysis::JetSelection::leadingJet " << jetIndices.size() << std::endl;
  return reordered[nth];
}
/*
std::vector<TLorentzVector> Analysis::drClean(std::vector<TLorentzVector> orig, std::vector<TLorentzVector> compare, double cone){
  std::vector<TLorentzVector> selected;
  for(size_t i = 0; i < orig.size(); ++i){
    for(size_t j = 0; j < compare.size(); ++j){
      double DR = ROOT::Math::VectorUtil::DeltaR(orig[i],compare[j]);
      if(DR < cone) continue;
      selected.push_back(orig[i]);
    }
  }
  return selected;
}
*/																																      
TLorentzVector Analysis::getZboson(std::vector<TLorentzVector> leptPlus,std::vector<TLorentzVector> leptMinus) {
  double mZ = 91.1876;
  TLorentzVector Zboson(0,0,0,0);
  std::vector<TLorentzVector> lp;
  std::vector<TLorentzVector> lm;
  for(size_t i=0; i < leptPlus.size(); ++i){
    for(size_t j=0; j < leptMinus.size(); ++j){
      //std::cout << "check lepton1: " << leptPlus[i].Pt() << " " << leptPlus[i].Eta() << " " << leptPlus[i].Phi() << " " << leptPlus[i].M() << std::endl;
      //std::cout << "check lepton2: " << leptMinus[i].Pt() << " " << leptMinus[i].Eta() << " " << leptMinus[i].Phi() << " " << leptMinus[i].M() << std::endl;
      TLorentzVector Zbosoncand = leptPlus[i]+leptMinus[j];
      //std::cout << "check Z " << Zbosoncand.Pt() << " " << Zbosoncand.M() << std::endl;
      if(Zbosoncand.Pt() < 30) continue;
      if(fabs(Zbosoncand.M()-mZ) > 20 ) continue;
      if(fabs(Zbosoncand.M()-mZ) < fabs(Zboson.M()-mZ) ) {
	Zboson = Zbosoncand;
	lp.clear(); lp.push_back(leptPlus[i]);
	lm.clear(); lm.push_back(leptMinus[i]);
      }
    }
  }
  leptonsPlus = lp;
  leptonsMinus = lm;
  
  return Zboson;
}
/*
TLorentzVector Analysis::selectLeadingJet(std::vector<TLorentzVector> jets, size_t nth){
  if(jets.size() >= nth) return jets[nth];
  else return TLorentzVector(0,0,0,0);
}
*/
double Analysis::alphaCut(double zpt){
  double theCut = 0.3;
  if(zpt>50 && zpt < 100) theCut = 0.6;
  if(zpt < 50) theCut = 1.0;
  theCut = 1.0; // cut disabled
  return theCut;
}

#include <sys/stat.h>
bool Analysis::exists(const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

#include <regex>
TVector3 Analysis::recalculateMET(std::vector<int> jets, float rho){
  /*
  // Type1 from Raw MET
  TVector3 met;
  met.SetPtEtaPhi(RawMET_pt,0,RawMET_phi);

  for(size_t i = 0; i < jets.size(); ++i){

    if(Jet_pt[i] < 15) continue;
    if(2.5 < fabs(Jet_eta[i]) && fabs(Jet_eta[i]) < 3.0 && Jet_pt[i] < 70) continue; 
    
    jec_l1rc->setJetPt(Jet_pt[i]);
    jec_l1rc->setJetEta(Jet_eta[i]);
    jec_l1rc->setJetA(Jet_area[i]);
    jec_l1rc->setRho(rho);
    float corr_L1 = jec_l1rc->getCorrection();
      
    jec_withL2L3->setJetPt(Jet_pt[i]);
    jec_withL2L3->setJetEta(Jet_eta[i]);
    jec_withL2L3->setJetA(Jet_area[i]);
    jec_withL2L3->setRho(rho);
    float corr_withL2L3 = jec_withL2L3->getCorrection();
    //    std::cout << "check jet " << Jet_pt[i] << " " << corr1 << " " << corr2 << std::endl;

    TVector3 jet;
    jet.SetPtEtaPhi(Jet_pt[i],0,Jet_phi[i]);
    met += (corr_L1 - corr_withL2L3)*jet;
    //    met += (1-corr_withL2L3)*jet;
  }
  return met;
  */
    
  TVector3 met;
  met.SetPtEtaPhi(MET_pt,0,MET_phi);
  //std::cout << "check met " << MET_pt << " " << MET_phi << std::endl;
  /*
  for(size_t i = 0; i < jets.size(); ++i){
    jec_noL2L3->setJetPt(Jet_pt[i]);
    jec_noL2L3->setJetEta(Jet_eta[i]);
    jec_noL2L3->setJetA(Jet_area[i]);
    jec_noL2L3->setRho(rho);
    float corr_noL2L3 = jec_noL2L3->getCorrection();

    jec_withL2L3->setJetPt(Jet_pt[i]);
    jec_withL2L3->setJetEta(Jet_eta[i]);
    jec_withL2L3->setJetA(Jet_area[i]);
    jec_withL2L3->setRho(rho);
    float corr_withL2L3 = jec_withL2L3->getCorrection();
    //    std::cout << "check jet " << Jet_pt[i] << " " << corr1 << " " << corr2 << std::endl;

    TVector3 jet;
    jet.SetPtEtaPhi(Jet_pt[i],0,Jet_phi[i]);
    ////    met += (corr_noL2L3 - corr_withL2L3)*jet;
  }
  */
  return met;
}

bool Analysis::passing(double value, std::string cut){
  //  cuts = {"0_30","30_50","50_70","70_100","100_140","140_200","200_Inf"}
  std::regex cut_re("(\\S+)_(\\S+)");
  std::smatch match;
  if (std::regex_search(cut, match, cut_re)){
    //std::cout << "check Analysis::passing re " << match.size() << " " << match[1] << " " << match[2] << std::endl;
    float lowEdge = atof(match.str(1).c_str());
    float highEdge = atof(match.str(2).c_str());
    //std::cout << "check Analysis::passing " << value << " " << lowEdge  << " " << highEdge << std::endl;
    if(lowEdge < fabs(value) && fabs(value) < highEdge) return true;
    if(highEdge == 0 && lowEdge < fabs(value)) return true;
    //    std::cout << "check atoi " << lowEdge << " " << highEdge << std::endl; 
  }
  //  exit(0);
  return false;
}

int Analysis::getJetFlavour(int jetIndex){
  if(isData) return -1;
  //std::cout << "check jet flavor " << Jet_hadronFlavour[jetIndex] << " " << Jet_partonFlavour[jetIndex] << std::endl;
  return abs(Jet_partonFlavour[jetIndex]);
}
/*
size_t Analysis::getEtaBin(double eta){
  for(size_t i = 0; i < nbins_eta-1; ++i){
    if(fabs(eta) > bins_eta[i] && fabs(eta) < bins_eta[i+1]) return i;
  }
  return 999;
}
*/
bool Analysis::Process(Long64_t entry) {
  //std::cout << "check Analysis::Process " << std::endl;
  //if(entry%100000 == 0) std::cout << datasetName << " entry " << entry << "/" << fChain->GetEntries() << std::endl;
  //else std::cout << entry << std::endl;
  //std::streamsize sss = std::cout.precision();
  //if(entry%100000 == 0) std::cout << "  Processed " << std::setprecision(2) << std::setw(3) << cMain->getCount("all events")/fChain->GetEntries() << std::setprecision(sss) << std::setw(6) << "    \r";
  
  //  fChain->GetEntry(entry);
  fChain->GetTree()->GetEntry(entry);
  //  if(!isData) std::cout << "eventWeight " << eventWeight << ",  pileupWeight " << Pileup_nTrueInt << " " << pileupWeight->getWeight(Pileup_nTrueInt) << std::endl;
  double totEventWeight = 1.0;
  //if(!isData) totEventWeight = eventWeight*pileupWeight->getWeight(Pileup_nTrueInt);
  if(!isData) totEventWeight = pileupWeight->getWeight(Pileup_nTrueInt);
  //if(!isData) std::cout << "eventWeight " << totEventWeight << " " << Pileup_nTrueInt << " " << pileupWeight->getWeight(Pileup_nTrueInt) << std::endl;
  if(totEventWeight <= 0) return false;
  cMain->increment("all events",totEventWeight);
  //  if(!isData) std::cout << "all events " << cMain->getCount("all events") << std::endl;

  // PS weight
  psWeights[0] = 1.0;
  if(!isData){
    for(size_t i = 1; i < npsWeights; ++i) psWeights[i] = PSWeight[i-1];
    //std::cout << "check PSWeight " << PSWeight[0] << " " << PSWeight[1] << " " << PSWeight[2] << " " << PSWeight[3] << std::endl;
  }
  /*
  if(!isData)
  for(size_t iPS = 0; iPS < npsWeights; ++iPS){
    std::cout << "check ps weights " << psWeights[iPS] << std::endl;
    //    totEventWeight = totEventWeight*psWeights[iPS];
  }  
  */
  // trigger
  bool trgDecision = this->getTriggerDecision();
  if(! trgDecision) return false;
  cMain->increment("trigger",totEventWeight);
  //std::cout << "check pass trigger " << std::endl;

  bool metCleaningDecision = this->METCleaning();
  if(! metCleaningDecision) return false;
  cMain->increment("MET cleaning",totEventWeight);


  // leptons
  //std::cout << "check leptonflavor " << leptonflavor << std::endl;
  if(leptonflavor == 13){
    leptonsPlus  = getMuons(1);
    leptonsMinus = getMuons(-1);
  }
  if(leptonflavor == 11){
    leptonsPlus  = getElectrons(1);
    leptonsMinus = getElectrons(-1);
  }

  //std::cout << "leptons " << leptonsPlus.size() << " " << leptonsMinus.size() << std::endl;

  int nLepton = leptonsPlus.size() + leptonsMinus.size();
  if(nLepton < 2 || nLepton > 3) return false;

  cMain->increment("2-3 leptons",totEventWeight);
  if(nLepton == 2) cMain->increment("2 leptons",totEventWeight);

  // Z boson
  TLorentzVector Zboson = getZboson(leptonsPlus,leptonsMinus);
  /*
  std::cout << "check leptons " << std::endl;
  for(size_t i = 0; i < leptonsPlus.size(); ++i){
    std::cout << "check l+ " << leptonsPlus[i].Pt() << " " << leptonsPlus[i].Eta() << " " << leptonsPlus[i].Phi() << " " << std::endl;
  }
  for(size_t i = 0; i < leptonsMinus.size(); ++i){
    std::cout << "check l- " << leptonsMinus[i].Pt() << " " << leptonsMinus[i].Eta() << " " << leptonsMinus[i].Phi() << " " << std::endl;
  }
  */
  TLorentzVector genZboson;
  if(!isData){
    std::vector<TLorentzVector> genleptonsPlus = getGenParticles(-leptonflavor);
    std::vector<TLorentzVector> genleptonsMinus = getGenParticles(leptonflavor);
    /*
    for(size_t i = 0; i < genleptonsPlus.size(); ++i){
      std::cout << "check gen l+ " << genleptonsPlus[i].Pt() << " " << genleptonsPlus[i].Eta() << " " << genleptonsPlus[i].Phi() << " " << std::endl;
    }
    for(size_t i = 0; i < genleptonsMinus.size(); ++i){
      std::cout << "check gen l- " << genleptonsMinus[i].Pt() << " " << genleptonsMinus[i].Eta() << " " << genleptonsMinus[i].Phi() << " " << std::endl;
    }
    */
    genZboson = getZboson(genleptonsPlus,genleptonsMinus);
  }

  h_Zpt->Fill(Zboson.Pt());

  //if(Zboson.Pt() < 30) return false;
  //if(Zboson.Pt() < 50 || Zboson.Pt() > 70) return false;
  //if(Zboson.Pt() < 100 || Zboson.Pt() > 140) return false;
  //if(Zboson.Pt() < 70 || Zboson.Pt() > 100) return false;
  //if(Zboson.Pt() < 140 || Zboson.Pt() > 200) return false;
  if(! (Zboson.Pt() > 0) ) return false;
  //std::cout << "check Zboson.Pt " << Zboson.Pt() << std::endl;
  cMain->increment("Z boson",totEventWeight);



  std::vector<int> jetSelection = getJetIndexList();
  jetSelection = drClean(jetSelection,leptonsPlus);
  jetSelection = drClean(jetSelection,leptonsMinus);
  jetSelection = jetId(jetSelection);
  //std::cout << "check jetSelection " << getJetIndexList().size() << " " << jetSelection.size() << std::endl;
  h_njets_sele->Fill(jetSelection.size());
  if(jetSelection.size() == 0) return false;
  /*
  std::vector<TLorentzVector> jets = getJets();
  //size_t nj = jets.size();
  jets = drClean(jets,muonsPlus);
  jets = drClean(jets,muonsMinus);
  //std::cout << "check jet cleaning " << nj << " " << jets.size() << " " << nj - jets.size() << std::endl;
  h_njets_sele->Fill(jets.size());
  if(jets.size() == 0) return false;
  */
  cMain->increment("jets",totEventWeight);

  int leadingJetIndex = getLeadingJetIndex(jetSelection);
  //std::cout << "check jet id " << Jet_jetId[leadingJetIndex] << std::endl;
  /*
  if(!isData){
    int jetGenFlavor = getJetFlavour(leadingJetIndex);
    cJetLabel->increment("all jets");
    switch(jetGenFlavor){
    case 5: cJetLabel->increment("b jets");
      break;
    case 4: cJetLabel->increment("c jets");
      break;
    case 21: cJetLabel->increment("gluon jets");
      break;
    default: cJetLabel->increment("uds jets");
      break;
    }
  }
  */
  TLorentzVector leadingJet = getJet(leadingJetIndex);
  h_jet1pt->Fill(leadingJet.Pt());
  h_jet1eta->Fill(leadingJet.Eta());

  h_JetPt_muEF->Fill(leadingJet.Pt(),Jet_muEF[leadingJetIndex]);
  if(Jet_muEF[leadingJetIndex] < 0.90){
    h_JetPt_chHEF->Fill(leadingJet.Pt(),Jet_chHEF[leadingJetIndex]);
    h_JetPt_neHEF->Fill(leadingJet.Pt(),Jet_neHEF[leadingJetIndex]);
    h_JetPt_neEmEF->Fill(leadingJet.Pt(),Jet_neEmEF[leadingJetIndex]);
    h_JetPt_chEmEF->Fill(leadingJet.Pt(),Jet_chEmEF[leadingJetIndex]);
  }

  double DRmin = 999;
  int leadingGenJetIndex = -1;
  if(!isData){
    for(size_t i = 0; i < nGenJet; ++i){
      TLorentzVector gjet = getGenJet(i);
      double DR = ROOT::Math::VectorUtil::DeltaR(leadingJet,gjet);
      if(DR < DRmin){
	DRmin = DR;
	leadingGenJetIndex = i;
      }
    }
  }

  TLorentzVector leadingGenJet;
  if(leadingGenJetIndex >= 0){
    leadingGenJet = getGenJet(leadingGenJetIndex);
    //std::cout << "check leading jet1 " << leadingJet.Pt() << " " << leadingGenJet.Pt() << std::endl;
  }

  //  size_t etaBin = getEtaBin(leadingJet.Eta());

  //if(leadingJet.Pt() < 12 || etaBin > nbins_eta) return false;
  if(leadingJet.Pt() < 12 || fabs(leadingJet.Eta()) > 2.5) return false;
  //std::cout << "check leading jet1 " << cMain->getCount("leading jet") << " " << totEventWeight <<std::endl;
  cMain->increment("leading jet",totEventWeight);
  if(jetSelection[0] != 0) cMain->increment("  0-jet == muon",totEventWeight);
  if(leadingJetIndex == 0) cMain->increment("  leading jet == 0-jet",totEventWeight);
  //std::cout << "check leading jet2 " << cMain->getCount("leading jet") << std::endl;
  //if(cMain->getCount("leading jet") < 0) exit(0);

  double phiBB = fabs(ROOT::Math::VectorUtil::DeltaPhi(leadingJet,Zboson)-TMath::Pi());
  h_phibb->Fill(phiBB);
  if(phiBB > 0.34 && phiBB < 2*TMath::Pi()-0.34) return false;
  cMain->increment("phiBB",totEventWeight);

  TLorentzVector subLeadingJet = getJet(getLeadingJetIndex(jetSelection,1));
  if(subLeadingJet.Pt() > 0){ 
    h_jet2pt->Fill(subLeadingJet.Pt());
    h_jet2eta->Fill(subLeadingJet.Eta());
  }

  TLorentzVector jets_all(0,0,0,0);
  /*
  for(size_t i = 0; i < jetSelection.size(); ++i) jets_all += getJet(i);
  for(size_t i = 0; i < leptonsPlus.size(); ++i) jets_all += leptonsPlus[i];
  for(size_t i = 0; i < leptonsMinus.size(); ++i) jets_all += leptonsMinus[i];
  */
  std::vector<int> jetIndex_all = jetSelection; //getJetIndexList();
  /*
  std::vector<TLorentzVector> allMus;
  for(size_t i = 0; i < nMuon; ++i){
    double pt = Muon_pt[i];
    double eta = Muon_eta[i];
    double phi = Muon_phi[i];
    double m = Muon_mass[i];

    TLorentzVector mu;
    mu.SetPtEtaPhiM(pt,eta,phi,m);
    allMus.push_back(mu);
    //jets_all+=mu;
  }
  */
  //  jetIndex_all = drClean(jetIndex_all,allMus);
  for(size_t i = 0; i < jetIndex_all.size(); ++i){
    //TLorentzVector j = getJet(i);
    TLorentzVector j = getJet(jetIndex_all[i]);
    //if(j.Pt() > 15) jets_all+=j;
    jets_all+=j;
  }

  TLorentzVector jets_notLeadingJet = jets_all - leadingJet;
  //std::cout << "check jets " << jets_all.Pt() << " " << (jets_notLeadingJet+leadingJet).Pt() << std::endl;
  //cMain->increment("subleading jet",totEventWeight);
  /*
  // Z boson
  std::cout << "check1 " << muonsPlus.size() << " " << muonsMinus.size() << std::endl;
  TLorentzVector Zboson = getZboson(muonsPlus,muonsMinus);
  std::cout << "check2 " << muonsPlus.size() << " " << muonsMinus.size() << std::endl;
  
  h_Zpt->Fill(Zboson.Pt());

  if(Zboson.Pt() < 30) return false;
  //  if(Zboson.Pt() < 70 || Zboson.Pt() > 100) return false;
  //if(Zboson.Pt() < 140 || Zboson.Pt() > 200) return false;
  cMain->increment("Z boson",totEventWeight);
  */
  //for (size_t i = 0; i < jets.size(); ++i){
  //  std::cout << "check jet " << i << " " << jets[i].Pt() << std::endl;
  //}
  //std::cout << "check leading " << leadingJet.Pt() << std::endl;
  //std::cout << "check subleading " << subLeadingJet.Pt() << " " << Zboson.Pt() << std::endl;
  //std::cout << "check 1 " << Zboson.Pt() << std::endl;
  double alpha = subLeadingJet.Pt()/Zboson.Pt();
  //std::cout << "check 2" << std::endl;
  //  double alpha = subLeadingJet.Pt()/Zboson.Pt();
  if(subLeadingJet.Pt() < 15) alpha = 0;
  // if(subLeadingJet.Pt() < 20) alpha = 0;

  h_alpha->Fill(alpha);

  if(Zboson.Pt() * alpha > 15){
  if(alpha < 0.10) h_Zpt_a10->Fill(Zboson.Pt());
  if(alpha < 0.15) h_Zpt_a15->Fill(Zboson.Pt());
  if(alpha < 0.20) h_Zpt_a20->Fill(Zboson.Pt());
  if(alpha < 0.30) h_Zpt_a30->Fill(Zboson.Pt());
  if(alpha < 0.40) h_Zpt_a40->Fill(Zboson.Pt());
  if(alpha < 0.50) h_Zpt_a50->Fill(Zboson.Pt());
  if(alpha < 0.60) h_Zpt_a60->Fill(Zboson.Pt());
  if(alpha < 0.80) h_Zpt_a80->Fill(Zboson.Pt());
  if(alpha < 1.0)  h_Zpt_a100->Fill(Zboson.Pt());
  }
  for(size_t i = 0; i < cuts.size(); ++i){
    if(passing(Zboson.Pt(),cuts[i])) h_alphaZpt[i]->Fill(alpha);
  }

  if(alpha < 0.3) cMain->increment("  alpha0.3",totEventWeight);
  if(alpha < 1.0) cMain->increment("  alpha1.0",totEventWeight);


  TVector3 MET = recalculateMET(jetSelection,fixedGridRhoFastjetAll);

  TVector3 METuncl = -MET - jets_all.Vect() - Zboson.Vect();
  //TVector3 METuncl = jets_all.Vect();
  //std::cout << "check MET uncl " << METuncl.Pt() << " " << TVector3(MET_MetUnclustEnUpDeltaX,MET_MetUnclustEnUpDeltaY,0).Pt() << std::endl;  
  float R_pT = leadingJet.Pt()/Zboson.Pt();
  //std::cout << "check R_pT " << R_pT << " " << leadingJet.Pt() << " " << Zboson.Pt() << std::endl;
  //std::cout << "check h_RpT " << h_RpT->GetEntries() << std::endl;
  float R_MPF = 1 + MET.Pt()*(cos(MET.Phi())*Zboson.Px() + sin(MET.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

  float R_MPFjet1 = -leadingJet.Pt()*(cos(leadingJet.Phi())*Zboson.Px() + sin(leadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  float R_MPFjetn = -jets_notLeadingJet.Pt()*(cos(jets_notLeadingJet.Phi())*Zboson.Px() + sin(jets_notLeadingJet.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());
  float R_MPFuncl = -METuncl.Pt()*(cos(METuncl.Phi())*Zboson.Px() + sin(METuncl.Phi())*Zboson.Py())/(Zboson.Pt()*Zboson.Pt());

  float R_pTGen  = -1;
  float R_MPFGen = -1;
  if(!isData){
    R_pTGen = leadingGenJet.Pt()/genZboson.Pt();
    R_MPFGen = 1 + GenMET_pt*(cos(GenMET_phi)*genZboson.Px() + sin(GenMET_phi)*genZboson.Py())/(genZboson.Pt()*genZboson.Pt());
  }

  //.,.
  //std::cout << "check MET " << MET.Pt() << " " << (METuncl-leadingJet.Vect()-jets_notLeadingJet.Vect()).Pt() << std::endl;
  //std::cout << "check R_MPF " << R_MPF << " " << R_MPFjet1+R_MPFjetn+R_MPFuncl << " " << R_MPFjet1 << " " << R_MPFjetn << " " <<  R_MPFuncl <<std::endl;

  //h_Zpt_mZ->Fill(Zboson.Pt(),Zboson.M(),totEventWeight);
  //h_Zpt_mZgen->Fill(Zboson.Pt(),genZboson.M(),totEventWeight);

  if(Zboson.Pt() > 30 and fabs(Zboson.Eta()) < 2.5 and alpha < 0.3 and leadingJet.Pt() > 30 and fabs(leadingJet.Eta()) < 1.3) {
    if(!isData){
      h_Mu0->Fill(Pileup_nTrueInt,totEventWeight);
      h_RhoVsMu0->Fill(Pileup_nTrueInt,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu0->Fill(Pileup_nTrueInt,PV_npvsGood,totEventWeight);
    }else{
      float mu = getAvgPU(run,luminosityBlock);
      h_Mu0->Fill(mu,totEventWeight);
      h_RhoVsMu0->Fill(mu,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu0->Fill(mu,PV_npvsGood,totEventWeight);
    }
  }
  if(Zboson.Pt() > 100 and fabs(Zboson.Eta()) < 2.5 and alpha < 0.3 and leadingJet.Pt() > 30 and fabs(leadingJet.Eta()) < 1.3 and subLeadingJet.Pt() < 30) {
    if(!isData){
      h_Mu->Fill(Pileup_nTrueInt,totEventWeight);
      h_RhoVsMu->Fill(Pileup_nTrueInt,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu->Fill(Pileup_nTrueInt,PV_npvsGood,totEventWeight);
    }else{
      float mu = getAvgPU(run,luminosityBlock);
      h_Mu->Fill(mu,totEventWeight);
      h_RhoVsMu->Fill(mu,fixedGridRhoFastjetAll,totEventWeight);
      h_NpvVsMu->Fill(mu,PV_npvsGood,totEventWeight);
    }
  }
  //std::cout << "check R_MPF " << R_MPF << " " << R_MPFjet1+R_MPFjetn+R_MPFuncl << std::endl;

  int jetGenFlavor = -1;
  if(!isData) jetGenFlavor = getJetFlavour(leadingJetIndex);

  if(!isData){
    double JESRpTGen = -1;
    if(R_pTGen > 0) JESRpTGen = R_pT/R_pTGen;
    if(genZboson.Pt() == 0) JESRpTGen = -10; 
    //if(JESRpTGen <= 0) cout << JESRpTGen << " = " << R_pT << "/" << R_pTGen << " (here, leadingJet.Pt()/Zbonson.Pt() = " << leadingJet.Pt() << "/" << Zboson.Pt() << " and leadingGenJet.Pt()/genZboson.Pt() = " << leadingGenJet.Pt() << "/" << genZboson.Pt() << endl;
    h_pTGenJet_JESRpTGen->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
    hprof_pTGenJet_JESRpTGen->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
    h_pTGenZ_JESRpTGen->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
    double JESRMPFGen = -10;
    if(R_MPFGen > 0) JESRMPFGen = R_MPF/R_MPFGen;
    h_pTGenJet_JESRMPFGen->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
    hprof_pTGenJet_JESRMPFGen->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
    h_pTGenZ_JESRMPFGen->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
    
    switch(jetGenFlavor){
    case 5:
      h_pTGenJet_JESRpTGen_genb->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genb->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      hprof_pTGenJet_JESRpTGen_genb->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      hprof_pTGenJet_JESRMPFGen_genb->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_genb->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_genb->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    case 4:
      h_pTGenJet_JESRpTGen_genc->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genc->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      hprof_pTGenJet_JESRpTGen_genc->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      hprof_pTGenJet_JESRMPFGen_genc->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_genc->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_genc->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    case 21:
      h_pTGenJet_JESRpTGen_geng->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_geng->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      hprof_pTGenJet_JESRpTGen_geng->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      hprof_pTGenJet_JESRMPFGen_geng->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_geng->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_geng->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    case 3:
    case 2:
    case 1:
      h_pTGenJet_JESRpTGen_genuds->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      h_pTGenJet_JESRMPFGen_genuds->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      hprof_pTGenJet_JESRpTGen_genuds->Fill(leadingGenJet.Pt(),JESRpTGen,totEventWeight);
      hprof_pTGenJet_JESRMPFGen_genuds->Fill(leadingGenJet.Pt(),JESRMPFGen,totEventWeight);
      h_pTGenZ_JESRpTGen_genuds->Fill(genZboson.Pt(),JESRpTGen,totEventWeight);
      h_pTGenZ_JESRMPFGen_genuds->Fill(genZboson.Pt(),JESRMPFGen,totEventWeight);
      break;
    default:
      break;
    }
  }
  /*
  for(size_t i = 0; i < nbins_alpha; ++i){
    //std::cout << "check filling " << i << " " << bins_alpha[i+1] << std::endl;
    for(size_t j = 0; j < bins_eta.size(); ++j){
      if(!passing(leadingJet.Eta(),bins_eta[j])) continue;
      if(alpha <= bins_alpha[i+1]) {
	h_Zpt_RpT[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
	h_Zpt_RMPF[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
	switch(jetGenFlavor){
	case 5: cJetLabel->increment("b jets");
          h_Zpt_RpT_genb[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
          h_Zpt_RMPF_genb[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
	  break;
	case 4: cJetLabel->increment("c jets");
	  h_Zpt_RpT_genc[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
	  h_Zpt_RMPF_genc[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
	  break;
	case 21: cJetLabel->increment("gluon jets");
	  h_Zpt_RpT_geng[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
	  h_Zpt_RMPF_geng[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
	  break;
	default: cJetLabel->increment("uds jets");
	  h_Zpt_RpT_genuds[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_pT);
	  h_Zpt_RMPF_genuds[i+nbins_alpha*j]->Fill(Zboson.Pt(),R_MPF);
	}
      }
    }
  }
  */
  ////  if(alpha > alphaCut(Zboson.Pt())) return false;
  cMain->increment("alpha",totEventWeight);

  h_RpT->Fill(R_pT,totEventWeight);
  h_RMPF->Fill(R_MPF,totEventWeight);


  /*
  if(alpha < 0.1){
    std::cout << datasetName << " entry " << entry << std::endl;
    std::cout << "event,lumi,run " << run << " " << luminosityBlock << " " << event << std::endl;
    std::cout << "check alpha,R_pT,R_MPF,alphaCut " << alpha << " " << R_pT << " " << R_MPF << " " << alphaCut(Zboson.Pt()) << std::endl;
  }
  */
  h_alpha_RpT->Fill(alpha,R_pT,totEventWeight);
  h_alpha_RMPF->Fill(alpha,R_MPF,totEventWeight);
  /*
  if(!isData){
    //    int jetGenFlavor = getJetFlavour(leadingJetIndex);
    cJetLabel->increment("all jets");
    switch(jetGenFlavor){
    case 5: cJetLabel->increment("b jets");
      h_alpha_RpT_genb->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_genb->Fill(alpha,R_MPF,totEventWeight);
      break;
    case 4: cJetLabel->increment("c jets");
      break;
    case 21: cJetLabel->increment("gluon jets");
      break;
    default: cJetLabel->increment("uds jets");
      break;
    }
  }
  */
  /*
  if(alpha < 0.3 && Zboson.Pt() > 30){
    h_jet1cpt->Fill(leadingJet.Pt());
    h_jet1ceta->Fill(leadingJet.Eta());
    h_jet2cpt->Fill(subLeadingJet.Pt());
    h_jet2ceta->Fill(subLeadingJet.Eta());

    h_jet2etapt->Fill(subLeadingJet.Eta(),subLeadingJet.Pt());
  }
  */
  // btagging
  // Jet_btagCMVA
  // Jet_btagCSVV2
  // Jet_btagDeepB
  // Jet_btagDeepC
  // Jet_btagDeepFlavB
  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation80X
  //CSVv2 pfCombinedInclusiveSecondaryVertexV2BJetTags
  // loose  0.460 CSVv2_ichep.csv
  // medium 0.800
  // tight  0.935
  //cMVAv2 pfCombinedMVAV2BJetTags "to be used only in ttbar jet pT regime" 
  // loose  -0.715 cMVAv2_ichep.csv
  // medium 0.185
  // tight  0.875

  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation94X
  //CSVv2 pfCombinedInclusiveSecondaryVertexV2BJetTags
  // loose  0.5803 CSVv2_94XSF_V2_B_F.csv
  // medium 0.8838
  // tight  0.9693
  //DeepCSV pfDeepCSVJetTags:probb + pfDeepCSVJetTags:probbb
  // loose  0.1522 DeepCSV_94XSF_V3_B_F.csv
  // medium 0.4941
  // tight  0.8001
  //DeepFlavB
  // The b discriminator is the sum of probb, probbb, and problepb
  // loose 0.0521 DeepFlavB_94XSF_V1_B_F.csv
  // medium 0.3033
  // tight 0.7489

  //https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation102X
  //DeepCSV
  // loose  0.1241
  // medium 0.4184
  // tight  0.7527
  //DeepFlavB
  // loose  0.0494
  // medium 0.2770
  // tight  0.7264


  // https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation
  Btag *btagCSVV2 = new Btag();
  btagCSVV2->set("2016",0.460,0.800,0.935);
  btagCSVV2->set("2017",0.5803,0.8838,0.9693);
  btagCSVV2->set("2018",0.5803,0.8838,0.9693); // FIXME, same as 2017, 10112019/S.Lehti

  Btag *btagDeepB = new Btag();
  btagDeepB->set("2016",0.2217,0.6321,0.8953);
  btagDeepB->set("2017",0.1522,0.4941,0.8001);
  btagDeepB->set("2018",0.1241,0.4184,0.7527);

  Btag *btagDeepC = new Btag();
  //btagDeepC->set("2016",-0.48,-0.1,0.69);
  //btagDeepC->set("2017",0.05,0.15,0.8);
  //btagDeepC->set("2018",0.04,0.137,0.66);
  //loosening the tight WP to get about same nember of c jets as b jets
  float frac = 0.5;
  btagDeepC->set("2016",-0.48,-0.1,-0.1+frac*(0.69+0.1));
  btagDeepC->set("2017",0.05,0.15,0.15+frac*(0.8-0.15));
  btagDeepC->set("2018",0.04,0.137,0.137+frac*(0.66-0.137));
  
  Btag *btagDeepFlavB = new Btag();
  btagDeepFlavB->set("2016",0.0614,0.3093,0.7221);
  btagDeepFlavB->set("2017",0.0521,0.3033,0.7489);
  btagDeepFlavB->set("2018",0.0494,0.2770,0.7264);


  //std::cout << "check leading jet " << leadingJet.Pt() << " " << leadingJet.Eta() << std::endl;
  //std::cout << "check jet[0}      " << Jet_pt[0] << " " << Jet_eta[0] << " " << Jet_btagCSVV2[0] << std::endl;
  if(btagCSVV2->exists(year)){
    if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->loose(year)){ // leading jet
      cMain->increment("btagCSVV2loose",totEventWeight);

      h_alpha_RpT_btagCSVV2loose->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagCSVV2loose->Fill(alpha,R_MPF,totEventWeight);
      if(!isData){
       int jetGenFlavor = getJetFlavour(leadingJetIndex);
        switch(jetGenFlavor){
        case 5: 
	  h_alpha_RpT_btagCSVV2loose_genb->Fill(alpha,R_pT,totEventWeight);
	  h_alpha_RMPF_btagCSVV2loose_genb->Fill(alpha,R_MPF,totEventWeight);
          break;
        case 4: 
          break;
        case 21:
          break;
        default:
          break;
        }
      }
    }
    //std::cout << "check btag " << year << " " << btagCSVV2->medium(year) << std::endl;
    if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->medium(year)){ // leading jet
      cMain->increment("btagCSVV2medium",totEventWeight);

      h_alpha_RpT_btagCSVV2medium->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagCSVV2medium->Fill(alpha,R_MPF,totEventWeight);
      if(!isData){
        int jetGenFlavor = getJetFlavour(leadingJetIndex);
        switch(jetGenFlavor){
        case 5:
	  h_alpha_RpT_btagCSVV2medium_genb->Fill(alpha,R_pT,totEventWeight);
	  h_alpha_RMPF_btagCSVV2medium_genb->Fill(alpha,R_MPF,totEventWeight);
          break;
        case 4:
          break;
        case 21:
          break;
        default:
          break;
        }
      }
    }
    if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->tight(year)){ // leading jet
      cMain->increment("btagCSVV2tight",totEventWeight);

      h_alpha_RpT_btagCSVV2tight->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagCSVV2tight->Fill(alpha,R_MPF,totEventWeight);
      if(!isData){
        int jetGenFlavor = getJetFlavour(leadingJetIndex);
        switch(jetGenFlavor){
        case 5:
	  h_alpha_RpT_btagCSVV2tight_genb->Fill(alpha,R_pT,totEventWeight);
	  h_alpha_RMPF_btagCSVV2tight_genb->Fill(alpha,R_MPF,totEventWeight);
          break;
        case 4:
          break;
        case 21:
          break;
        default:
          break;
        }
      }
    }
  }
  if(btagDeepB->exists(year)){
    if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->loose(year)){ // leading jet
      cMain->increment("btagDeepBloose",totEventWeight);

      h_alpha_RpT_btagDeepBloose->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagDeepBloose->Fill(alpha,R_MPF,totEventWeight);
    }
    if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepB->medium(year)){ // leading jet
      cMain->increment("btagDeepBmedium",totEventWeight);

      h_alpha_RpT_btagDeepBmedium->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagDeepBmedium->Fill(alpha,R_MPF,totEventWeight);
    }
    if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->tight(year)){ // leading jet 
      cMain->increment("btagDeepBtight",totEventWeight);

      h_alpha_RpT_btagDeepBtight->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagDeepBtight->Fill(alpha,R_MPF,totEventWeight);
    }
  }
  if(btagDeepFlavB->exists(year)){
    if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->loose(year)){ // leading jet
      cMain->increment("btagDeepFlavBloose",totEventWeight);

      h_alpha_RpT_btagDeepFlavBloose->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagDeepFlavBloose->Fill(alpha,R_MPF,totEventWeight);
    }
    if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->medium(year)){ // leading jet
      cMain->increment("btagDeepFlavBmedium",totEventWeight);

      h_alpha_RpT_btagDeepFlavBmedium->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagDeepFlavBmedium->Fill(alpha,R_MPF,totEventWeight);
    }
    if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->tight(year)){ // leading jet
      cMain->increment("btagDeepFlavBtight",totEventWeight);

      h_alpha_RpT_btagDeepFlavBtight->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_btagDeepFlavBtight->Fill(alpha,R_MPF,totEventWeight);
    }
  }
  /*
  if(Jet_btagCMVA[0] > 0.5803){ // leading jet
    cMain->increment("btagCMVAloose",totEventWeight);

    h_alpha_RpT_btagCMVAloose->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_btagCMVAloose->Fill(alpha,R_MPF,totEventWeight);
  }
  if(Jet_btagCMVA[0] > 0.8838){ // leading jet
    cMain->increment("btagCMVAmedium",totEventWeight);

    h_alpha_RpT_btagCMVAmedium->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_btagCMVAmedium->Fill(alpha,R_MPF,totEventWeight);
  }
  if(Jet_btagCMVA[0] > 0.9693){ // leading jet
    cMain->increment("btagCMVAtight",totEventWeight);

    h_alpha_RpT_btagCMVAtight->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_btagCMVAtight->Fill(alpha,R_MPF,totEventWeight);
  }
  */

  for(size_t i = 0; i < cuts.size(); ++i){
    if(!passing(Zboson.Pt(),cuts[i])) continue;

    h_alpha_RpT_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
    h_alpha_RMPF_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);

    int jetGenFlavor = getJetFlavour(leadingJetIndex);
    if(!isData && abs(jetGenFlavor) == 5){
      h_alpha_RpT_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
      h_alpha_RMPF_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
    }

    if(btagCSVV2->exists(year)){
      if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->loose(year)){ // leading jet
	h_alpha_RpT_btagCSVV2loose_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
	h_alpha_RMPF_btagCSVV2loose_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
	if(!isData && abs(jetGenFlavor) == 5){
	  h_alpha_RpT_btagCSVV2loose_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
	  h_alpha_RMPF_btagCSVV2loose_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
	}
      }
      if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->medium(year)){ // leading jet
	h_alpha_RpT_btagCSVV2medium_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
	h_alpha_RMPF_btagCSVV2medium_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
	if(!isData && abs(jetGenFlavor) == 5){
          h_alpha_RpT_btagCSVV2medium_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2medium_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
        }
      }
      if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->tight(year)){ // leading jet
	h_alpha_RpT_btagCSVV2tight_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
	h_alpha_RMPF_btagCSVV2tight_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
	if(!isData && abs(jetGenFlavor) == 5){
          h_alpha_RpT_btagCSVV2tight_Zpt_genb[i]->Fill(alpha,R_pT,totEventWeight);
          h_alpha_RMPF_btagCSVV2tight_Zpt_genb[i]->Fill(alpha,R_MPF,totEventWeight);
        }
      }
    }
    // DeepB
    if(btagDeepB->exists(year)){
      if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->loose(year)){ // leading jet                                                    
        h_alpha_RpT_btagDeepBloose_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepBloose_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->medium(year)){ // leading jet                                                   
        h_alpha_RpT_btagDeepBmedium_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepBmedium_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->tight(year)){ // leading jet                                                    
        h_alpha_RpT_btagDeepBtight_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
        h_alpha_RMPF_btagDeepBtight_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
    }
    // DeepFlavB
    if(btagDeepFlavB->exists(year)){
      if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->loose(year)){ // leading jet
	h_alpha_RpT_btagDeepFlavBloose_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
	h_alpha_RMPF_btagDeepFlavBloose_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->medium(year)){ // leading jet
	h_alpha_RpT_btagDeepFlavBmedium_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
	h_alpha_RMPF_btagDeepFlavBmedium_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
      if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->tight(year)){ // leading jet
	h_alpha_RpT_btagDeepFlavBtight_Zpt[i]->Fill(alpha,R_pT,totEventWeight);
	h_alpha_RMPF_btagDeepFlavBtight_Zpt[i]->Fill(alpha,R_MPF,totEventWeight);
      }
    }

  }

  //if(R_pT < 0 || R_pT > 2 || R_MPF < 0 || R_MPF > 2) return false;
  cMain->increment("RpT+MPF cuts",totEventWeight);

    h_jet1cpt->Fill(leadingJet.Pt());
    h_jet1ceta->Fill(leadingJet.Eta());
    h_jet2cpt->Fill(subLeadingJet.Pt());
    h_jet2ceta->Fill(subLeadingJet.Eta());

    h_jet2etapt->Fill(subLeadingJet.Eta(),subLeadingJet.Pt());

  //int jetGenFlavor = getJetFlavour(leadingJetIndex);
  //std::cout << "check jetGenFlavor " << jetGenFlavor << std::endl;
    //std::cout << "check npsWeights " << npsWeights << " " << isData << std::endl;
  for(size_t iPS = 0; iPS < npsWeights; ++iPS){
    float eweight = totEventWeight*psWeights[iPS];
    //std::cout << "check eweight " << iPS << " " << eweight << std::endl;
    for(size_t i = 0; i < nbins_alpha; ++i){
      //std::cout << "check alpha " << leadingJet.Pt() << " " << alpha << " " << bins_alpha[i+1] << std::endl;
      if(alpha < bins_alpha[i+1]) {
	for(size_t j = 0; j < bins_eta.size(); ++j){
	  //std::cout << "check jet eta " << leadingJet.Pt() << " " << leadingJet.Eta() << " " << bins_eta[j] << " " << passing(leadingJet.Eta(),bins_eta[j]) << std::endl;
	  if(!passing(leadingJet.Eta(),bins_eta[j])) continue;

	  size_t index = i+nbins_alpha*j+nbins_alpha*bins_eta.size()*iPS;
	  /*
	      size_t ialpha = index%(nbins_alpha*bins_eta.size())%nbins_alpha;
	      size_t ieta = index%(nbins_alpha*bins_eta.size())/nbins_alpha;
	      size_t ipsw = index/(nbins_alpha*bins_eta.size());
	      std::cout << "check index i " << ialpha << " " << i << std::endl;
	      std::cout << "check index j " << ieta << " " << j << std::endl;
	      std::cout << "check index iPS " << ipsw << " " << iPS << std::endl;
	  std::cout << "check passing " << leadingJet.Pt() << " " << index << std::endl;
	  */
	  h_Zpt_mZ[index]->Fill(Zboson.Pt(),Zboson.M(),eweight);
          if(!isData) h_Zpt_mZgen[index]->Fill(Zboson.Pt(),genZboson.M(),eweight);

	  h_Zpt_RpT[index]->Fill(Zboson.Pt(),R_pT,eweight);
	  h_Zpt_RMPF[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	  h_Zpt_RMPFjet1[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
	  h_Zpt_RMPFjetn[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
	  h_Zpt_RMPFuncl[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	  cJetLabel->increment("all jets");
	  switch(jetGenFlavor){
	  case 5: cJetLabel->increment("b jets");
	    h_Zpt_RpT_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_genb[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
	    h_Zpt_RMPFjetn_genb[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
	    h_Zpt_RMPFuncl_genb[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    break;
	  case 4: cJetLabel->increment("c jets");
	    h_Zpt_RpT_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_genc[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_genc[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_genc[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    break;
	  case 21: cJetLabel->increment("gluon jets");
	    h_Zpt_RpT_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_geng[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_geng[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_geng[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    break;
	  case 3:
	  case 2:
	  case 1: cJetLabel->increment("uds jets");
	    h_Zpt_RpT_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_genuds[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_genuds[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_genuds[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    break;
	  default: cJetLabel->increment("unclassified jets");
	  }
	  if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->loose(year)){ // leading jet
	    h_Zpt_RpT_btagCSVV2loose[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_btagCSVV2loose[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    if(!isData){
	      //std::cout << "check jetGenFlavor " << jetGenFlavor << std::endl;
	      switch(jetGenFlavor){
	      case 5:
		//std::cout << "check filling 5" << std::endl;
		h_Zpt_RpT_btagCSVV2loose_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2loose_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 4:
		//std::cout << "check filling 4" << std::endl;
		h_Zpt_RpT_btagCSVV2loose_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2loose_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 21:
		h_Zpt_RpT_btagCSVV2loose_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2loose_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 3:
	      case 2:
	      case 1:
		h_Zpt_RpT_btagCSVV2loose_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2loose_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
		//default:
	      }
	    }
	  }
	  if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->medium(year)){ // leading jet
	    h_Zpt_RpT_btagCSVV2medium[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_btagCSVV2medium[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    if(!isData){
	      switch(jetGenFlavor){ 
	      case 5:
		h_Zpt_RpT_btagCSVV2medium_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2medium_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 4:
		h_Zpt_RpT_btagCSVV2medium_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2medium_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 21:
		h_Zpt_RpT_btagCSVV2medium_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2medium_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 3:
	      case 2:
	      case 1:
		h_Zpt_RpT_btagCSVV2medium_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2medium_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
		//default:
	      }
	    }
	  }
	  if(Jet_btagCSVV2[leadingJetIndex] > btagCSVV2->tight(year)){ // leading jet
	    h_Zpt_RpT_btagCSVV2tight[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_btagCSVV2tight[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    if(!isData){
	      switch(jetGenFlavor){
	      case 5:
		h_Zpt_RpT_btagCSVV2tight_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2tight_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 4:
		h_Zpt_RpT_btagCSVV2tight_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2tight_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 21:
		h_Zpt_RpT_btagCSVV2tight_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2tight_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 3:
	      case 2:
	      case 1:
		h_Zpt_RpT_btagCSVV2tight_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagCSVV2tight_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
		//default:
	      }
	    }
	  }
	  if(Jet_btagDeepB[leadingJetIndex] > btagDeepB->tight(year)){ // leading jet
	    h_Zpt_RpT_btagDeepBtight[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_btagDeepBtight[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_btagDeepBtight[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
	    h_Zpt_RMPFjetn_btagDeepBtight[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
	    h_Zpt_RMPFuncl_btagDeepBtight[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    cJetLabel_btagDeepBtight->increment("all jets");
	    if(!isData){
	      switch(jetGenFlavor){
	      case 5:cJetLabel_btagDeepBtight->increment("b jets");
		h_Zpt_RpT_btagDeepBtight_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepBtight_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 4:cJetLabel_btagDeepBtight->increment("c jets");
		h_Zpt_RpT_btagDeepBtight_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepBtight_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 21:cJetLabel_btagDeepBtight->increment("gluon jets");
		h_Zpt_RpT_btagDeepBtight_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepBtight_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 3:
	      case 2:
	      case 1:cJetLabel_btagDeepBtight->increment("uds jets");
		h_Zpt_RpT_btagDeepBtight_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepBtight_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      default:cJetLabel_btagDeepBtight->increment("unclassified jets");
	      }
	    }
	  }
	  if(Jet_btagDeepFlavB[leadingJetIndex] > btagDeepFlavB->tight(year)){ // leading jet
	    h_Zpt_RpT_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_btagDeepFlavBtight[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    if(!isData){
	      switch(jetGenFlavor){
	      case 5:
		h_Zpt_RpT_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepFlavBtight_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 4:
		h_Zpt_RpT_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepFlavBtight_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 21:
		h_Zpt_RpT_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_btagDeepFlavBtight_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 3:
	      case 2:
	      case 1:
		h_Zpt_RpT_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepFlavBtight_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
		//default:
	      }
	    }
	  }
	  if(Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
	     Jet_btagDeepC[leadingJetIndex] > btagDeepC->tight(year)){ // leading jet
            h_Zpt_RpT_btagDeepCtight[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_btagDeepCtight[index]->Fill(Zboson.Pt(),R_MPF,eweight);
            h_Zpt_RMPFjet1_btagDeepCtight[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
            h_Zpt_RMPFjetn_btagDeepCtight[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
            h_Zpt_RMPFuncl_btagDeepCtight[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    cJetLabel_btagDeepCtight->increment("all jets");
            if(!isData){
              switch(jetGenFlavor){
              case 5:cJetLabel_btagDeepCtight->increment("b jets");
                h_Zpt_RpT_btagDeepCtight_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepCtight_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 4:cJetLabel_btagDeepCtight->increment("c jets");
                h_Zpt_RpT_btagDeepCtight_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepCtight_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 21:cJetLabel_btagDeepCtight->increment("gluon jets");
                h_Zpt_RpT_btagDeepCtight_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepCtight_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 3:
	      case 2:
	      case 1:cJetLabel_btagDeepCtight->increment("uds jets");
                h_Zpt_RpT_btagDeepCtight_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_btagDeepCtight_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      default: cJetLabel_btagDeepCtight->increment("unclassified jets");
              }
            }
          }
	  if(Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
	     Jet_btagDeepC[leadingJetIndex] <= btagDeepC->tight(year) &&
	     Jet_qgl[leadingJetIndex] >= 0 &&
	     Jet_qgl[leadingJetIndex] < 0.5){
	    h_Zpt_RpT_gluontag[index]->Fill(Zboson.Pt(),R_pT,eweight);
	    h_Zpt_RMPF_gluontag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_gluontag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
	    h_Zpt_RMPFjetn_gluontag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
	    h_Zpt_RMPFuncl_gluontag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    cJetLabel_gluontag->increment("all jets");
	    if(!isData){
	      switch(jetGenFlavor){
	      case 5:cJetLabel_gluontag->increment("b jets");
		h_Zpt_RpT_gluontag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_gluontag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 4:cJetLabel_gluontag->increment("c jets");
		h_Zpt_RpT_gluontag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_gluontag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 21:cJetLabel_gluontag->increment("gluon jets");
		h_Zpt_RpT_gluontag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_gluontag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      case 3:
	      case 2:
	      case 1: cJetLabel_gluontag->increment("uds jets");
		h_Zpt_RpT_gluontag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
		h_Zpt_RMPF_gluontag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		break;
	      default: cJetLabel_gluontag->increment("unclassified jets");
	      }
	    }
	  }
	  if(Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
             Jet_btagDeepC[leadingJetIndex] <= btagDeepC->tight(year) &&
             Jet_qgl[leadingJetIndex] >= 0.5){
            h_Zpt_RpT_quarktag[index]->Fill(Zboson.Pt(),R_pT,eweight);
            h_Zpt_RMPF_quarktag[index]->Fill(Zboson.Pt(),R_MPF,eweight);
	    h_Zpt_RMPFjet1_quarktag[index]->Fill(Zboson.Pt(),R_MPFjet1,eweight);
	    h_Zpt_RMPFjetn_quarktag[index]->Fill(Zboson.Pt(),R_MPFjetn,eweight);
	    h_Zpt_RMPFuncl_quarktag[index]->Fill(Zboson.Pt(),R_MPFuncl,eweight);
	    cJetLabel_quarktag->increment("all jets");
            if(!isData){
              switch(jetGenFlavor){
              case 5:cJetLabel_quarktag->increment("b jets");
                h_Zpt_RpT_quarktag_genb[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genb[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 4:cJetLabel_quarktag->increment("c jets");
                h_Zpt_RpT_quarktag_genc[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genc[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
              case 21:cJetLabel_quarktag->increment("gluon jets");
                h_Zpt_RpT_quarktag_geng[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_geng[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
	      case 3:cJetLabel_quarktag->increment("uds jets");
		h_Zpt_RpT_quarktag_gens[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_gens[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		h_Zpt_RpT_quarktag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
	      case 2:
	      case 1:cJetLabel_quarktag->increment("uds jets");
                h_Zpt_RpT_quarktag_genud[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genud[index]->Fill(Zboson.Pt(),R_MPF,eweight);
		h_Zpt_RpT_quarktag_genuds[index]->Fill(Zboson.Pt(),R_pT,eweight);
                h_Zpt_RMPF_quarktag_genuds[index]->Fill(Zboson.Pt(),R_MPF,eweight);
                break;
	      default: cJetLabel_quarktag->increment("unclassified jets");
              }
            }
          }
	  if(Jet_btagDeepB[leadingJetIndex] <= btagDeepB->tight(year) &&
             Jet_btagDeepC[leadingJetIndex] <= btagDeepC->tight(year) &&
             Jet_qgl[leadingJetIndex] < 0){
            cJetLabel_qgtLT0tag->increment("all jets");
            if(!isData){
              switch(jetGenFlavor){
              case 5:cJetLabel_qgtLT0tag->increment("b jets");
                break;
              case 4:cJetLabel_qgtLT0tag->increment("c jets");
                break;
              case 21:cJetLabel_qgtLT0tag->increment("gluon jets");
                break;
              case 3:
              case 2:
              case 1: cJetLabel_qgtLT0tag->increment("uds jets");
                break;
              default: cJetLabel_qgtLT0tag->increment("unclassified jets");
              }
            }
          }

	}
      }
    }
  }
  return true;
}
/*
bool Analysis::GetTriggerDecision(const char* trg){
  std::vector<std::string> triggerNames;
  std::string s_trg = std::string(trg);
  size_t pos = s_trg.find("||");
  if (pos == s_trg.size()){
    triggerNames.push_back(s_trg);
  }else{
    while (pos < s_trg.size()){
      pos = s_trg.find("||");
      triggerNames.push_back(s_trg.substr(0,pos));
      s_trg = s_trg.substr(pos+2,s_trg.size());
    }
  }
  bool trgDecision = false;
  for(size_t i = 0; i < triggerNames.size(); ++i){
    //    std::cout << "check trgMap " << trgMap[triggerNames[i].c_str()] << std::endl;
    //    TObject* to = fReader.FindObject(triggerNames[i].c_str());
    //    TTreeReaderValue<Bool_t> trgBit = {fReader, triggerNames[i].c_str()};
    //    trgDecision = trgDecision || *trgBit;
    //    TBranch* b = fChain->FindBranch(triggerNames[i].c_str());
    //    bool value = b->
    //        std::cout << "check branch " << triggerBit->GetName() << std::endl;
    //    bool tmp = *HLT_IsoMu27;
    //bool tmp2 = *HLT_Mu45_eta2p1;
    std::cout << "check triggerBit " << triggerNames[i] << " " << triggerBit[i] << std::endl; 
  }
  //    std::cout << "check " << *HLT_test << std::endl;
    
    
  
  return trgDecision;
}
*/
