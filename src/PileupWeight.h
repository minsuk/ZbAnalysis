#ifndef PileupWeight_h
#define PileupWeight_h

#include "TH1F.h"

class PileupWeight {
 public:
  PileupWeight(TH1F*, TH1F*);
  ~PileupWeight();
  
  double getWeight(int pileup);
  void calculateWeights(TH1F*, TH1F*);

  TH1F* getPUData(){return h_data;}
  TH1F* getPUMC(){return h_mc;}

 private:
  TH1F *h_data;
  TH1F *h_mc;
  TH1F *h_weight;

  bool noData;
};
#endif
