#ifndef __counters__
#define __counters__

#include "TH1F.h"
#include <string>
#include <map>
#include <vector>

class Counters {
 public:
  Counters(const char* name);
  ~Counters();

  void increment(std::string counterName, double weight = 1);
  void increment(const char* counterName, double weight = 1);
  
  void print();
  void printBothCounters();

  double getCount(const char* counterName);
  
  TH1F* getHisto(std::string newname = "unweighted counters");
  TH1F* getWeightedHisto(std::string newname = "weighted counters");

  void book(std::string,long value = 0);

 private:
  std::string getName();

  std::string name;
  std::map<std::string,long> unweightedCounter;
  std::map<std::string,double> weightedCounter;
  std::vector<std::string>  counterOrder;
};
#endif
